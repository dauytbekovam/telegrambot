package CZN.repository;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import CZN.model.standart.Message;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Repository
public interface MessageRepo extends CrudRepository<Message, Integer> {

    Message findFirstByIdAndLanguageId(int id, int languageId);
    List<Message> findAllByNameContainingAndLanguageIdOrderById(String name, int langId);

    @Query("select m from Message m where m.id = ?1 and m.languageId =?2")
    Message findByIdAndLanguageId(int id, int languageId);

    @Query("select m.name from Message m where m.id = :id and m.languageId = :langId")
    String getMessageText(@Param("id") int id, @Param("langId") int langId);

    @Transactional
    @Modifying
    @Query("update Message set name = ?1 where id = ?2 and languageId = ?3")
    void update(String name, int id, int langId);


    @Query("select m.name from Message m WHERE m.id =?1 and m.languageId =?2")
    Optional<String> getName(int id, int langId);
}
