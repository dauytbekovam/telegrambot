package CZN.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import CZN.model.standart.LanguageUser;

@Repository
public interface LanguageUserRepo extends CrudRepository<LanguageUser, Integer> {
    LanguageUser getByChatId(long chatId);
}
