package CZN.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import CZN.model.standart.Properties;

@Repository
public interface PropertiesRepo extends CrudRepository<Properties, Integer> {
Properties findFirstById(int id);
}
