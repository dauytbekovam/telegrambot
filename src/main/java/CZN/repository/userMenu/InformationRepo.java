package CZN.repository.userMenu;

import CZN.model.custom.userMenu.InfoQuestion;
import CZN.model.custom.userMenu.Information;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface InformationRepo extends CrudRepository<Information, Integer> {
    List<Information> findAllByOrderById();
    Information findById(int id);
    List<InfoQuestion> findAllById(int id);

}
