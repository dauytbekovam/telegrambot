package CZN.repository.userMenu;


import CZN.model.custom.userMenu.InfoQuestion;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface InfoQuestionRepo extends CrudRepository<InfoQuestion, Integer> {
    InfoQuestion findById(int id);
}
