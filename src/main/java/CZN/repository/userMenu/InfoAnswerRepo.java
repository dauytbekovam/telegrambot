package CZN.repository.userMenu;


import CZN.model.custom.userMenu.InfoAnswer;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface InfoAnswerRepo extends CrudRepository<InfoAnswer, Integer> {
    InfoAnswer findByInfoQuestionId(int id);
}
