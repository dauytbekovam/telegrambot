package CZN.repository.userMenu;


import CZN.model.custom.userMenu.PollAnswer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PollAnswerRepo extends JpaRepository<PollAnswer, Integer> {

    PollAnswer findById(int id);
}
