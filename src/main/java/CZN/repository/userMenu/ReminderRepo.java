package CZN.repository.userMenu;

import CZN.model.custom.userMenu.Reminder;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ReminderRepo extends CrudRepository<Reminder, Integer> {
    List<Reminder> findAll();
    List<Reminder> findAllByUserId(int id);
    Reminder findById(int id);
}
