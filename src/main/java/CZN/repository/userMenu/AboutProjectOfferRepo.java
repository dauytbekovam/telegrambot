package CZN.repository.userMenu;

import CZN.model.custom.userMenu.AboutProjectOffer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface AboutProjectOfferRepo extends JpaRepository<AboutProjectOffer, Integer> {

    AboutProjectOffer findById(long id);

    List<AboutProjectOffer> findAllByDateBetweenOrderById(Date startDate, Date endDate);
}
