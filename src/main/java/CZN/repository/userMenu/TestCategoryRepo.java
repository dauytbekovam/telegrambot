package CZN.repository.userMenu;

import CZN.model.custom.userMenu.TestCategory;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TestCategoryRepo extends CrudRepository<TestCategory, Integer> {
    List<TestCategory> findAllByOrderById();
    TestCategory findById(int id);
}
