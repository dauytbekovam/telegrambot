package CZN.repository.userMenu;

import CZN.model.custom.userMenu.AboutProjectContactInfo;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AboutProjectContactInfoRepo extends CrudRepository<AboutProjectContactInfo, Integer> {

    List<AboutProjectContactInfo> findAllByOrderById();
    AboutProjectContactInfo findById(int id);

}
