package CZN.repository.userMenu;

import CZN.model.custom.userMenu.ProblemSubCategory;
import CZN.model.standart.Role;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface RoleRepo extends CrudRepository<Role, Integer> {
    List<Role> findAll();
    Role findById(int id);
    List<Role> findAllByOrderById();
}
