package CZN.repository.userMenu;

import CZN.model.custom.userMenu.PerDay;
import CZN.model.custom.userMenu.PerDayTime;
import CZN.model.standart.Employee;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PerDayTimeRepo  extends CrudRepository<PerDayTime, Integer> {

    PerDayTime findById(int id);
}
