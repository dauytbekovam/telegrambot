package CZN.repository.userMenu;


import CZN.model.custom.userMenu.PollInterview;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface InterviewRepo extends CrudRepository<PollInterview, Integer> {

    List<PollInterview> findAll();
    List<PollInterview> findAllByOrderById();
    PollInterview findById(int id);

}
