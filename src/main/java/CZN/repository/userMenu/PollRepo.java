package CZN.repository.userMenu;


import CZN.model.custom.userMenu.Poll;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface PollRepo extends JpaRepository<Poll, Integer> {
    Poll findById(int id);

    List<Poll> findAllByDateBetween(Date start, Date end);

    List<Poll> findAllByDateBetweenAndInterview(Date start, Date end,int id);

}
