package CZN.repository.userMenu;


import CZN.model.custom.userMenu.ProblemCategory;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProblemCategoryRepo extends CrudRepository<ProblemCategory, Integer> {
    List<ProblemCategory> findAllByOrderById();
    ProblemCategory findById(int id);
}
