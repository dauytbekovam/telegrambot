package CZN.repository.userMenu;


import CZN.model.custom.userMenu.ProblemSubCategory;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProblemSubCategoryRepo extends CrudRepository<ProblemSubCategory, Integer> {
    List<ProblemSubCategory> findAllByOrderById();
    ProblemSubCategory findById(int id);
}
