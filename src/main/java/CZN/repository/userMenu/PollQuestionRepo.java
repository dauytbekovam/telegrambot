package CZN.repository.userMenu;


import CZN.model.custom.userMenu.PollQuestion;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PollQuestionRepo extends JpaRepository<PollQuestion, Integer> {
    PollQuestion findById(int id);
}
