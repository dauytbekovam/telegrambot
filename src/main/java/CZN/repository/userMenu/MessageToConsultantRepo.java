package CZN.repository.userMenu;


import CZN.model.custom.userMenu.MessageToConsultant;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface MessageToConsultantRepo extends JpaRepository<MessageToConsultant, Integer> {
    List<MessageToConsultant> findAllByOrderById();
    MessageToConsultant findById(long id);
    List<MessageToConsultant> findAllByDateBetweenOrderById(Date startDate, Date endDate);

}
