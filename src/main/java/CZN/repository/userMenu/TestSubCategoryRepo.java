package CZN.repository.userMenu;


import CZN.model.custom.userMenu.TestSubCategory;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TestSubCategoryRepo extends CrudRepository<TestSubCategory, Integer> {
    List<TestSubCategory> findAllByOrderById();
    TestSubCategory findById(int id);
}
