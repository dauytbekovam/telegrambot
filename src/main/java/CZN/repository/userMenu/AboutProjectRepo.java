package CZN.repository.userMenu;

import CZN.model.custom.userMenu.AboutProject;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AboutProjectRepo extends CrudRepository<AboutProject, Integer> {
    List<AboutProject> findAllByOrderById();
    AboutProject findById(int id);
}
