package CZN.repository.userMenu;

import CZN.model.standart.Employee;
import CZN.model.standart.Role;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EmployeeRepo extends CrudRepository<Employee, Integer> {
    int countByChatId(long chatId);
    List<Employee> findAll();
    Employee findById(int id);
//    Employee findByChatId(long chatId);
    Employee findByChatId(long chatId);
    List<Employee> findAllByOrderById();


}
