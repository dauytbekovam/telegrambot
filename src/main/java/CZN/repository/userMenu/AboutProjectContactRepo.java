package CZN.repository.userMenu;


import CZN.model.custom.userMenu.AboutProjectContact;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AboutProjectContactRepo extends CrudRepository<AboutProjectContact, Integer> {

    List<AboutProjectContact> findAllByOrderById();
    AboutProjectContact findById(int id);

}
