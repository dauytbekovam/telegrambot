package CZN.repository.userMenu;

import CZN.model.custom.userMenu.PerDay;
import CZN.model.standart.Employee;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PerDayRepo  extends CrudRepository<PerDay, Integer> {

    List<PerDay> findAllByOrderById();
    PerDay findById(int id);
}
