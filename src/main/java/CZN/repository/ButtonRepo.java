package CZN.repository;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import CZN.model.standart.Button;

import java.util.List;
import java.util.Optional;

@Repository
public interface ButtonRepo extends CrudRepository<Button, Integer> {
    Button  findByNameAndLangId(String buttonName, int languageId);

    @Query("select m from Button m where m.id = ?1 and m.langId =?2")
    Button  findByIdAndLangId(int id, int langId);

    int     countByNameAndLangId(String name, int langId);
    boolean existsButtonByNameAndLangId(String text, int langId);
    List<Button> findAllByNameContainingAndLangIdOrderById(String value, int langId);
    Button   findById(int id);

    @Transactional
    @Modifying
    @Query("update Button set name = ?1 where id = ?2 and langId = ?3")
    void update(String name, int id, int langId);

    @Query("select m.name from Button m WHERE m.id =?1 and m.langId =?2")
    Optional<String> getName(int id, int langId);
}
