package CZN.repository;

import CZN.repository.userMenu.*;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class TelegramBotRepositoryProvider {

    @Getter
    @Setter
    private static PropertiesRepo propertiesRepo;
    @Getter
    @Setter
    private static LanguageUserRepo languageUserRepo;
    @Getter
    @Setter
    private static UsersRepo usersRepo;
    @Getter
    @Setter
    private static ButtonRepo buttonRepo;
    @Getter
    @Setter
    private static MessageRepo messageRepo;
    @Getter
    @Setter
    private static KeyboardRepo keyboardRepo;
    @Getter
    @Setter
    private static AdminRepo adminRepo;
    @Getter
    @Setter
    private static InformationRepo informationRepo;
    @Getter
    @Setter
    private static InfoAnswerRepo infoAnswerRepo;
    @Getter
    @Setter
    private static InfoQuestionRepo infoQuestionRepo;
    @Getter
    @Setter
    private static TestCategoryRepo testCategoryRepo;
    @Getter
    @Setter
    private static TestSubCategoryRepo testSubCategoryRepo;
    @Getter
    @Setter
    private static ProblemCategoryRepo problemCategoryRepo;
    @Getter
    @Setter
    private static ProblemSubCategoryRepo problemSubCategoryRepo;
    @Getter
    @Setter
    private static AboutProjectRepo aboutProjectRepo;
    @Getter
    @Setter
    private static AboutProjectOfferRepo aboutProjectOfferRepo;
    @Getter
    @Setter
    private static AboutProjectContactRepo aboutProjectContactRepo;
    @Getter
    @Setter
    private static AboutProjectContactInfoRepo aboutProjectContactInfoRepo;
    @Getter
    @Setter
    private static InterviewRepo interviewRepo;
    @Getter
    @Setter
    private static PollRepo pollRepo;
    @Getter
    @Setter
    private static PollQuestionRepo pollQuestionRepo;
    @Getter
    @Setter
    private static PollAnswerRepo pollAnswerRepo;
    @Getter
    @Setter
    private static MessageToConsultantRepo messageToConsultantRepo;
    @Getter
    @Setter
    private static RoleRepo roleRepo;
    @Getter
    @Setter
    private static EmployeeRepo employeeRepo;
    @Getter
    @Setter
    private static PerDayRepo perDayRepo;
    @Getter
    @Setter
    private static PerDayTimeRepo perDayTimeRepo;
    @Getter
    @Setter
    private static ReminderRepo reminderRepo;


    //---------------------------------------------------------------
    @Autowired
    public TelegramBotRepositoryProvider(
                                         PropertiesRepo propertiesRepo, LanguageUserRepo languageUserRepo,
                                         UsersRepo usersRepo, ButtonRepo buttonRepo, MessageRepo messageRepo,
                                         KeyboardRepo keyboardRepo, AdminRepo adminRepo,

                                         InformationRepo informationRepo,
                                         InfoAnswerRepo infoAnswerRepo,
                                         InfoQuestionRepo infoQuestionRepo,

                                         TestCategoryRepo testCategoryRepo,
                                         TestSubCategoryRepo testSubCategoryRepo,

                                         ProblemCategoryRepo problemCategoryRepo,
                                         ProblemSubCategoryRepo problemSubCategoryRepo,

                                         AboutProjectRepo aboutProjectRepo,
                                         AboutProjectOfferRepo aboutProjectOfferRepo,
                                         AboutProjectContactRepo aboutProjectContactRepo,
                                         AboutProjectContactInfoRepo aboutProjectContactInfoRepo,

                                         InterviewRepo interviewRepo,
                                         PollRepo pollRepo,
                                         PollAnswerRepo pollAnswerRepo,
                                         PollQuestionRepo pollQuestionRepo,

                                         MessageToConsultantRepo messageToConsultantRepo,
                                         RoleRepo roleRepo,

                                         EmployeeRepo employeeRepo,

                                         PerDayRepo perDayRepo,
                                         PerDayTimeRepo perDayTimeRepo,

                                         ReminderRepo reminderRepo) {
        setPropertiesRepo(propertiesRepo);
        setLanguageUserRepo(languageUserRepo);
        setUsersRepo(usersRepo);
        setButtonRepo(buttonRepo);
        setMessageRepo(messageRepo);
        setKeyboardRepo(keyboardRepo);
        setAdminRepo(adminRepo);
        setInformationRepo(informationRepo);
        setInfoAnswerRepo(infoAnswerRepo);
        setInfoQuestionRepo(infoQuestionRepo);
        setTestCategoryRepo(testCategoryRepo);
        setTestSubCategoryRepo(testSubCategoryRepo);
        setProblemCategoryRepo(problemCategoryRepo);
        setProblemSubCategoryRepo(problemSubCategoryRepo);
        setAboutProjectRepo(aboutProjectRepo);
        setAboutProjectOfferRepo(aboutProjectOfferRepo);
        setAboutProjectContactRepo(aboutProjectContactRepo);
        setAboutProjectContactInfoRepo(aboutProjectContactInfoRepo);
        setInterviewRepo(interviewRepo);
        setPollRepo(pollRepo);
        setPollQuestionRepo(pollQuestionRepo);
        setPollAnswerRepo(pollAnswerRepo);
        setMessageToConsultantRepo(messageToConsultantRepo);
        setRoleRepo(roleRepo);
        setEmployeeRepo(employeeRepo);
        setPerDayRepo(perDayRepo);
        setPerDayTimeRepo(perDayTimeRepo);
        setReminderRepo(reminderRepo);

    }
}
