package CZN;


import CZN.model.custom.userMenu.Reminder;
import CZN.repository.TelegramBotRepositoryProvider;
import CZN.repository.userMenu.ReminderRepo;
import CZN.service.SaveDocument;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.telegram.telegrambots.bots.DefaultAbsSender;
import org.telegram.telegrambots.meta.TelegramBotsApi;
import org.telegram.telegrambots.meta.exceptions.TelegramApiRequestException;
import org.telegram.telegrambots.updatesreceivers.DefaultBotSession;
import CZN.configuration.Bot;

import java.util.*;

@SpringBootApplication
@Slf4j
public class CznApplication implements CommandLineRunner {

	public static Map<Reminder, Timer> reminderMap = new HashMap<>();

	public static void main(String[] args) {
		System.out.println("HEllo");
		SpringApplication.run(CznApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		log.info("ApiContextInitializer.InitNormal()");
		TelegramBotsApi telegramBotsApi = new TelegramBotsApi(DefaultBotSession.class);
		Bot bot = new Bot();
		try {
			telegramBotsApi.registerBot(bot);
			addTimer(bot);
			log.info("Bot was registered: " + bot.getBotUsername());
		} catch (TelegramApiRequestException e) {
			log.error("Error in main class", e);
		}
	}
	public static void addTimer(DefaultAbsSender bot) {
		Timer timer;
		ReminderRepo reminderRepo = TelegramBotRepositoryProvider.getReminderRepo();
		List<Reminder> reminders = reminderRepo.findAll();
		Calendar calendar = Calendar.getInstance();
		for	(Reminder reminder : reminders){
			timer = new Timer(true);

			calendar.set(Calendar.HOUR_OF_DAY, reminder.getHour());
			calendar.set(Calendar.MINUTE, reminder.getMinute());
			calendar.set(Calendar.SECOND, 0);
			Date date = calendar.getTime();
//			timer.
			timer.scheduleAtFixedRate(new SaveDocument(bot,reminder), date, 1000 * 60 * 60 * 24); // 1000 = 1 seconds  1000 * 60 * 60 * 12

			reminderMap.put(reminder, timer);

		}

	}

}
