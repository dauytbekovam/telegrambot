package CZN.enums;

public enum FileType {
    audio, document, photo, voice, video
}
