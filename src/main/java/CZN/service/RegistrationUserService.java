package CZN.service;

import CZN.enums.WaitingType;
import CZN.model.standart.User;
import CZN.util.BotUtil;
import CZN.util.Const;
import CZN.util.UpdateUtil;
import org.springframework.stereotype.Service;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

@Service
public class RegistrationUserService {

    private User user;
    private long chatId;
    private BotUtil botUtil;
    private WaitingType waitingType = WaitingType.START;
    private boolean COMEBACK = false;
    private boolean EXIT = true;

    public boolean isRegistration(Update update, BotUtil botUtil) throws TelegramApiException {
        if (botUtil == null || chatId == 0) {
            chatId = UpdateUtil.getChatId(update);
            this.botUtil = botUtil;
        }
        switch (waitingType) {
            case START:
                user = new User();
                user.setChatId(chatId);
                getName();
                waitingType = WaitingType.SET_FULL_NAME;
                return COMEBACK;
            case SET_FULL_NAME:
                if (update.hasMessage() && update.getMessage().hasText() && update.getMessage().getText().length() <= 50) {
                    user.setFullName(update.getMessage().getText());
                    user.setUsername(UpdateUtil.getFrom(update));
                    return EXIT;
                } else {
                    wrongData();
                    getName();
                }
                return COMEBACK;
        }
        return EXIT;
    }

    private int wrongData() throws TelegramApiException {
        return botUtil.sendMessage(Const.WRONG_DATA_TEXT, chatId);
    }

    private int getName() throws TelegramApiException {
        return botUtil.sendMessage(Const.SET_FULL_NAME_MESSAGE, chatId);
    }

    public User getUser() {
        return user;
    }

}
