package CZN.service;


import CZN.model.custom.userMenu.Reminder;
import org.telegram.telegrambots.bots.DefaultAbsSender;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;

import java.util.*;

public class SaveDocument extends TimerTask {
    DefaultAbsSender bot;
    Reminder reminder;

    public SaveDocument(DefaultAbsSender bot, Reminder reminder) {
        this.bot = bot;
        this.reminder = reminder;
    }

    @Override
    public void run() {

        try {
            SendMessage sendMessage = new SendMessage();
            sendMessage.setChatId(reminder.getUserId());
            sendMessage.setText("Напоминаем о необходимости приема лекарства. Если хотите " +
                    "остановить напоминалку, нажмите /cancel" + reminder.getId() + "\nизменить схему приема - /editTime"
                    + reminder.getId());
            bot.execute(sendMessage);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


}
