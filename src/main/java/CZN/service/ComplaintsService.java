package CZN.service;


import CZN.enums.FileType;
import CZN.enums.Language;
import CZN.model.custom.userMenu.MessageToConsultant;
import CZN.repository.MessageRepo;
import CZN.repository.PropertiesRepo;
import CZN.repository.TelegramBotRepositoryProvider;
import CZN.repository.userMenu.MessageToConsultantRepo;
import CZN.util.Const;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFColor;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.telegram.telegrambots.bots.DefaultAbsSender;
import org.telegram.telegrambots.meta.api.methods.GetFile;
import org.telegram.telegrambots.meta.api.methods.send.SendDocument;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.methods.updatingmessages.DeleteMessage;
import org.telegram.telegrambots.meta.api.objects.InputFile;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import java.awt.Color;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.*;
import java.util.stream.Collectors;

@Slf4j
@Service
public class ComplaintsService {


    private final MessageRepo messageRepo;
    private final MessageToConsultantRepo userComplaintRepo;

    private XSSFWorkbook workbook = new XSSFWorkbook();
    private XSSFCellStyle style = workbook.createCellStyle();
    private Language currentLanguage = Language.ru;
    private Sheet sheets;
    private Sheet sheet;
    private int i = 0;
    private DefaultAbsSender bot;
    PropertiesRepo propertiesRepo = TelegramBotRepositoryProvider.getPropertiesRepo();

    @Value(value = "${path_value}")
    private String path;

    public ComplaintsService(MessageRepo messageRepo, MessageToConsultantRepo userComplaintRepo) {
        this.messageRepo = messageRepo;
        this.userComplaintRepo = userComplaintRepo;
    }

    public void sendComplaitsService(long chatId, DefaultAbsSender bot, Date dateBegin, Date dateEnd, int messagePrevReport) {
        currentLanguage = LanguageService.getLanguage(chatId);
        try {
            this.bot = bot;
            sendCompReport(chatId, dateBegin, dateEnd, messagePrevReport);
        } catch (Exception e) {
            log.error("Can't create/send report", e);
            try {
                bot.execute(new SendMessage(String.valueOf(chatId), "Ошибка при создании отчета"));
            } catch (TelegramApiException ex) {
                log.error("Can't send message", ex);
            }
        }
    }

    private void sendCompReport(long chatId, Date dateBegin, Date dateEnd, int messagePrevReport) throws TelegramApiException, IOException {
        sheets = workbook.createSheet("Отчет по консультациям");
        sheet = workbook.getSheetAt(0);
        List<MessageToConsultant> reports = userComplaintRepo.findAllByDateBetweenOrderById(dateBegin, dateEnd);
        if (reports == null || reports.size() == 0) {
            bot.execute(new DeleteMessage(String.valueOf(chatId), messagePrevReport));
            bot.execute(new SendMessage(String.valueOf(chatId), "За выбранный период заявки отсутствуют"));
            return;
        }
        BorderStyle thin = BorderStyle.THIN;
        short black = IndexedColors.BLACK.getIndex();
        XSSFCellStyle styleTitle = setStyle(workbook, thin, black, style);
        int rowIndex = 0;
        createTitle(styleTitle, rowIndex, Arrays.asList
                (("№;" +
                        "ФИО отправителя;" +
                        "Номер телефона;" +
                        "Сообщение от отправителя;" +
                        "Дата;" +
                        "Ответ от консультанта;" +
                        "Дата ответа от консультанта;").split(Const.SPLIT)));
       // createTitle(styleTitle, rowIndex, Arrays.asList("№;ФИО;Название Компании;Номер телефона;Тип жалоба;Жалоб;Дата;Ответ от оператора;Дата ответа от оператора;Файлы от пользователя;Файлы от оператора".split(Const.SPLIT)));
        List<List<String>> info = reports.stream().map(x -> {
            List<String> list = new ArrayList<>();
            list.add(getNumber());
            list.add(x.getUser().getFullName());
            list.add(x.getUser().getPhone());
            list.add(x.getOffer());
            list.add(CZN.util.DateUtil.getDayDate(x.getDate()));
            list.add(x.getAnswer());
            try {
                list.add(CZN.util.DateUtil.getDayDate(x.getDateEmplyee()));
            } catch (Exception e) {
                e.printStackTrace();
                list.add(" ");
            }
            return list;
        }).collect(Collectors.toList());
        addInfo(info, rowIndex);
        sendFile(chatId, bot, dateBegin, dateEnd);
    }

    private String getFileLinks(Map<String, FileType> file) {

        StringBuilder s = new StringBuilder();

        for (Map.Entry<String, FileType> map : file.entrySet()) {

            s.append("https://api.telegram.org/file/bot").append(propertiesRepo.findFirstById(Const.BOT_TOKEN).getValue()).append("/").append(uploadFile(map.getKey())).append("\n");
        }

        return s.toString();

    }

    private String getNumber() {
        i++;
        return String.valueOf(i);
    }

    private void addInfo(List<List<String>> reports, int rowIndex) {
        int cellIndex;
        for (List<String> report : reports) {
            sheets.createRow(++rowIndex);
            insertToRow(rowIndex, report, style);
        }
        cellIndex = 0;
        sheets.autoSizeColumn(cellIndex++);
        sheets.setColumnWidth(cellIndex++, 4000);
        sheets.setColumnWidth(cellIndex++, 4000);
        sheets.setColumnWidth(cellIndex++, 4000);
        sheets.autoSizeColumn(cellIndex++);
    }

    private void createTitle(XSSFCellStyle styleTitle, int rowIndex, List<String> title) {
        sheets.createRow(rowIndex);
        insertToRow(rowIndex, title, styleTitle);
    }

    private void insertToRow(int row, List<String> cellValues, CellStyle cellStyle) {
        int cellIndex = 0;
        for (String cellValue : cellValues) {
            addCellValue(row, cellIndex++, cellValue, cellStyle);
        }
    }

    private void addCellValue(int rowIndex, int cellIndex, String cellValue, CellStyle cellStyle) {
        sheets.getRow(rowIndex).createCell(cellIndex).setCellValue(getString(cellValue));
        sheet.getRow(rowIndex).getCell(cellIndex).setCellStyle(cellStyle);
    }

    private String getString(String nullable) {
        if (nullable == null) return "";
        return nullable;
    }

    private String uploadFile(String fileId) {

        Objects.requireNonNull(fileId);

        GetFile getFile = new GetFile();
        getFile.setFileId(fileId);

        try {
            org.telegram.telegrambots.meta.api.objects.File file = bot.execute(getFile);
            return file.getFilePath();
        } catch (TelegramApiException e) {
            System.out.println(e.getMessage());
            throw new IllegalMonitorStateException();
        }
    }

    private XSSFCellStyle setStyle(XSSFWorkbook workbook, BorderStyle thin, short black, XSSFCellStyle style) {
        style.setWrapText(true);
        style.setAlignment(HorizontalAlignment.CENTER);
        style.setVerticalAlignment(VerticalAlignment.CENTER);
        style.setFillBackgroundColor(IndexedColors.BLUE.getIndex());
        style.setBorderTop(thin);
        style.setBorderBottom(thin);
        style.setBorderRight(thin);
        style.setBorderLeft(thin);
        style.setTopBorderColor(black);
        style.setRightBorderColor(black);
        style.setBottomBorderColor(black);
        style.setLeftBorderColor(black);
        BorderStyle tittle = BorderStyle.MEDIUM;

        XSSFCellStyle styleTitle = workbook.createCellStyle();
        styleTitle.setWrapText(true);
        styleTitle.setAlignment(HorizontalAlignment.CENTER);
        styleTitle.setVerticalAlignment(VerticalAlignment.CENTER);
        styleTitle.setBorderTop(tittle);
        styleTitle.setBorderBottom(tittle);
        styleTitle.setBorderRight(tittle);
        styleTitle.setBorderLeft(tittle);
        styleTitle.setTopBorderColor(black);
        styleTitle.setRightBorderColor(black);
        styleTitle.setBottomBorderColor(black);
        styleTitle.setLeftBorderColor(black);
        style.setFillForegroundColor(new XSSFColor(new Color(0, 52, 94)));
        return styleTitle;
    }

    private void sendFile(long chatId, DefaultAbsSender bot, Date dateBegin, Date dateEnd) throws IOException, TelegramApiException {
        String fileName = "Консультации за: " + CZN.util.DateUtil.getDayDate(dateBegin) + " - " + CZN.util.DateUtil.getDayDate(dateEnd) + ".xlsx";
//        String path = "C:\\test\\" + fileName;
        //path += new Date().getTime();
        path += fileName;
        File dir = new File("C:\\test\\");
        if (!dir.exists()) {
            dir.mkdir();
        }
        try (FileOutputStream stream = new FileOutputStream(path)) {
            workbook.write(stream);
        } catch (IOException e) {
            log.error("Can't send File error: ", e);
        }
        sendFile(chatId, bot, fileName, path);
    }

    private void sendFile(long chatId, DefaultAbsSender bot, String fileName, String path) throws IOException, TelegramApiException {
        File file = new File(path);
        try (FileInputStream fileInputStream = new FileInputStream(file)) {
            bot.execute(SendDocument.builder().chatId(String.valueOf(chatId)).document(new InputFile(fileInputStream, fileName)).build());
        }
        file.delete();
    }
}
