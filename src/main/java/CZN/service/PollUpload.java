package CZN.service;

import CZN.model.custom.userMenu.PollAnswer;
import CZN.model.custom.userMenu.PollInterview;
import CZN.model.custom.userMenu.PollQuestion;
import CZN.repository.TelegramBotRepositoryProvider;
import CZN.repository.userMenu.PollAnswerRepo;
import CZN.repository.userMenu.PollQuestionRepo;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.telegram.telegrambots.bots.DefaultAbsSender;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;


import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Slf4j
public class PollUpload {
    long chatId;
    File file;
    PollInterview interview;
    int langId;

    public PollInterview sendServiceReport(long chatId, DefaultAbsSender bot, File file, PollInterview interview, int langId) throws TelegramApiException {
        try {
            this.chatId = chatId;
            this.file = file;
            this.interview = interview;
            this.langId = langId;
            return importExel();
        } catch (Exception e) {
            log.error("Can't create/send report", e);
            try {
                bot.execute(new SendMessage(String.valueOf(chatId), "Ошибка при импортировании"));
            } catch (TelegramApiException ex) {
                log.error("Can't send message", ex);
            }
            return null;
        }
    }


    private PollInterview importExel() throws IOException, InvalidFormatException {

        XSSFWorkbook workbook = new XSSFWorkbook(file);

        XSSFSheet sheet = workbook.getSheetAt(0);
        int k = 0, l;
        boolean isContains;

        List<PollQuestion> questions;

        PollAnswerRepo answerRepo = TelegramBotRepositoryProvider.getPollAnswerRepo();
        PollQuestionRepo questionRepo = TelegramBotRepositoryProvider.getPollQuestionRepo();

        questions = getQuestions();

        if (questions.size() == 0) {
            isContains = false;
            questions = new ArrayList<>();
        } else {
            isContains = true;
        }

        System.out.println(questions.size());

        for (int i = 0; i < sheet.getLastRowNum() + 1; i++) {
            Row row = sheet.getRow(i);
            if (langId == 1) {
                if (isContains) {
                    questions.get(k).setNameRU(row.getCell(0).getStringCellValue());
                } else {
                    PollQuestion question = new PollQuestion();
                    question.setAnswers(new ArrayList<>());
                    question.setNameRU(row.getCell(0).getStringCellValue());
                    questions.add(k, question);
                }
            }
            if (langId == 2) {
                if (isContains) {
                    questions.get(k).setNameKZ(row.getCell(0).getStringCellValue());
                } else {
                    PollQuestion question = new PollQuestion();
                    question.setAnswers(new ArrayList<>());
                    question.setNameKZ(row.getCell(0).getStringCellValue());
                    questions.add(k, question);
                }
            }

            l = 0;

            for (int j = 1; j < row.getLastCellNum(); j++) {
                if (langId == 1) {
                    if (isContains) {
                        PollAnswer answer = questions.get(k).getAnswers().get(l);
                        answer.setNameRU(getStringValue(row, j));
                        answerRepo.saveAndFlush(answer);
                    } else {
                        PollAnswer answer = new PollAnswer();
                        answer.setNameRU(getStringValue(row, j));
                        answer = answerRepo.saveAndFlush(answer);
                        questions.get(k).getAnswers().add(l, answer);
                    }
                }
                if (langId == 2) {
                    if (isContains) {
                        PollAnswer answer = questions.get(k).getAnswers().get(l);
                        answer.setNameKZ(getStringValue(row, j));
                        answerRepo.saveAndFlush(answer);
                        //questions.get(k).getAnswers().get(l).setNameKZ(getStringValue(row, j));
                    } else {
                        PollAnswer answer = new PollAnswer();
                        answer.setNameKZ(getStringValue(row, j));
                        answer = answerRepo.saveAndFlush(answer);
                        questions.get(k).getAnswers().add(l, answer);
                    }
                }
                l++;
            }

            k++;

        }

        List<PollQuestion> questionList = new ArrayList<>();

        for (PollQuestion question : questions) {
            questionList.add(questionRepo.saveAndFlush(question));
        }

        interview.setQuestions(questionList);
        return interview;
    }

    private List<PollQuestion> getQuestions() {

        Set<PollQuestion> questionSet = new HashSet<>(interview.getQuestions());

        List<PollQuestion> questions = new ArrayList<>(questionSet);

        for (int i = 0; i < questions.size(); i++) {
            for (int j = 1 + i; j < questions.size(); j++) {
                PollQuestion temp = questions.get(i);
                if (temp.getId() > questions.get(j).getId()) {
                    PollQuestion x = questions.get(j);
                    questions.set(i, x);
                    questions.set(j, temp);
                }
            }
        }

        return questions;

    }

    private String getStringValue(Row row, int i) {
        try {
            return row.getCell(i).getStringCellValue();
        } catch (Exception e) {
            return getNumericValue(row, i);
        }
    }

    private String getNumericValue(Row row, int i) {
        Double phoneDouble;
        try {
            phoneDouble = row.getCell(i).getNumericCellValue();
            return String.valueOf(phoneDouble.longValue());
        } catch (Exception e) {
            return "";
        }
    }

}
