package CZN.service;


import CZN.model.custom.userMenu.Poll;
import CZN.model.custom.userMenu.PollInterview;
import CZN.model.custom.userMenu.PollQuestion;
import CZN.repository.TelegramBotRepositoryProvider;
import CZN.repository.userMenu.PollAnswerRepo;
import CZN.repository.userMenu.PollQuestionRepo;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Service;
import org.telegram.telegrambots.bots.DefaultAbsSender;
import org.telegram.telegrambots.meta.api.methods.send.SendDocument;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.InputFile;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;

@Slf4j
@Service
public class PollService {


    private XSSFWorkbook workbook = new XSSFWorkbook();
    private XSSFCellStyle style = workbook.createCellStyle();
    private Sheet sheet;

    private String path = "C:\\test";

    public void sendPollService(long chatId, DefaultAbsSender bot, Date dateBegin, Date dateEnd, List<Poll> polls, PollInterview interview) {
        try {
            sendCompReport(chatId, bot, dateBegin, dateEnd, polls, interview);
        } catch (Exception e) {
            log.error("Can't create/send report", e);
            try {
                bot.execute(new SendMessage(String.valueOf(chatId), "Ошибка при создании отчета"));
            } catch (TelegramApiException ex) {
                log.error("Can't send message", ex);
            }
        }
    }

    private void sendCompReport(long chatId, DefaultAbsSender bot, Date dateBegin, Date dateEnd, List<Poll> polls, PollInterview interview) throws TelegramApiException, IOException {
        sheet = workbook.createSheet();
        sheet = workbook.getSheetAt(0);


        if (polls == null || polls.size() == 0) {
            bot.execute(new SendMessage(String.valueOf(chatId), "За выбранный период отсутствует опрос"));
            return;
        }
        int rowIndex = 0;

        Map<Integer, Integer> map = new HashMap<>();

        sheet.createRow(rowIndex);

        sheet.getRow(rowIndex).createCell(0).setCellValue("№");
        sheet.getRow(rowIndex).getCell(0).setCellStyle(setStyle());

        sheet.getRow(rowIndex).createCell(1).setCellValue("ФИО");
        sheet.getRow(rowIndex).getCell(1).setCellStyle(setStyle());

        sheet.getRow(rowIndex).createCell(2).setCellValue("Номер телефона");
        sheet.getRow(rowIndex).getCell(2).setCellStyle(setStyle());

        sheet.getRow(rowIndex).createCell(3).setCellValue("Дата");
        sheet.getRow(rowIndex).getCell(3).setCellStyle(setStyle());


        int cellIndex = 4;

        List<PollQuestion> q = getQuestions(interview);

        for (PollQuestion question : q) {

            sheet.getRow(rowIndex).createCell(cellIndex).setCellValue(question.getNameRU());
            sheet.getRow(rowIndex).getCell(cellIndex).setCellStyle(setStyle());

            map.put(question.getId(), cellIndex);

            cellIndex++;
        }

        rowIndex++;

        String s;
        PollAnswerRepo answerRepo = TelegramBotRepositoryProvider.getPollAnswerRepo();

        for (Poll poll : polls) {
            sheet.createRow(rowIndex);

            sheet.getRow(rowIndex).createCell(0).setCellValue(rowIndex);
            sheet.getRow(rowIndex).getCell(0).setCellStyle(setStyle());

            sheet.getRow(rowIndex).createCell(1).setCellValue("Анонимный пользователь");
            sheet.getRow(rowIndex).getCell(1).setCellStyle(setStyle());

            sheet.getRow(rowIndex).createCell(2).setCellValue("Номер скрыт");
            sheet.getRow(rowIndex).getCell(2).setCellStyle(setStyle());

            sheet.getRow(rowIndex).createCell(3).setCellValue(new SimpleDateFormat("dd.MM.yyyy HH:mm").format(poll.getDate()));
            sheet.getRow(rowIndex).getCell(3).setCellStyle(setStyle());

            for (Map.Entry<Integer, Integer> entry : poll.getAnswers().entrySet()) {
                if (poll.getAnswersReason().get(entry.getKey()) != null) {
                    s = answerRepo.findById(entry.getValue()).get().getNameRU() + "\n" + poll.getAnswersReason().get(entry.getKey());
                } else s = String.valueOf(answerRepo.findById(entry.getValue()).get().getNameRU());
                sheet.getRow(rowIndex).createCell(map.get(entry.getKey())).setCellValue(s);
                sheet.getRow(rowIndex).getCell(map.get(entry.getKey())).setCellStyle(setStyle());
            }

            rowIndex++;
        }

        for (int i = 0; i < q.size() + 2; i++)
            sheet.autoSizeColumn(i);

        sendFile(chatId, bot, dateBegin, dateEnd);
    }

    private List<PollQuestion> getQuestions(PollInterview interview) {

        Set<PollQuestion> questionSet = new HashSet<>(interview.getQuestions());

        List<PollQuestion> questions = new ArrayList<>(questionSet);

        for (int i = 0; i < questions.size(); i++) {
            for (int j = 1 + i; j < questions.size(); j++) {
                PollQuestion temp = questions.get(i);
                if (temp.getId() > questions.get(j).getId()) {
                    PollQuestion x = questions.get(j);
                    questions.set(i, x);
                    questions.set(j, temp);
                }
            }
        }

        return questions;

    }

    private XSSFCellStyle setStyle() {

        short black = IndexedColors.BLACK.getIndex();
        BorderStyle tittle = BorderStyle.MEDIUM;

        XSSFCellStyle styleTitle = workbook.createCellStyle();
        styleTitle.setWrapText(true);
        styleTitle.setAlignment(HorizontalAlignment.CENTER);
        styleTitle.setVerticalAlignment(VerticalAlignment.CENTER);
        styleTitle.setBorderTop(tittle);
        styleTitle.setBorderBottom(tittle);
        styleTitle.setBorderRight(tittle);
        styleTitle.setBorderLeft(tittle);
        styleTitle.setTopBorderColor(black);
        styleTitle.setRightBorderColor(black);
        styleTitle.setBottomBorderColor(black);
        styleTitle.setLeftBorderColor(black);
        return styleTitle;
    }

    private void sendFile(long chatId, DefaultAbsSender bot, Date dateBegin, Date dateEnd) throws IOException, TelegramApiException {
        String fileName = "Опрос.xlsx";
        path += fileName;
        File dir = new File("C:\\test\\");
        if (!dir.exists()) {
            dir.mkdir();
        }
        try (FileOutputStream stream = new FileOutputStream(path)) {
            workbook.write(stream);
        } catch (IOException e) {
            log.error("Can't send File error: ", e);
        }
        sendFile(chatId, bot, fileName, path);
    }

    private void sendFile(long chatId, DefaultAbsSender bot, String fileName, String path) throws IOException, TelegramApiException {
        File file = new File(path);
        try (FileInputStream fileInputStream = new FileInputStream(file)) {
            bot.execute(SendDocument.builder().chatId(String.valueOf(chatId)).document(new InputFile(fileInputStream, fileName)).build());
        }
        file.delete();
    }
}
