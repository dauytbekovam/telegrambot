package CZN.command;

import CZN.command.impl.*;
import CZN.command.impl.adminMenu.*;
//import CZN.command.impl.consultantMenu.id011_AnswerEmployeeForOffer;
import CZN.command.impl.consultantMenu.id015_ConsultantMenu;
import CZN.command.impl.consultantMenu.id016_Active;
import CZN.command.impl.userMenu.*;
import CZN.exceptions.NotRealizedMethodException;

public class CommandFactory {

    public static Command getCommand(long id) {
        Command result = getCommandWithoutReflection((int) id);
        if (result == null) throw new NotRealizedMethodException("Not realized for type: " + id);
        return result;
    }

    private static Command getCommandWithoutReflection(int id) {
        switch (id) {
            case 1:
                return new id001_ShowInfo();
            case 2:
                return new id002_Registration();
            case 3:
                return new id003_SelectionLanguage();
            case 4:
                return new id004_FileOrPhoto();
            case 5:
                return new id005_ShowAdminInfo();
            case 6:
                return new id006_EditAdmin();
            case 7:
                return new id007_Info();
            case 8:
                return new id008_Testing();
            case 9:
                return new id009_Problem();
            case 10:
                return new id010_AboutProjectInfo();
            case 11:
                return new id0011_AnswerEmployeeForMessageToConsultant();
            case 12:
                return new id012_Interview();
            case 13:
                return new id013_SheduleChat();
            case 14:
                return new id014_ConsultantContact();
            case 15:
                return new id015_ConsultantMenu();
            case 16:
                return new id016_Active();
            case 17:
                return new id017_EditMenuButtons();
            case 18:
                return new id018_EditEmployee();
            case 19:
                return new id019_SendMessage();
            case 20:
                return new id020_SendMessageSheduled();
            case 21:
                return new id021_EditMenuMessages();
            case 22:
                return new id022_EditQuestionAnswers();
            case 23:
                return new id023_EditConsultant();
            case 24:
                return new id024_ReportPoll();
            case 25:
                return new id025_ReportConsulting();
            case 26:
                return new id026_RegistrationUser();
            case 27:
                return new id027_Get_Chat();


        }
        return null;
    }

}
