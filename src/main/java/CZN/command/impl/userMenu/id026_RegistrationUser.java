package CZN.command.impl.userMenu;

import CZN.command.Command;
import CZN.service.RegistrationUserService;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;


public class id026_RegistrationUser extends Command{

    int deleteId;
    int wrongDeleteId;
    private RegistrationUserService registrationUserService = new RegistrationUserService();;

    @Override
    public boolean execute() throws TelegramApiException {
        if (!isRegistered()) {
            if (!registrationUserService.isRegistration(update, botUtils)) {
                return COMEBACK;
            } else {
                usersRepo.save(registrationUserService.getUser());
            }
        }
        sendMessageWithAddition(); // выбор языка интерфейса
        deleteMessages();
        return EXIT;
    }

    private void deleteMessages() {
        deleteMessage(deleteId);
        deleteMessage(updateMessageId);
        deleteMessage(wrongDeleteId);
    }
}
