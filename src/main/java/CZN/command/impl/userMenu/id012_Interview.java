package CZN.command.impl.userMenu;

import CZN.command.Command;
import CZN.enums.WaitingType;
import CZN.exceptions.ButtonNotFoundException;
import CZN.exceptions.CommandNotFoundException;
import CZN.exceptions.KeyboardNotFoundException;
import CZN.exceptions.MessageNotFoundException;
import CZN.model.custom.userMenu.PollInterview;
import CZN.model.custom.userMenu.Poll;
import CZN.model.custom.userMenu.PollAnswer;
import CZN.model.custom.userMenu.PollQuestion;
import CZN.util.ButtonsLeaf;
import com.itextpdf.text.DocumentException;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.SQLException;
import java.util.*;

public class id012_Interview extends Command {

    int deleteId;
    int wrongDeleteId;
    PollInterview interview;
    Poll poll;
    int i = 0;
    int sum = 0;

    @Override
    public boolean execute() throws TelegramApiException, IOException, SQLException, FileNotFoundException, MessageNotFoundException, KeyboardNotFoundException, ButtonNotFoundException, CommandNotFoundException, DocumentException {
        switch (waitingType) {
            case START:
                deleteMessages();
                deleteUpdateMess();
                if (isButton(44)) {
                    poll = new Poll();
                    poll.setDate(new Date());
                    return getInterviews();
                }
                return COMEBACK;
            case CHOOSE_OPTION:
                if (hasCallbackQuery()) {
                    deleteMessages();
                    deleteUpdateMess();
                    if (isButton(19)) {
                        deleteMessages();
                        deleteUpdateMess();
                        return EXIT;
                    } else {
                        deleteMessages();
                        deleteUpdateMess();
                        interview = interviewRepo.findById(Integer.parseInt(updateMessageText));
                        poll.setInterview(interview.getId());
                        interview.setQuestions(getQuestions());
                        return getInterviewQuestion();
                    }
                } else {
                    deleteMessages();
                    deleteUpdateMess();
                    sendWrongData();
                    return getInterviews();
                }
            case WRITE_ANSWER:
                if (hasCallbackQuery()) {
                    deleteMessages();
                    deleteUpdateMess();
                    if (isButton(39)) {
                        if (i == 0) {
                            deleteMessages();
                            deleteUpdateMess();
                            return getInterviews();
                        } else {
                            deleteMessages();
                            deleteUpdateMess();
                            i--;
                            return getInterviewQuestion();
                        }
                    }
                    PollAnswer answer = pollAnswerRepo.findById(Integer.parseInt(updateMessageText));
                    if (poll.getAnswers() == null)
                        poll.setAnswers(new HashMap<>());
                    poll.getAnswers().put(interview.getQuestions().get(i).getId(), answer.getId());
                    try {
                        if (Integer.parseInt(answer.getNameKZ()) < 7) {
                            return getReason();
                        }
                    } catch (Exception ignored) {
                    }
                    i++;
                    if (interview.getQuestions().size() != i)
                        return getInterviewQuestion();
                    else {
                        deleteMessages();
                        deleteUpdateMess();
                        pollRepo.save(poll);
                        int point = 0;
                        for (Map.Entry<Integer, Integer> entry : poll.getAnswers().entrySet()) {

                            point += pollAnswerRepo.findById(entry.getValue().intValue()).getPoint();

                        }
                        String s = getText(82) + usersRepo.findByChatId(chatId).getFullName() +
                                next + getText(83);
                        String resultText = "";

                        if(poll.getInterview() == 1){
                             resultText = "";
                        }else if(poll.getInterview() == 2){
                            resultText = "0-7 баллов ® «норма» (отсутствие достоверно выраженных симптомов тревоги и депрессии)\n" +
                                    "\n 8-10 баллов ® «субклинически выраженная тревога / депрессия»\n" +
                                    "\n 11 баллов и выше ® «клинически выраженная тревога / депрессия»";
                        }else if(poll.getInterview() == 3){
                            resultText = "18–16 баллов ® У Вас высокая приверженность терапии. Продолжая принимать регулярно препараты, Вы не можете  передать вирус своим половым партнерам. Продолжайте заботиться о своем здоровье и здоровье своих близких! \n"+
                            "\n16–14 баллов ® Умеренная приверженность терапии. Вы можете добиться высокой эффективности антиретровирусной терапии, если поработаете со своим графиком приема препарата, будете использовать «напоминалки» (например, установить время на телефоне). Если будете пропускать прием препаратов, может развиться лекарственная устойчивость ВИЧ и терапия может стать неэффективной. Неопределяемая вирусная нагрузка – залог высокого качества жизни!  \n"
                            + "\n Менее 14 баллов  ® Низкая приверженность  лечению .  Вы принимаете препараты нерегулярно,  но можете добиться высокой эффективности антиретровирусной терапии, если поработаете со своим графиком приема препарата, будете использовать «напоминалки», советоваться с «товарищем по лечению». Для эффективного контроля ВИЧ-инфекции важно регулярно сдавать  лабораторные анализы на – вирусную нагрузку. При регулярно приеме препаратов,  Вы добьетесь неопределяемого уровня вирусной нагрузки и не сможете передать ВИЧ своему половому партнеру, таким образом Вы заботитесь о своих близких!";
                        }

                        sendMessage(s + "\n \n Количество баллов - "+ point + " \n \n" + resultText);
                        return EXIT;
                    }
                } else {
                    deleteMessages();
                    deleteUpdateMess();
                    sendWrongData();
                    return getInterviewQuestion();
                }
            case WRITE_ANS:
                if (hasCallbackQuery()) {
                    deleteMessages();
                    deleteUpdateMess();
                    if (isButton(39)) {
                        return getInterviewQuestion();
                    }
                } else if (hasMessageText()) {
                    deleteMessages();
                    deleteUpdateMess();
                    i++;
                    if (interview.getQuestions().size() != i)
                        return getInterviewQuestion();
                    else {
                        deleteMessages();
                        deleteUpdateMess();
                        poll = pollRepo.save(poll);
                        System.out.println(poll);
                        int point = 0;
                        for (Map.Entry<Integer, Integer> entry : poll.getAnswers().entrySet()) {

                            point += pollAnswerRepo.findById(entry.getValue().intValue()).getPoint();

                        }
                        String s = getText(82) + usersRepo.findByChatId(chatId).getFullName() +
                                next + getText(83);
                        sendMessage(s + point);
                        return EXIT;
                    }
                } else {
                    deleteMessages();
                    deleteUpdateMess();
                    sendWrongData();
                    return getReason();
                }
        }
        deleteMessages();
        deleteUpdateMess();
        return EXIT;
    }

    private String getQuestion() {
        if (getLangId() == 1)
            return interview.getQuestions().get(i).getNameRU();
        else return interview.getQuestions().get(i).getNameKZ();
    }

    private List<PollQuestion> getQuestions() {

        Set<PollQuestion> questionSet = new HashSet<>(interview.getQuestions());

        List<PollQuestion> questions = new ArrayList<>(questionSet);

        for (int i = 0; i < questions.size(); i++) {
            for (int j = 1 + i; j < questions.size(); j++) {
                PollQuestion temp = questions.get(i);
                if (temp.getId() > questions.get(j).getId()) {
                    PollQuestion x = questions.get(j);
                    questions.set(i, x);
                    questions.set(j, temp);
                }
            }
        }

        return questions;

    }

    private List<PollAnswer> getAnswers() {

        List<PollAnswer> answers = interview.getQuestions().get(i).getAnswers();

        for (int i = 0; i < answers.size(); i++) {
            for (int j = 1 + i; j < answers.size(); j++) {
                PollAnswer temp = answers.get(i);
                if (temp.getId() > answers.get(j).getId()) {
                    PollAnswer x = answers.get(j);
                    answers.set(i, x);
                    answers.set(j, temp);
                }
            }
        }

        return answers;

    }

    private void sendWrongData() throws TelegramApiException {
        wrongDeleteId = sendMessage(18);
    }

    private boolean getInterviews() throws TelegramApiException {

        List<PollInterview> interviewList = interviewRepo.findAllByOrderById();

        List<String> names = new ArrayList<>();
        List<String> ids = new ArrayList<>();

        for (PollInterview interview : interviewList) {
            if (getLangId() == 1) {
                names.add(interview.getNameRU());
                ids.add(String.valueOf(interview.getId()));
            } else {
                names.add(interview.getNameKZ());
                ids.add(String.valueOf(interview.getId()));
            }
        }

        names.add(buttonRepo.findByIdAndLangId(39, getLangId()).getName());
        ids.add(buttonRepo.findByIdAndLangId(39, getLangId()).getName());

        ButtonsLeaf buttonsLeaf = new ButtonsLeaf(names, ids);

        deleteId = sendMessageWithKeyboard(getText(200), buttonsLeaf.getListButtonWithDataList());

        waitingType = WaitingType.CHOOSE_OPTION;

        return COMEBACK;

    }

    private boolean getInterviewQuestion() throws TelegramApiException {


        List<String> names = new ArrayList<>();
        List<String> ids = new ArrayList<>();

        List<PollAnswer> answers = getAnswers();

        for (PollAnswer question : answers) {
            if (getLangId() == 1) {
                names.add(question.getNameRU());
                ids.add(String.valueOf(question.getId()));
            } else if (getLangId() == 2) {
                names.add(question.getNameKZ());
                ids.add(String.valueOf(question.getId()));
            }
        }

        names.add(buttonRepo.findByIdAndLangId(39, getLangId()).getName());
        ids.add(buttonRepo.findByIdAndLangId(39, getLangId()).getName());

        ButtonsLeaf buttonsLeaf = new ButtonsLeaf(names, ids);

        deleteId = sendMessageWithKeyboard(getQuestion(), buttonsLeaf.getListButtonWithDataList());

        waitingType = WaitingType.WRITE_ANSWER;

        return COMEBACK;

    }

    private boolean getReason() throws TelegramApiException {
        deleteId = sendMessageWithKeyboard(getText(74), 40);
        waitingType = WaitingType.WRITE_ANS;
        return COMEBACK;
    }

    private void deleteMessages() {
        deleteMessage(deleteId);
        deleteMessage(updateMessageId);
        deleteMessage(wrongDeleteId);
    }

    private void deleteUpdateMess() {
        deleteMessage(updateMessageId);
    }
}
