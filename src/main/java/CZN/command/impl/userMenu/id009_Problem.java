package CZN.command.impl.userMenu;

import CZN.command.Command;
import CZN.enums.WaitingType;
import CZN.model.custom.userMenu.ProblemCategory;
import CZN.model.custom.userMenu.ProblemSubCategory;
import CZN.util.ButtonsLeaf;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import java.util.ArrayList;
import java.util.List;

public class id009_Problem extends Command {

    int deleteId;
    int wrongDeleteId;

    @Override
    public boolean execute() throws TelegramApiException {

        switch (waitingType){
            case START:
                if(isButton(42)){
                    deleteMessages();
                    deleteUpdateMess();
                    getAllCategories();
                } else return COMEBACK;
                return COMEBACK;
            case CHOICE_QUESTION:
                deleteMessages();
                deleteUpdateMess();
                if(hasCallbackQuery()){
                    if(isButton(19)){
                        deleteMessages();
                        deleteUpdateMess();
                        getAllCategories();
                        return COMEBACK;
                    }else {
                        deleteMessages();
                        deleteUpdateMess();
                        System.out.println(updateMessageText);
                        ProblemSubCategory subCategory = problemCategoryRepo.findById(Integer.parseInt(updateMessageText)).getProblemSubCategory();
                        if (getLangId() == 1)
                            sendMessageWithKeyboard(subCategory.getNameRus(),6);
                        else sendMessageWithKeyboard(subCategory.getNameKaz(),6);
                    }
                }
                return COMEBACK;
        }
        deleteMessages();
        deleteUpdateMess();
        return EXIT;
    }

    private void getAllCategories() throws TelegramApiException {
        List<ProblemCategory> categories = problemCategoryRepo.findAllByOrderById();
        List<String> names = new ArrayList<>();
        List<String> ids = new ArrayList<>();
        for (ProblemCategory testCategory : categories) {
            if (getLangId() == 1)
                names.add(testCategory.getNameRus());
            else names.add(testCategory.getNameKaz());
            ids.add(String.valueOf(testCategory.getId()));
        }
        names.add(buttonRepo.findByIdAndLangId(19, getLangId()).getName());
        ids.add(buttonRepo.findByIdAndLangId(19, getLangId()).getName());
        ButtonsLeaf buttonsLeaf = new ButtonsLeaf(names, ids);

        toDeleteMessage(sendMessageWithKeyboard(getText(35), buttonsLeaf.getListButtonWithDataList()));
        waitingType = WaitingType.CHOICE_QUESTION;
    }

    private void deleteMessages() {
        deleteMessage(deleteId);
        deleteMessage(updateMessageId);
        deleteMessage(wrongDeleteId);
    }

    private void deleteUpdateMess() {
        deleteMessage(updateMessageId);
    }

}
