package CZN.command.impl.userMenu;

import CZN.command.Command;
import CZN.enums.FileType;
import CZN.enums.WaitingType;
import CZN.exceptions.ButtonNotFoundException;
import CZN.exceptions.CommandNotFoundException;
import CZN.exceptions.KeyboardNotFoundException;
import CZN.exceptions.MessageNotFoundException;
import CZN.model.custom.userMenu.MessageToConsultant;
import CZN.model.standart.Employee;
import com.itextpdf.text.DocumentException;
import org.telegram.telegrambots.meta.api.methods.send.SendDocument;
import org.telegram.telegrambots.meta.api.methods.send.SendMediaGroup;
import org.telegram.telegrambots.meta.api.methods.send.SendPhoto;
import org.telegram.telegrambots.meta.api.methods.send.SendVideo;
import org.telegram.telegrambots.meta.api.objects.InputFile;
import org.telegram.telegrambots.meta.api.objects.media.InputMedia;
import org.telegram.telegrambots.meta.api.objects.media.InputMediaDocument;
import org.telegram.telegrambots.meta.api.objects.media.InputMediaPhoto;
import org.telegram.telegrambots.meta.api.objects.media.InputMediaVideo;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.*;

public class id0011_AnswerEmployeeForMessageToConsultant extends Command {

    Map<String, FileType> files;
    MessageToConsultant toConsultantMessage = new MessageToConsultant();
    Employee employee;
    MessageToConsultant messageToConsultant;
    int deleteId;

    @Override
    public boolean execute() throws TelegramApiException, IOException, SQLException, FileNotFoundException, MessageNotFoundException, KeyboardNotFoundException, ButtonNotFoundException, CommandNotFoundException, DocumentException {
        deleteMessage();
        switch (waitingType) {
            case START:
                if (isButton(47)) {
                    deleteMessages();
                    int id;
                    try {
                        id = Integer.parseInt(update.getCallbackQuery().getMessage().getText().split("№")[1].split("\n")[0]);
                    } catch (Exception e) {
                        try {
                            id = Integer.parseInt(update.getCallbackQuery().getMessage().getCaption().split("№")[1].split("\n")[0]);
                        } catch (Exception exception) {
                            id = 0;
                        }
                    }
                    if (id == 0) {
                        deleteId = sendMessage("Ошибка при ответе");
                        return EXIT;
                    } else {
                        deleteMessages();
                        toConsultantMessage = messageToConsultantRepo.findById(id);
                        deleteId = sendMessage(getText(36));
                        waitingType = WaitingType.WRITE_ANS;
                        return COMEBACK;
                    }
                }
            case WRITE_ANS:
                if (update.hasMessage() &&
                        update.getMessage().hasText() &&
                        update.getMessage().getText().length() <= 50) {
                    deleteMessages();
                    files = new HashMap<>();
                    toConsultantMessage.setAnswer(update.getMessage().getText());
                    toConsultantMessage.setDateEmplyee(new Date());
                    toConsultantMessage.setAccepted(true);
                    sendMessageWithKeyboard(getText(71), 28);
                    waitingType = WaitingType.SET_APL;
                    return COMEBACK;
                } else {
                    sendWrongData();
                    deleteId = sendMessage(getText(36));
                    waitingType = WaitingType.WRITE_ANS;
                    return COMEBACK;
                }
            case SET_APL:
                if (hasCallbackQuery()) {
                    if (isButton(92)) {
                        sendMessageWithKeyboard(getText(131), 34);
                        waitingType = WaitingType.SET_EMPFILEAPL;
                        return COMEBACK;
                    } else if (isButton(91)) {
                        sendMessage(getText(119));
                        waitingType = WaitingType.SET_FILEAPL;
                        return COMEBACK;
                    }
                } else {
                    sendWrongData();
                    sendMessageWithKeyboard(getText(71), 28);
                    waitingType = WaitingType.SET_APL;
                    return COMEBACK;
                }
            case SET_FILEAPL:
                if (update.hasMessage() && update.getMessage().hasPhoto()) {
                    if (files.size() < 11) {
                        files.put(updateMessagePhoto, FileType.photo);
                        sendMessage(120);
                        sendMessageWithKeyboard(getText(71), 28);
                        waitingType = WaitingType.SET_APL;
                        return COMEBACK;
                    } else {
                        sendMessageWithKeyboard(getText(131), 34);
                        waitingType = WaitingType.SET_EMPFILEAPL;
                        return COMEBACK;
                    }

                } else if (update.hasMessage() && update.getMessage().hasVideo()) {
                    if (files.size() < 11) {
                        files.put(updateMessageVideo, FileType.video);
                        sendMessage(120);
                        sendMessageWithKeyboard(getText(71), 28);
                        waitingType = WaitingType.SET_APL;
                        return COMEBACK;
                    } else {
                        sendMessageWithKeyboard(getText(131), 34);
                        waitingType = WaitingType.SET_EMPFILEAPL;
                        return COMEBACK;
                    }

                } else if (update.hasMessage() && update.getMessage().hasDocument()) {
                    if (files.size() < 11) {
                        files.put(updateMessageDocument, FileType.document);
                        sendMessage(120);
                        sendMessageWithKeyboard(getText(71), 28);
                        waitingType = WaitingType.SET_APL;
                        return COMEBACK;
                    } else {
                        sendMessageWithKeyboard(getText(131), 34);
                        waitingType = WaitingType.SET_EMPFILEAPL;
                        return COMEBACK;
                    }
                } else {
                    sendMessageWithKeyboard(getText(71), 28);
                    waitingType = WaitingType.SET_APL;
                    return COMEBACK;
                }
            case SET_EMPFILEAPL:
                if (hasCallbackQuery()) {
                    if (isButton(97)) {
                        //files.putAll(offer.getFile());
                        toConsultantMessage.setFileEmp(files);
                        messageToConsultantRepo.save(toConsultantMessage);
                        StringBuilder stringBuilder = new StringBuilder();
                        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");

                        stringBuilder.append("№: ").append(toConsultantMessage.getId()).append(next).
                                append(getText(37)).append(next).
                                append(getText(38)).append(toConsultantMessage.getAnswer()).append(next).
                                append(getText(30)).append(simpleDateFormat.format(toConsultantMessage.getDate()));

                        int sizePhoto = 0;
                        int sizeVideo = 0;
                        int sizeDocument = 0;
                        List<String> photos = new ArrayList<>();
                        List<String> videos = new ArrayList<>();
                        List<String> document = new ArrayList<>();

                        for (Map.Entry<String, FileType> s : files.entrySet()) {
                            if (s.getValue().equals(FileType.photo)) {
                                sizePhoto++;
                                photos.add(s.getKey());
                            } else if (s.getValue().equals(FileType.video)) {
                                sizeVideo++;
                                videos.add(s.getKey());
                            } else if (s.getValue().equals(FileType.document)) {
                                sizeDocument++;
                                document.add(s.getKey());
                            }
                        }

                        List<InputMedia> media = new ArrayList<>();
                        List<InputMedia> mediaDocument = new ArrayList<>();
                        SendDocument sendDocument = new SendDocument();
                        SendVideo sendVideo = new SendVideo();
                        SendPhoto sendPhoto = new SendPhoto();
                        InputMediaPhoto inputMediaPhoto;
                        InputMediaVideo inputMediaVideo;
                        if (sizePhoto > 0 && sizeVideo > 0) {
                            for (String s : photos) {
                                inputMediaPhoto = new InputMediaPhoto();
                                inputMediaPhoto.setMedia(s);
                                media.add(inputMediaPhoto);
                            }

                            for (String s : videos) {
                                inputMediaVideo = new InputMediaVideo();
                                inputMediaVideo.setMedia(s);
                                media.add(inputMediaVideo);
                            }
                        } else if (sizePhoto > 0 && sizeVideo == 0) {
                            for (String s : photos) {
                                inputMediaPhoto = new InputMediaPhoto();
                                inputMediaPhoto.setMedia(s);
                                media.add(inputMediaPhoto);
                            }
                        } else if (sizePhoto == 0 && sizeVideo > 0) {
                            for (String s : videos) {
                                inputMediaVideo = new InputMediaVideo();
                                inputMediaVideo.setMedia(s);
                                media.add(inputMediaVideo);
                            }
                        }
                        InputMediaDocument inputMediaDocument;
                        if (sizeDocument > 0) {
                            for (String s : document) {
                                inputMediaDocument = new InputMediaDocument();
                                inputMediaDocument.setMedia(s);
                                mediaDocument.add(inputMediaDocument);
                            }

                        }

                        SendMediaGroup sendMediaGroupDoc = new SendMediaGroup();
                        SendMediaGroup sendMediaGroup = new SendMediaGroup();

                        if (mediaDocument.size() > 1) {
                            sendMediaGroupDoc.setMedias(mediaDocument);
                            sendMediaGroupDoc.setChatId(String.valueOf(toConsultantMessage.getUser().getChatId()));
                            bot.execute(sendMediaGroupDoc);
                        } else if (sizeDocument == 1) {
                            sendDocument.setDocument(new InputFile(document.get(0)));
                            sendDocument.setChatId(String.valueOf(toConsultantMessage.getUser().getChatId()));
                            bot.execute(sendDocument);
                        }

                        if (media.size() > 0) {
                            if (photos.size() == 0 && videos.size() == 1) {
                                sendVideo.setVideo(new InputFile(videos.get(0)));
                                sendVideo.setParseMode("html");
                                sendVideo.setCaption(stringBuilder.toString());
                                sendVideo.setChatId(String.valueOf(toConsultantMessage.getUser().getChatId()));
                                bot.execute(sendVideo);
                                deleteId = sendMessage(34);
                                return EXIT;
                            } else if (photos.size() == 1 && videos.size() == 0) {
                                sendPhoto.setPhoto(new InputFile(photos.get(0)));
                                sendPhoto.setParseMode("html");
                                sendPhoto.setCaption(stringBuilder.toString());
                                sendPhoto.setChatId(String.valueOf(toConsultantMessage.getUser().getChatId()));
                                bot.execute(sendPhoto);
                                deleteId = sendMessage(34);
                                return EXIT;
                            } else {
                                sendMediaGroup.setMedias(media);
                                sendMediaGroup.setChatId(String.valueOf(toConsultantMessage.getUser().getChatId()));
                                bot.execute(sendMediaGroup);
                                sendMessageWithKeyboard(stringBuilder.toString(), keyboardMarkUpService.select(10), toConsultantMessage.getUser().getChatId());
                                deleteId = sendMessage(34);
                                return EXIT;
                            }
                        } else {
                            sendMessageWithKeyboard(stringBuilder.toString(), keyboardMarkUpService.select(10), toConsultantMessage.getUser().getChatId());
                            deleteId = sendMessage(34);
                            return EXIT;
                        }
                    }
                    return COMEBACK;
                }
                return COMEBACK;
        }
        return EXIT;
    }

    private void deleteMessages() {
        deleteMessage(updateMessageId);
        deleteMessage(deleteId);
    }

    private void sendWrongData() throws TelegramApiException {
        toDeleteMessage(sendMessage(18));
    }
}
