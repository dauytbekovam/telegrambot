package CZN.command.impl.userMenu;

import CZN.command.Command;
import CZN.enums.WaitingType;
import CZN.model.custom.userMenu.TestCategory;
import CZN.model.custom.userMenu.TestSubCategory;
import CZN.util.ButtonsLeaf;
import org.telegram.telegrambots.meta.api.methods.send.SendVideo;
import org.telegram.telegrambots.meta.api.objects.InputFile;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import java.util.ArrayList;
import java.util.List;

public class id008_Testing extends Command {

    int deleteId;
    int wrongDeleteId;

    @Override
    public boolean execute() throws TelegramApiException {
        switch (waitingType){
            case START:
                deleteMessages();
                deleteUpdateMess();
                if (!isRegistered()){
                    deleteMessages();
                    deleteUpdateMess();
                    sendMessage("Вы не зарегистрированы, зарегистрируйтесь пожалуйста нажав на кнопку -> /start");
                }
                if(isButton(41)) {
                    deleteMessages();
                    deleteUpdateMess();
                    getAllCategories();
                }else return COMEBACK;
                return COMEBACK;
            case CHOICE_QUESTION:
                deleteMessages();
                deleteUpdateMess();
                if(hasCallbackQuery()){
                    if(isButton(19)){
                        deleteMessages();
                        getAllCategories();
                        return COMEBACK;
                    }else {
                        deleteMessages();
                        deleteUpdateMess();
                        if(Integer.parseInt(updateMessageText) == 3){
                            if (getLangId() == 1){
                                // rus
                                SendVideo sendVideo = new SendVideo();
                                sendVideo.setChatId(String.valueOf(chatId));
                                sendVideo.setVideo(new InputFile("BAACAgIAAxkBAAIfx2LnefKbP9N5IliNx72_lVnVcldhAAJZHwACs9c5S2q5UxTG186VKQQ"));
                                bot.execute(sendVideo);
                            }else if(getLangId() == 2){
                                // kaz
                                SendVideo sendVideo = new SendVideo();
                                sendVideo.setChatId(String.valueOf(chatId));
                                sendVideo.setVideo(new InputFile("BAACAgIAAxkBAAIfu2Lndu3W7b_C7VgQkYsdrWFK3ZEyAAI2HwACs9c5S0pru8bhtif8KQQ"));
                                bot.execute(sendVideo);
                            }
                        }

                        TestSubCategory subCategory = testCategoryRepo.findById(Integer.parseInt(updateMessageText)).getTestSubCategory();

                        if (getLangId() == 1)
                            sendMessageWithKeyboard(subCategory.getNameRus(),6);
                        else sendMessageWithKeyboard(subCategory.getNameKaz(),6);
                    }
                }
                return COMEBACK;
        }
        deleteMessages();
        deleteUpdateMess();
        return EXIT;
    }

    private void getAllCategories() throws TelegramApiException {
        List<TestCategory> categories = testCategoryRepo.findAllByOrderById();
        List<String> names = new ArrayList<>();
        List<String> ids = new ArrayList<>();
        for (TestCategory testCategory : categories) {
            if (getLangId() == 1)
                names.add(testCategory.getNameRus());
            else names.add(testCategory.getNameKaz());
            ids.add(String.valueOf(testCategory.getId()));
        }
        names.add(buttonRepo.findByIdAndLangId(19, getLangId()).getName());
        ids.add(buttonRepo.findByIdAndLangId(19, getLangId()).getName());
        ButtonsLeaf buttonsLeaf = new ButtonsLeaf(names, ids);

        toDeleteMessage(sendMessageWithKeyboard(getText(40), buttonsLeaf.getListButtonWithDataList()));
        waitingType = WaitingType.CHOICE_QUESTION;
    }

    private void deleteMessages() {
        deleteMessage(deleteId);
        deleteMessage(updateMessageId);
        deleteMessage(wrongDeleteId);
    }

    private void deleteUpdateMess() {
        deleteMessage(updateMessageId);
    }

}
