package CZN.command.impl.userMenu;

import CZN.command.Command;
import CZN.enums.WaitingType;
import CZN.model.custom.userMenu.InfoAnswer;
import CZN.model.custom.userMenu.InfoQuestion;
import CZN.model.custom.userMenu.Information;
import CZN.util.ButtonsLeaf;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import java.util.ArrayList;
import java.util.List;

public class id007_Info extends Command {

    private int deleteId;
    private int wrongDeleteId;
    private String getQuestionsByCategory;

    @Override
    public boolean execute() throws TelegramApiException {
        switch (waitingType){
            case START:
                deleteMessages();
                deleteUpdateMess();
                if (!isRegistered())
                    sendMessage("Вы не зарегистрированы, зарегистрируйтесь пожалуйста нажав на кнопку -> /start");
                if(isButton(40)) {
                    deleteMessages();
                    deleteUpdateMess();
                    getAllCategories();
                }else return COMEBACK;
                return COMEBACK;
            case CHOICE_QUESTION:
                deleteMessages();
                deleteUpdateMess();
                if(hasCallbackQuery()){
                    if(isButton(19)){
                        deleteMessages();
                        deleteUpdateMess();
                        getAllCategories();
                        return COMEBACK;
                    }else {
                        deleteMessages();
                        deleteUpdateMess();
                        getQuestionsByCategory = updateMessageText;
                        getQuestionsByCategory(updateMessageText);
                        return COMEBACK;
                    }
                }
                return COMEBACK;
            case CHOICE_OPTION:
                deleteMessages();
                deleteUpdateMess();
                if(hasCallbackQuery()){
                    if(isButton(19)){
                        deleteMessages();
                        deleteUpdateMess();
                        getAllCategories();
                        return COMEBACK;
                    }else {
                        deleteMessages();
                        deleteUpdateMess();
                        InfoAnswer answer = infoAnswerRepo.findByInfoQuestionId(Integer.parseInt(updateMessageText));
                        if (getLangId() == 1)
                            sendMessageWithKeyboard(answer.getAnswerRus(),6);
                        else sendMessageWithKeyboard(answer.getAnswerKaz(),6);
                        waitingType = WaitingType.CHOICE_END;
                    }
                }
                return COMEBACK;
            case CHOICE_END:
                deleteMessages();
                deleteUpdateMess();
                if(hasCallbackQuery()){
                    if(isButton(19)){
                        getQuestionsByCategory(getQuestionsByCategory);
                        return COMEBACK;
                    }
                }
        }
        deleteMessages();
        deleteUpdateMess();
        return EXIT;
    }

    private void getAllCategories() throws TelegramApiException {
        List<Information> categories = informationRepo.findAllByOrderById();
        List<String> names = new ArrayList<>();
        List<String> ids = new ArrayList<>();
        for (Information infoCategory : categories) {
            if (getLangId() == 1)
                names.add(infoCategory.getNameRus());
            else names.add(infoCategory.getNameKaz());
            ids.add(String.valueOf(infoCategory.getId()));
        }
        names.add(buttonRepo.findByIdAndLangId(19, getLangId()).getName());
        ids.add(buttonRepo.findByIdAndLangId(19, getLangId()).getName());
        ButtonsLeaf buttonsLeaf = new ButtonsLeaf(names, ids);
        toDeleteMessage(sendMessageWithKeyboard(getText(39), buttonsLeaf.getListButtonWithDataList()));
        waitingType = WaitingType.CHOICE_QUESTION;
    }

    private void getQuestionsByCategory(String userText) throws TelegramApiException{
        Information category = informationRepo.findById(Integer.parseInt(userText));
        List<InfoQuestion> questions = category.getInfoQuestions();
        List<String> names = new ArrayList<>();
        List<String> ids = new ArrayList<>();
        for (InfoQuestion question : questions) {
            if (getLangId() == 1)
                names.add(question.getQuestionRus());
            else names.add(question.getQuestionKaz());
            ids.add(String.valueOf(question.getId()));
        }
        names.add(buttonRepo.findByIdAndLangId(19, getLangId()).getName());
        ids.add(buttonRepo.findByIdAndLangId(19, getLangId()).getName());
        ButtonsLeaf buttonsLeaf = new ButtonsLeaf(names, ids);
        toDeleteKeyboard(sendMessageWithKeyboard(getText(16), buttonsLeaf.getListButtonWithDataList()));
        waitingType = WaitingType.CHOICE_OPTION;
    }

    private void deleteMessages() {
        deleteMessage(deleteId);
        deleteMessage(updateMessageId);
        deleteMessage(wrongDeleteId);
    }

    private void deleteUpdateMess() {
        deleteMessage(updateMessageId);
    }


}
