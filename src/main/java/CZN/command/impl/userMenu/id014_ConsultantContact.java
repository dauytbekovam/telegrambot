package CZN.command.impl.userMenu;

import CZN.command.Command;
import CZN.enums.FileType;
import CZN.enums.WaitingType;
import CZN.exceptions.ButtonNotFoundException;
import CZN.exceptions.CommandNotFoundException;
import CZN.exceptions.KeyboardNotFoundException;
import CZN.exceptions.MessageNotFoundException;
import CZN.model.custom.userMenu.MessageToConsultant;
import CZN.model.standart.Employee;
import CZN.model.standart.Role;
import CZN.model.standart.User;
import CZN.util.BotUtil;
import CZN.util.ButtonsLeaf;
import CZN.util.Const;
import CZN.util.UpdateUtil;
import com.itextpdf.text.DocumentException;
import org.telegram.telegrambots.meta.api.methods.send.SendDocument;
import org.telegram.telegrambots.meta.api.methods.send.SendMediaGroup;
import org.telegram.telegrambots.meta.api.methods.send.SendPhoto;
import org.telegram.telegrambots.meta.api.methods.send.SendVideo;
import org.telegram.telegrambots.meta.api.objects.InputFile;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.media.InputMedia;
import org.telegram.telegrambots.meta.api.objects.media.InputMediaDocument;
import org.telegram.telegrambots.meta.api.objects.media.InputMediaPhoto;
import org.telegram.telegrambots.meta.api.objects.media.InputMediaVideo;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.*;

public class id014_ConsultantContact extends Command {

    Map<String, FileType> files;
    private MessageToConsultant messageToConsultant;
    int deleteId;
    Employee employee;
    String role;
    int wrongDeleteId;
    private User user;

    @Override
    public boolean execute() throws TelegramApiException, IOException, SQLException, FileNotFoundException, MessageNotFoundException, KeyboardNotFoundException, ButtonNotFoundException, CommandNotFoundException, DocumentException {

        switch (waitingType){
            case START:
                deleteMessages();
                deleteUpdateMess();
                if(isButton(46)){
                    getAllRoles();
                }else if (isButton(24)) {
                    String str = update.getCallbackQuery().getMessage().getText();
                    String[] words = str.split("\n");
                    String idNumber = words[0];
                    String[] getId = idNumber.split("№: ");
                    int id = Integer.parseInt(getId[1]);
                    messageToConsultant = messageToConsultantRepo.findById(id);
                    employee = employeeRepo.findById(messageToConsultant.getEmployee().getId());
                    deleteId = sendMessage(21);
                    waitingType = WaitingType.ENTER_OFFER;
                    return COMEBACK;
                } else if (isButton(25)) {
                    sendMessage("Спасибо за обращение.");
                    return EXIT;
                }else return COMEBACK;
                return COMEBACK;
            case CHOICE_QUESTION:
                deleteMessages();
                if(hasCallbackQuery()){
                    if(isButton(19)){
                        getAllRoles();
                        return COMEBACK;
                    }else {
                        getAllEmployeeByRole(updateMessageText);
                        return COMEBACK;
                    }
                }
                return COMEBACK;
            case CHOICE_CONSULTANT:
                deleteMessages();
                if(hasCallbackQuery()){
                    if(isButton(19)){
                        getAllRoles();
                        return COMEBACK;
                    }else {
                        employee = employeeRepo.findById(Integer.parseInt(updateMessageText));
                        deleteId = sendMessage(21);
                        waitingType = WaitingType.ENTER_OFFER;
                        return COMEBACK;
                    }
                }
            case ENTER_OFFER:
                if (update.hasMessage() && update.getMessage().hasText()) {
                    deleteMessages();
                    files = new HashMap<>();
                    messageToConsultant = new MessageToConsultant();
                    User user = usersRepo.findByChatId(chatId);
                    messageToConsultant.setOffer(update.getMessage().getText());
                    messageToConsultant.setUser(user);
                    messageToConsultant.setAccepted(false);
                    messageToConsultant.setDate(new Date());
                    messageToConsultant.setEmployee(employee);
                    sendMessageWithKeyboard(getText(70)
                            + "\n" + "\n" + "" + getText(151), 31);
                    waitingType = WaitingType.SET_PHONE_NUMBER;
                    return COMEBACK;
                } else {
                    sendWrongData();
                    deleteMessages();
                    deleteUpdateMess();
                    sendMessage(21);
//                    deleteMessages();
                    waitingType = WaitingType.ENTER_OFFER;
                    return COMEBACK;
                }
            case SET_PHONE_NUMBER:
                deleteMessages();
                user = usersRepo.findByChatId(chatId);
                if(hasCallbackQuery()){
                    if(isButton(2)){
                        getPhone();
                        deleteMessages();
                        waitingType = WaitingType.SET_PHONE_NUMBER_USER;
                        return COMEBACK;
                    }else if(isButton(3)){
                        user.setPhone(getText(42));
                        messageToConsultant.setUser(user);
                        usersRepo.save(user);
                       // не трогать
                        sendMessageWithKeyboard(getText(71), 30);
                        waitingType = WaitingType.SET_FILEOFFERRR;
                        return COMEBACK;
                    } else {
                        sendWrongData();
                        deleteMessages();
                        deleteUpdateMess();
                        sendMessageWithKeyboard(getText(70)  + "\n" + "\n" + "<i>Ваши данные не будут переданы третьим " +
                                "лицам или использованы без Вашего согласия. Ваш номер нужен нам только для " +
                                "обратной связи </i>", 31);
                        waitingType = WaitingType.SET_PHONE_NUMBER;
                        return COMEBACK;
                    }
                }
            case SET_PHONE_NUMBER_USER:
                if (botUtils.hasContactOwner(update)) {
                    String phone = update.getMessage().getContact().getPhoneNumber();
                    if (update.getMessage().getContact().getPhoneNumber().startsWith("8")) {
                        phone = update.getMessage().getContact().getPhoneNumber().replaceFirst("8", "+7");
                    }
                    if (update.getMessage().getContact().getPhoneNumber().startsWith("7")) {
                        phone = update.getMessage().getContact().getPhoneNumber().replaceFirst("7", "+7");
                    }
                    user.setPhone(phone);
                    messageToConsultant.setUser(user);
                    usersRepo.save(user);
                    deleteMessages();
                    deleteUpdateMess();
                    sendMessageWithKeyboard(getText(71), 30);
                    waitingType = WaitingType.SET_FILEOFFERRR;
                    return COMEBACK;
                } else {
                    wrongData();
                    getPhone();
                    return COMEBACK;
                }
            case SET_FILEOFFERRR:
                deleteMessages();
                if (hasCallbackQuery()) {
                    if (isButton(94)) {
                        sendMessageWithKeyboard(getText(131),33);
                        waitingType = WaitingType.SET_OFFERFILES;
                        return COMEBACK;
                    } else if (isButton(93) || isButton(99)) {
                        sendMessage(getText(119));
                        waitingType = WaitingType.SET_FILEOFFERRE;
                        return COMEBACK;
                    }
                } else {
                    sendWrongData();
                    sendMessageWithKeyboard(getText(71), 30);
                    waitingType = WaitingType.SET_FILEOFFERRR;
                    return COMEBACK;
                }
            case SET_FILEOFFERRE:
                if (update.hasMessage() && update.getMessage().hasPhoto()) {
                    if (files.size()<11){
                        files.put(updateMessagePhoto,FileType.photo);
                        sendMessage(120);
                        // поменяла тут
                        sendMessageWithKeyboard(getText(71), 35);
                        waitingType = WaitingType.SET_FILEOFFERRR;
                        return COMEBACK;
                    }else {
                        sendMessageWithKeyboard(getText(132),33);
                        waitingType = WaitingType.SET_OFFERFILES;
                        return COMEBACK;
                    }
                } else if (update.hasMessage() && update.getMessage().hasVideo()) {
                    if (files.size()<11){
                        files.put(updateMessageVideo,FileType.video);
                        sendMessage(120);
                        //поменяла тут
                        sendMessageWithKeyboard(getText(71), 35);
                        waitingType = WaitingType.SET_FILEOFFERRR;
                        return COMEBACK;
                    }else {
                        sendMessageWithKeyboard(getText(132),33);
                        waitingType = WaitingType.SET_OFFERFILES;
                        return COMEBACK;
                    }
                } else if (update.hasMessage() && update.getMessage().hasDocument()) {
                    if (files.size()<11){
                        files.put(updateMessageDocument,FileType.document);
                        sendMessage(120);
                        // поменяла тут
                        sendMessageWithKeyboard(getText(71), 35);
                        waitingType = WaitingType.SET_FILEOFFERRR;
                        return COMEBACK;
                    }else {
                        sendMessageWithKeyboard(getText(132),33);
                        waitingType = WaitingType.SET_OFFERFILES;
                        return COMEBACK;
                    }
                }
            case SET_OFFERFILES:
                deleteMessages();
                if(hasCallbackQuery()){
                    if (isButton(96)){
                        messageToConsultant.setFile(files);
                        messageToConsultantRepo.save(messageToConsultant);
                        StringBuilder stringBuilder = new StringBuilder();

                        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
                        if(messageToConsultant.getUser().getPhone() == null){
                            messageToConsultant.getUser().setPhone(getText(42));
                        }
                        stringBuilder.append("№").append(messageToConsultant.getId()).append(next).
                                append(getText(27)).append(messageToConsultant.getUser().getFullName()).append(next).
                                append(getText(28)).append(messageToConsultant.getUser().getPhone()).append(next).
                                append(getText(38)).append(messageToConsultant.getOffer()).append(next).
                                append(getText(30)).append(simpleDateFormat.format(messageToConsultant.getDate()));

                        int sizePhoto = 0;
                        int sizeVideo = 0;
                        int sizeDocument = 0;
                        List<String> photos = new ArrayList<>();
                        List<String> videos = new ArrayList<>();
                        List<String> document = new ArrayList<>();

                        for (Map.Entry<String,FileType> s: files.entrySet()){
                            if (s.getValue().equals(FileType.photo)) {
                                sizePhoto++;
                                photos.add(s.getKey());
                            }else if (s.getValue().equals(FileType.video)) {
                                sizeVideo++;
                                videos.add(s.getKey());
                            }else if (s.getValue().equals(FileType.document)) {
                                sizeDocument++;
                                document.add(s.getKey());
                            }
                        }

                        List<InputMedia> media = new ArrayList<>();
                        List<InputMedia> mediaDocument = new ArrayList<>();
                        SendDocument sendDocument = new SendDocument();
                        SendVideo sendVideo = new SendVideo();
                        SendPhoto sendPhoto = new SendPhoto();
                        InputMediaPhoto inputMediaPhoto;
                        InputMediaVideo inputMediaVideo;

                        if (sizePhoto > 0 && sizeVideo > 0) {
                            for (String s : photos) {
                                inputMediaPhoto = new InputMediaPhoto();
                                inputMediaPhoto.setMedia(s);
                                media.add(inputMediaPhoto);
                            }

                            for (String s : videos) {
                                inputMediaVideo = new InputMediaVideo();
                                inputMediaVideo.setMedia(s);
                                media.add(inputMediaVideo);
                            }
                        }else if (sizePhoto>0 && sizeVideo==0){
                            for (String s : photos) {
                                inputMediaPhoto = new InputMediaPhoto();
                                inputMediaPhoto.setMedia(s);
                                media.add(inputMediaPhoto);
                            }
                        }else if (sizePhoto==0 && sizeVideo>0){
                            for (String s : videos) {
                                inputMediaVideo = new InputMediaVideo();
                                inputMediaVideo.setMedia(s);
                                media.add(inputMediaVideo);
                            }
                        }
                        InputMediaDocument inputMediaDocument;
                        if (sizeDocument > 0) {
                            for (String s : document) {
                                inputMediaDocument = new InputMediaDocument();
                                inputMediaDocument.setMedia(s);
                                mediaDocument.add(inputMediaDocument);
                            }

                        }
                        SendMediaGroup sendMediaGroupDoc = new SendMediaGroup();
                        SendMediaGroup sendMediaGroup = new SendMediaGroup();

                        if (mediaDocument.size()>1) {
                            sendMediaGroupDoc.setMedias(mediaDocument);
                            sendMediaGroupDoc.setChatId(String.valueOf(employee.getChatId()));
                            bot.execute(sendMediaGroupDoc);
                        }else if (sizeDocument==1){
                            sendDocument.setDocument(new InputFile(document.get(0)));
                            sendDocument.setChatId(String.valueOf(employee.getChatId()));
                            bot.execute(sendDocument);
                        }
                        User user = usersRepo.findByChatId(employee.getChatId());
                        if (media.size() > 0) {
                            if (photos.size() == 0 && videos.size() == 1) {
                                sendVideo.setVideo(new InputFile(videos.get(0)));
                                sendVideo.setParseMode("html");
                                sendVideo.setCaption(stringBuilder.toString());
                                sendVideo.setReplyMarkup(keyboardMarkUpService.select(9));
                                sendVideo.setChatId(String.valueOf(employee.getChatId()));
                                bot.execute(sendVideo);
                                sendMessage(getText(23) + " <b>"
                                        + messageToConsultant.getUser().getFullName() + "</b>."
                                        + "\n" + "\n" + getText(31)
                                        + " " + user.getFullName() + " Вам ответит в течение одного рабочего дня");
                                return COMEBACK;
                            }
                            else if (photos.size() == 1 && videos.size() == 0){
                                sendPhoto.setPhoto(new InputFile(photos.get(0)));
                                sendPhoto.setParseMode("html");
                                sendPhoto.setCaption(stringBuilder.toString());
                                sendPhoto.setReplyMarkup(keyboardMarkUpService.select(9));
                                sendPhoto.setChatId(String.valueOf(employee.getChatId()));
                                bot.execute(sendPhoto);
                                sendMessage(getText(23) + " <b>"
                                        + messageToConsultant.getUser().getFullName() + "</b>!"
                                        + "\n" + "\n" + getText(31)
                                        + " " + user.getFullName() + " Вам ответит в течение одного рабочего дня");
                                return COMEBACK;
                            }
                            else {
                                sendMediaGroup.setMedias(media);
                                sendMediaGroup.setChatId(String.valueOf(employee.getChatId()));
                                bot.execute(sendMediaGroup);
                                sendMessageWithKeyboard(stringBuilder.toString(), keyboardMarkUpService.select(9), employee.getChatId());
                                sendMessage(getText(23) + " <b>"
                                        + messageToConsultant.getUser().getFullName() + "</b>!"
                                        + "\n" + "\n" + getText(31)
                                        + " " + user.getFullName() + " Вам ответит в течение одного рабочего дня");
                                return COMEBACK;
                            }
                        } else {
                            sendMessageWithKeyboard(stringBuilder.toString(), keyboardMarkUpService.select(9), employee.getChatId());
                            sendMessage(getText(23) + " <b>"
                                    + messageToConsultant.getUser().getFullName() + "</b>!"
                                    + "\n" + "\n" + getText(31)
                                    + " " + user.getFullName() + " Вам ответит в течение одного рабочего дня");
                            return COMEBACK;
                        }
                    }
                }
        }
        return EXIT;
    }

    private void getAllRoles() throws TelegramApiException {
        List<Role> categories = roleRepo.findAllByOrderById();
        List<String> names = new ArrayList<>();
        List<String> ids = new ArrayList<>();
        for (Role role : categories) {
            if (getLangId() == 1)
                names.add(role.getRussianName());
            else names.add(role.getKazName());
            ids.add(String.valueOf(role.getId()));
        }
        names.add(buttonRepo.findByIdAndLangId(19, getLangId()).getName());
        ids.add(buttonRepo.findByIdAndLangId(19, getLangId()).getName());
        ButtonsLeaf buttonsLeaf = new ButtonsLeaf(names, ids);
        toDeleteMessage(sendMessageWithKeyboard(getText(22), buttonsLeaf.getListButtonWithDataList()));
        waitingType = WaitingType.CHOICE_QUESTION;
    }

    private void getAllEmployeeByRole(String userText) throws TelegramApiException{

        Role role = roleRepo.findById(Integer.parseInt(userText));
        List<Employee> allEmployees = employeeRepo.findAll();

        Set<Employee> employeesWithRole = new HashSet<>();

        List<String> names = new ArrayList<>();
        List<String> ids = new ArrayList<>();

        for (int i = 0; i < allEmployees.size(); i++) {
            Employee employee = allEmployees.get(i);
            System.out.println(employee);
            List<Role> employeeRoles = employee.getRoleList();
            System.out.println(employeeRoles);
            for (int j = 0; j < employeeRoles.size(); j++) {
                if(role.getId() == employeeRoles.get(j).getId()){
                    employeesWithRole.add(employee);
                }
            }
        }

        for (Employee employee : employeesWithRole) {
            if (getLangId() == 1)
                names.add(employee.getFullName());
            else if (getLangId() == 2){
                names.add(employee.getFullName());
            }
            ids.add(String.valueOf(employee.getId()));
        }

        names.add(buttonRepo.findByIdAndLangId(19, getLangId()).getName());
        ids.add(buttonRepo.findByIdAndLangId(19, getLangId()).getName());

        ButtonsLeaf buttonsLeaf = new ButtonsLeaf(names, ids);
        toDeleteMessage(sendMessageWithKeyboard(getText(26), buttonsLeaf.getListButtonWithDataList()));
        waitingType = WaitingType.CHOICE_CONSULTANT;
    }

    private void sendWrongData() throws TelegramApiException {
        toDeleteMessage(sendMessage(18));
    }

    private void deleteMessages() {
        deleteMessage(deleteId);
        deleteMessage(updateMessageId);
        deleteMessage(wrongDeleteId);
    }

    private int wrongData() throws TelegramApiException {
        return botUtils.sendMessage(Const.WRONG_DATA_TEXT, chatId);
    }

    private void deleteUpdateMess() {
        deleteMessage(updateMessageId);
    }

    private int getPhone() throws TelegramApiException {
        return botUtils.sendMessage(Const.SEND_CONTACT_MESSAGE, chatId);
    }

}
