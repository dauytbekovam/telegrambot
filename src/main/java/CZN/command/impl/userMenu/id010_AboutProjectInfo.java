package CZN.command.impl.userMenu;

import CZN.command.Command;
import CZN.enums.FileType;
import CZN.enums.WaitingType;
import CZN.exceptions.ButtonNotFoundException;
import CZN.exceptions.CommandNotFoundException;
import CZN.exceptions.KeyboardNotFoundException;
import CZN.exceptions.MessageNotFoundException;
import CZN.model.custom.userMenu.AboutProject;
import CZN.model.custom.userMenu.AboutProjectOffer;
import CZN.model.custom.userMenu.AboutProjectContact;
import CZN.model.custom.userMenu.AboutProjectContactInfo;
import CZN.model.standart.Admin;
import CZN.model.standart.User;
import CZN.util.ButtonsLeaf;
import com.itextpdf.text.DocumentException;
import org.telegram.telegrambots.meta.api.methods.send.SendDocument;
import org.telegram.telegrambots.meta.api.methods.send.SendMediaGroup;
import org.telegram.telegrambots.meta.api.methods.send.SendPhoto;
import org.telegram.telegrambots.meta.api.methods.send.SendVideo;
import org.telegram.telegrambots.meta.api.objects.InputFile;
import org.telegram.telegrambots.meta.api.objects.media.InputMedia;
import org.telegram.telegrambots.meta.api.objects.media.InputMediaDocument;
import org.telegram.telegrambots.meta.api.objects.media.InputMediaPhoto;
import org.telegram.telegrambots.meta.api.objects.media.InputMediaVideo;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.*;

public class id010_AboutProjectInfo extends Command {

    Map<String, FileType> files;
    private AboutProjectOffer aboutProjectOffer;
    int deleteId;
    int rate;
    int wrongDeleteId;

    @Override
    public boolean execute() throws TelegramApiException, IOException, SQLException, FileNotFoundException, MessageNotFoundException, KeyboardNotFoundException, ButtonNotFoundException, CommandNotFoundException, DocumentException {
        switch (waitingType){
            case START:
                if(isButton(43)) {
                    deleteMessages();
                    deleteUpdateMess();
                    getAllCategories();
                } else return COMEBACK;
                return COMEBACK;
            case CHOICE_QUESTION:
                if(hasCallbackQuery()){
                    if(isButton(19)){
                        deleteMessages();
                        deleteUpdateMess();
                        getAllCategories();
                        return COMEBACK;
                    }
                    else {
                        deleteMessages();
                        deleteUpdateMess();
//                        не нужный функционал
//                        if(Integer.parseInt(updateMessageText) == 1){
//                            deleteId = sendMessage("<i>Вы можете оставить свои рекомендации по улучшению бота \n \n</i>" + getText(20));
//                            waitingType = WaitingType.ENTER_OFFER;
//                            return COMEBACK;
//                        }
                        if(Integer.parseInt(updateMessageText) == 2){
                            List<AboutProjectContact> categories = aboutProjectContactRepo.findAllByOrderById();
                            List<String> names = new ArrayList<>();
                            List<String> ids = new ArrayList<>();
                            for (AboutProjectContact testCategory : categories) {
                                if (getLangId() == 1)
                                    names.add(testCategory.getNameRus());
                                else names.add(testCategory.getNameKaz());
                                ids.add(String.valueOf(testCategory.getId()));
                            }
                            names.add(buttonRepo.findByIdAndLangId(19, getLangId()).getName());
                            ids.add(buttonRepo.findByIdAndLangId(19, getLangId()).getName());
                            ButtonsLeaf buttonsLeaf = new ButtonsLeaf(names, ids);

                            toDeleteMessage(sendMessageWithKeyboard(getText(41), buttonsLeaf.getListButtonWithDataList()));
                            waitingType = WaitingType.CHOICE_CONTACT;
                            return COMEBACK;
                        }
                        if(Integer.parseInt(updateMessageText) == 3){
                            sendRate();
                            waitingType = WaitingType.SET_RATE;
                            return COMEBACK;
                            //sendMessageWithKeyboard(getText());
                        }
                    }
                }
                return COMEBACK;
            case SET_RATE:
                if (hasCallbackQuery()){
                    rate = Integer.parseInt(updateMessageText);
                    sendMessageWithKeyboard("Вы можете оставить обратную связь, или же пропустить", 36);
                    waitingType = WaitingType.SET_CHOICE;
                    return COMEBACK;
//                    if (rate < 5){
//                        deleteId = sendMessage("Пожалуйста, напишите обратную связь, почему поставили меньше 5");
//                        waitingType = WaitingType.ENTER_OFFER;
//                    }else {
//                        sendMessage("Спасибо большое за оценку.");
//                    }
                }
                return COMEBACK;
            case SET_CHOICE:
                if(hasCallbackQuery()){
                    if(isButton(48)){
                        deleteId = sendMessage("Пожалуйста, напишите обратную связь");
                        waitingType = WaitingType.ENTER_OFFER;
                        return COMEBACK;
                    }else if(isButton(100)){
                        sendMessage("Спасибо большое за оценку.");
                        return COMEBACK;
                    }
                }
                return COMEBACK;
            case CHOICE_CONTACT:
                if(hasCallbackQuery()){
                    deleteMessages();
                    deleteUpdateMess();
                    if(isButton(19)){
                        getAllCategories();
                        return COMEBACK;
                    }else {
                        AboutProjectContactInfo subCategory = aboutProjectContactRepo.findById(Integer.parseInt(updateMessageText)).getAboutProjectContactInfo();
                        if (getLangId() == 1)
                            sendMessageWithKeyboard(subCategory.getNameRus(),6);
                        else sendMessageWithKeyboard(subCategory.getNameKaz(),6);
                    }
                }
                return COMEBACK;
            case ENTER_OFFER:
                deleteMessages();
                deleteUpdateMess();
                if (update.hasMessage() && update.getMessage().hasText()) {
                    files = new HashMap<>();
                    aboutProjectOffer = new AboutProjectOffer();
                    User user = usersRepo.findByChatId(chatId);
                    aboutProjectOffer.setOffer(update.getMessage().getText());
                    aboutProjectOffer.setUser(user);
                    aboutProjectOffer.setAccepted(false);
                    aboutProjectOffer.setDate(new Date());
                    sendMessageWithKeyboard(getText(71), 30);
                    waitingType = WaitingType.SET_FILEOFFERRR;
                    return COMEBACK;
                }
                else {
                    sendWrongData();
                    deleteMessages();
                    sendMessage("<i>Вы можете оставить свои рекомендации по улучшению бота </i>");
                    sendMessage(20);
                    waitingType = WaitingType.ENTER_OFFER;
                    return COMEBACK;
                }
            case SET_FILEOFFERRR:
                deleteMessages();
                deleteUpdateMess();
                if (hasCallbackQuery()) {
                    if (isButton(94)) {
                        sendMessageWithKeyboard(getText(131),33);
                        waitingType = WaitingType.SET_OFFERFILES;
                        return COMEBACK;
                    }
                    else if (isButton(93)) {
                        deleteId = sendMessage(getText(119));
                        waitingType = WaitingType.SET_FILEOFFERRE;
                        return COMEBACK;
                    }
                }
                else {
                    sendWrongData();
                    sendMessageWithKeyboard(getText(71), 30);
                    waitingType = WaitingType.SET_FILEOFFERRR;
                    return COMEBACK;
                }
            case SET_FILEOFFERRE:
                deleteMessages();
                deleteUpdateMess();
                if (update.hasMessage() && update.getMessage().hasPhoto()) {
                    if (files.size()<11){
                        files.put(updateMessagePhoto,FileType.photo);
                        deleteId = sendMessage(120);
                        sendMessageWithKeyboard(getText(71), 30);
                        waitingType = WaitingType.SET_FILEOFFERRR;
                        return COMEBACK;
                    }else {
                        sendMessageWithKeyboard(getText(132),33);
                        waitingType = WaitingType.SET_OFFERFILES;
                        return COMEBACK;
                    }

                }
                else if (update.hasMessage() && update.getMessage().hasVideo()) {
                    if (files.size()<11){
                        files.put(updateMessageVideo,FileType.video);
                        sendMessage(120);
                        sendMessageWithKeyboard(getText(71), 30);
                        waitingType = WaitingType.SET_FILEOFFERRR;
                        return COMEBACK;
                    }else {
                        sendMessageWithKeyboard(getText(132),33);
                        waitingType = WaitingType.SET_OFFERFILES;
                        return COMEBACK;
                    }
                }
                else if (update.hasMessage() && update.getMessage().hasDocument()) {
                    if (files.size()<11){
                        files.put(updateMessageDocument,FileType.document);
                        sendMessage(120);
                        sendMessageWithKeyboard(getText(71), 30);
                        waitingType = WaitingType.SET_FILEOFFERRR;
                        return COMEBACK;
                    }else {
                        sendMessageWithKeyboard(getText(132),33);
                        waitingType = WaitingType.SET_OFFERFILES;
                        return COMEBACK;
                    }
                }
            case SET_OFFERFILES:
                deleteMessages();
                deleteUpdateMess();
                if(hasCallbackQuery()){
                    if (isButton(96)){
                        aboutProjectOffer.setFile(files);
                        aboutProjectOfferRepo.save(aboutProjectOffer);
                        StringBuilder stringBuilder = new StringBuilder();

                        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
                        stringBuilder.append("№").append(aboutProjectOffer.getId()).append(next).
                                append(getText(27)).append(aboutProjectOffer.getUser().getFullName()).append(next).
                                append(getText(28)).append(aboutProjectOffer.getUser().getPhone()).append(next).
                                append(getText(38)).append(aboutProjectOffer.getOffer()).append(next).
                                append(getText(30)).append(simpleDateFormat.format(aboutProjectOffer.getDate()));

                        int sizePhoto = 0;
                        int sizeVideo = 0;
                        int sizeDocument = 0;
                        List<String> photos = new ArrayList<>();
                        List<String> videos = new ArrayList<>();
                        List<String> document = new ArrayList<>();


                        for (Map.Entry<String,FileType> s: files.entrySet()){
                            if (s.getValue().equals(FileType.photo)) {
                                sizePhoto++;
                                photos.add(s.getKey());
                            }else if (s.getValue().equals(FileType.video)) {
                                sizeVideo++;
                                videos.add(s.getKey());
                            }else if (s.getValue().equals(FileType.document)) {
                                sizeDocument++;
                                document.add(s.getKey());
                            }
                        }

                        List<InputMedia> media = new ArrayList<>();
                        List<InputMedia> mediaDocument = new ArrayList<>();
                        SendDocument sendDocument = new SendDocument();
                        SendVideo sendVideo = new SendVideo();
                        SendPhoto sendPhoto = new SendPhoto();
                        InputMediaPhoto inputMediaPhoto;
                        InputMediaVideo inputMediaVideo;

                        if (sizePhoto > 0 && sizeVideo > 0) {
                            for (String s : photos) {
                                inputMediaPhoto = new InputMediaPhoto();
                                inputMediaPhoto.setMedia(s);
                                media.add(inputMediaPhoto);
                            }

                            for (String s : videos) {
                                inputMediaVideo = new InputMediaVideo();
                                inputMediaVideo.setMedia(s);
                                media.add(inputMediaVideo);
                            }
                        }else if (sizePhoto>0 && sizeVideo==0){
                            for (String s : photos) {
                                inputMediaPhoto = new InputMediaPhoto();
                                inputMediaPhoto.setMedia(s);
                                media.add(inputMediaPhoto);
                            }
                        }else if (sizePhoto==0 && sizeVideo>0){
                            for (String s : videos) {
                                inputMediaVideo = new InputMediaVideo();
                                inputMediaVideo.setMedia(s);
                                media.add(inputMediaVideo);
                            }
                        }
                        InputMediaDocument inputMediaDocument;
                        if (sizeDocument > 0) {
                            for (String s : document) {
                                inputMediaDocument = new InputMediaDocument();
                                inputMediaDocument.setMedia(s);
                                mediaDocument.add(inputMediaDocument);
                            }

                        }
                        SendMediaGroup sendMediaGroupDoc = new SendMediaGroup();
                        SendMediaGroup sendMediaGroup = new SendMediaGroup();

                        if (mediaDocument.size()>1) {
                            sendMediaGroupDoc.setMedias(mediaDocument);
                            for (Admin admin : adminRepo.findAll()) {
                                sendMediaGroupDoc.setChatId(String.valueOf(admin.getChatId()));
                                bot.execute(sendMediaGroupDoc);
                            }
                        }else if (sizeDocument==1){
                            sendDocument.setDocument(new InputFile(document.get(0)));
                            for (Admin admin : adminRepo.findAll()) {
                                sendDocument.setChatId(String.valueOf(admin.getChatId()));
                                bot.execute(sendDocument);
                            }
                        }

                        if (media.size() > 0) {
                            if (photos.size() == 0 && videos.size() == 1) {
                                sendVideo.setVideo(new InputFile(videos.get(0)));
                                sendVideo.setParseMode("html");
                                sendVideo.setCaption(stringBuilder.toString());
//                                sendVideo.setReplyMarkup(keyboardMarkUpService.select(9));
                                for (Admin admin : adminRepo.findAll()) {
                                    sendVideo.setChatId(String.valueOf(admin.getChatId()));
                                    bot.execute(sendVideo);
                                }
                                sendMessage("<b>Успешно отправлено!</b>" );
                                return COMEBACK;
                            }else if (photos.size() == 1 && videos.size() == 0){
                                sendPhoto.setPhoto(new InputFile(photos.get(0)));
                                sendPhoto.setParseMode("html");
                                sendPhoto.setCaption(stringBuilder.toString());
//                                sendPhoto.setReplyMarkup(keyboardMarkUpService.select(9));
                                for (Admin admin : adminRepo.findAll()) {
                                    sendPhoto.setChatId(String.valueOf(admin.getChatId()));
                                    bot.execute(sendPhoto);
                                }
                                sendMessage("<b>Успешно отправлено!</b>" );
                                return COMEBACK;
                            }else {
                                sendMediaGroup.setMedias(media);
                                for (Admin admin : adminRepo.findAll()) {
                                    sendMediaGroup.setChatId(String.valueOf(admin.getChatId()));
                                    bot.execute(sendMediaGroup);
                                    sendMessage(stringBuilder.toString(), admin.getChatId());
                                }
                                sendMessage("<b>Успешно отправлено!</b>" );
                                return COMEBACK;
                            }
                        } else {
                            for (Admin admin : adminRepo.findAll()) {
                                sendMessage(stringBuilder.toString(), admin.getChatId());
                            }
                            sendMessage("<b>Успешно отправлено!</b>" );
                            return COMEBACK;
                        }
                    }
                }
        }
        deleteMessages();
        deleteUpdateMess();
        return EXIT;
    }

    private void sendRate() throws TelegramApiException {
        List<String> names = new ArrayList<>();
        List<String> ids = new ArrayList<>();

        names.add("⭐️");
        ids.add("1");

        names.add("⭐️");
        ids.add("2");

        names.add("⭐️");
        ids.add("3");

        names.add("⭐️");
        ids.add("4");

        names.add("⭐️");
        ids.add("5");

        ButtonsLeaf buttonsLeaf = new ButtonsLeaf(names,ids,false, 5);

        sendMessageWithKeyboard("Оцените", buttonsLeaf.getListButtonWithDataList());


    }

    private void sendWrongData() throws TelegramApiException {
        toDeleteMessage(sendMessage(18));
    }

    private void deleteMessages() {
        deleteMessage(updateMessageId);
        deleteMessage(deleteId);
    }

    private void getAllCategories() throws TelegramApiException {
        List<AboutProject> categories = aboutProjectRepo.findAllByOrderById();
        List<String> names = new ArrayList<>();
        List<String> ids = new ArrayList<>();
        for (AboutProject testCategory : categories) {
            if (getLangId() == 1)
                names.add(testCategory.getNameRus());
            else names.add(testCategory.getNameKaz());
            ids.add(String.valueOf(testCategory.getId()));
        }
        names.add(buttonRepo.findByIdAndLangId(19, getLangId()).getName());
        ids.add(buttonRepo.findByIdAndLangId(19, getLangId()).getName());
        ButtonsLeaf buttonsLeaf = new ButtonsLeaf(names, ids);

        toDeleteMessage(sendMessageWithKeyboard("<b>Данный бот разработан в рамках проекта " +
                "«Алматинская модель по контролю над эпидемией ВИЧ», " +
                "финансируемого Фондом Элтона Джона по борьбе со СПИДом. " +
                "Проект реализуется Центром по профилактике и борьбе со СПИД г. Алматы, " +
                "общественными организациями \"Реванш\", \"Community Friends\" и \"Центрально-азиатская " +
                "ассоциация людей, живущих с ВИЧ\", а также ICAP при Колумбийском университете.</b>", buttonsLeaf.getListButtonWithDataList()));
        waitingType = WaitingType.CHOICE_QUESTION;
    }

    private void deleteUpdateMess() {
        deleteMessage(updateMessageId);
    }
}
