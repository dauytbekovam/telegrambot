package CZN.command.impl.userMenu;


import CZN.CznApplication;
import CZN.command.Command;
import CZN.enums.WaitingType;
import CZN.exceptions.ButtonNotFoundException;
import CZN.exceptions.CommandNotFoundException;
import CZN.exceptions.KeyboardNotFoundException;
import CZN.exceptions.MessageNotFoundException;
import CZN.model.custom.userMenu.PerDay;
import CZN.model.custom.userMenu.PerDayTime;
import CZN.model.custom.userMenu.Reminder;
import CZN.model.standart.User;
import CZN.repository.TelegramBotRepositoryProvider;
import CZN.repository.userMenu.ReminderRepo;
import CZN.service.SaveDocument;
import CZN.util.ButtonsLeaf;
import com.itextpdf.text.DocumentException;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.bots.AbsSender;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.SQLException;
import java.sql.Time;
import java.util.*;

public class id013_SheduleChat extends Command {

    private AbsSender absSender;
    private int deleteId;
    private int wrongDeleteId;
    private int editId;
    private List<PerDayTime> perDayTimeList = new ArrayList<>();
    private int idDay;
    private User user;
    private PerDay perDay;
    private int reminderId;
    Reminder reminder;

    @Override
    public boolean execute() throws TelegramApiException, IOException, SQLException, FileNotFoundException, MessageNotFoundException, KeyboardNotFoundException, ButtonNotFoundException, CommandNotFoundException, DocumentException {
        switch (waitingType) {
            case START:
                if (updateMessageText.contains("/cancel")) {
                    deleteMessages();
                    deleteUpdateMess();
                    System.out.println(updateMessageText);
                    String strr = updateMessageText;
                    String[] wordss = strr.split("/cancel");

                    reminderId = Integer.parseInt(wordss[1]);

                    updateMessageText = "/cancel";
                }
                else if (updateMessageText.contains("/editTime")) {
                    deleteMessages();
                    deleteUpdateMess();
                    System.out.println(updateMessageText);
                    String strr = updateMessageText;
                    String[] wordss = strr.split("/editTime");

                    reminderId = Integer.parseInt(wordss[1]);

                    updateMessageText = "/editTime";
                }

                if (isButton(45))
                    getPerDays();
                else if (isButton(8)) {

                    Reminder reminder = reminderRepo.findById(reminderId);
                    CznApplication.reminderMap.get(reminder).cancel();

                    reminderRepo.delete(reminder);

                    deleteId = sendMessage("Бот больше не будет напоминать " +
                            "вам о необходимости принятия лекарства");
                } else if (isButton(11)) {
                    reminder = reminderRepo.findById(reminderId);
                    return getNewDate();
                } else return COMEBACK;
                return COMEBACK;

            case SET_NEW_DATE:
                if (hasMessageText()) {
                    String str = updateMessageText;
                    String[] words = str.split(":");
                    if (words.length <= 1) {
                        sendWrongData();
                        return getNewDate();
                    } else {
                        int hour = Integer.parseInt(words[0]);
                        int minute = Integer.parseInt(words[1]);
                        System.out.println(words[1]);
                        if (hour >= 0 && hour <= 23 && minute >= 0 && minute <= 60) {
                            Date dateTime = new Date();
                            dateTime.setHours(hour);
                            dateTime.setMinutes(minute);

                            Timer timer =  CznApplication.reminderMap.get(reminder);

                            CznApplication.reminderMap.remove(reminder);

                            reminder.setMinute(minute);
                            reminder.setHour(hour);

                            reminder = reminderRepo.save(reminder);


                            timer.scheduleAtFixedRate(new SaveDocument(bot,reminder), dateTime, 1000 * 60 * 60 * 24);

                            CznApplication.reminderMap.put(reminder, timer);

                            System.out.println(CznApplication.reminderMap);
                            deleteId = sendMessage("Вы успешно поменяли время напоминания");
                            return COMEBACK;
                        }

                    }

                }
            case CHOICE_QUESTION:
                deleteMessages();
                deleteUpdateMess();
                if (hasCallbackQuery()) {
                    if (isButton(19)) {
                        deleteMessages();
                        deleteUpdateMess();
                        getPerDays();
                        return COMEBACK;
                    } else {
                        deleteMessages();
                        deleteUpdateMess();
                        idDay = Integer.parseInt(updateMessageText);
                        if (idDay == 1) {
                            deleteMessages();
                            deleteUpdateMess();
                            perDay = perDayRepo.findById(1);
                            deleteId = sendMessage("Напишите во сколько необходимо упомянуть \n \n (например 9:00)");
                            waitingType = WaitingType.GET_HOUR;
                            return COMEBACK;
                        } else if (idDay == 2) {
                            deleteMessages();
                            deleteUpdateMess();
                            perDay = perDayRepo.findById(1);
                            deleteId = sendMessage("Укажите первое время во сколько необходимо упомянуть \n \n (например 9:00)");
                            waitingType = WaitingType.GET_FIRST_HOUR;
                            return COMEBACK;
                        }
                    }
                }
                return COMEBACK;
            case GET_HOUR:
                deleteMessages();
                if (update.hasMessage() && update.getMessage().hasText()) {
                    System.out.println(updateMessageText);
                    String str = updateMessageText;
                    String[] words = str.split(":");
                    try {
                        System.out.println(words.length);
                        if (words.length <= 1) {
                            sendWrongData();
                            deleteId = sendMessage("Напишите во сколько необходимо упомянуть через '<b> : </b>' \n \n (например 9:00)");
                            waitingType = WaitingType.GET_HOUR;
                        } else {
                            int hour = Integer.parseInt(words[0]);
                            int minute = Integer.parseInt(words[1]);
                            System.out.println(words[1]);
                            if (hour >= 0 && hour <= 23 && minute >= 0 && minute <= 60) {

                                Date dateTime = new Date();
                                dateTime.setHours(hour);
                                dateTime.setMinutes(minute);

                                user = usersRepo.findByChatId(chatId);

                                Reminder reminder = new Reminder();
                                reminder.setUserId(String.valueOf(user.getChatId()));
                                reminder.setHour(hour);
                                reminder.setMinute(minute);
                                reminder = reminderRepo.save(reminder);

                                Timer timer = new Timer(true);
                                Calendar calendar = Calendar.getInstance();
                                calendar.set(Calendar.HOUR_OF_DAY, reminder.getHour());
                                calendar.set(Calendar.MINUTE, reminder.getMinute());
                                calendar.set(Calendar.SECOND, 0);

                                Date date = calendar.getTime();
                                timer.scheduleAtFixedRate(new SaveDocument(bot, reminder), date, 1000 * 60 * 60 * 24); // 1000 = 1 seconds  1000 * 60 * 60 * 12

                                CznApplication.reminderMap.put(reminder, timer);

                                deleteId = sendMessage("Вам придет сообщение упомянание в " + dateTime.getHours() + ":" + dateTime.getMinutes() + "минут");
                                return COMEBACK;
                            } else {
                                sendWrongData();
                                deleteId = sendMessage("Напишите во сколько необходимо упомянуть \n \n (например 9:00)");
                                waitingType = WaitingType.GET_HOUR;
                                return COMEBACK;
                            }
                        }
                    } catch (NumberFormatException exception) {
                        sendWrongData();
                        deleteId = sendMessage("Напишите во сколько необходимо упомянуть \n \n (например 9:00)");
                        waitingType = WaitingType.GET_HOUR;
                        return COMEBACK;
                    }
                    return COMEBACK;
                }
                return COMEBACK;
            case GET_FIRST_HOUR:
                deleteMessages();
                if (update.hasMessage() && update.getMessage().hasText()) {
                    String str = updateMessageText;
                    String[] words = str.split(":");
                    try {
                        System.out.println(words.length);
                        if (words.length <= 1) {
                            sendWrongData();
                            deleteId = sendMessage("Укажите первое время во сколько необходимо упомянуть \n \n (например 9:00)");
                            waitingType = WaitingType.GET_FIRST_HOUR;
                        } else {
                            int hour = Integer.parseInt(words[0]);
                            int minute = Integer.parseInt(words[1]);
                            if (hour >= 0 && hour <= 23 && minute >= 0 && minute <= 60) {

                                Date dateTime = new Date();
                                dateTime.setHours(hour);
                                dateTime.setMinutes(minute);

                                user = usersRepo.findByChatId(chatId);

                                Reminder reminder = new Reminder();
                                reminder.setUserId(String.valueOf(user.getChatId()));
                                reminder.setHour(hour);
                                reminder.setMinute(minute);
                                reminder = reminderRepo.save(reminder);

                                Timer timer = new Timer(true);
                                Calendar calendar = Calendar.getInstance();
                                calendar.set(Calendar.HOUR_OF_DAY, reminder.getHour());
                                calendar.set(Calendar.MINUTE, reminder.getMinute());
                                calendar.set(Calendar.SECOND, 0);

                                Date date = calendar.getTime();
                                timer.scheduleAtFixedRate(new SaveDocument(bot, reminder), date, 1000 * 60 * 60 * 24); // 1000 = 1 seconds  1000 * 60 * 60 * 12

                                deleteId = sendMessage("Укажите второе время во сколько необходимо упомянуть \n \n (например 9:00)");
                                waitingType = WaitingType.GET_SECOND_HOUR;
                                return COMEBACK;
                            } else {
                                sendWrongData();
                                deleteId = sendMessage("Укажите первое время во сколько необходимо упомянуть \n \n (например 9:00)");
                                waitingType = WaitingType.GET_HOUR;
                                return COMEBACK;
                            }
                        }
                    } catch (NumberFormatException exception) {
                        sendWrongData();
                        deleteId = sendMessage("Укажите первое время во сколько необходимо упомянуть \n \n (например 9:00)");
                        waitingType = WaitingType.GET_FIRST_HOUR;
                        return COMEBACK;
                    }
                    return COMEBACK;
                }
                return COMEBACK;
            case GET_SECOND_HOUR:
                deleteMessages();
                if (update.hasMessage() && update.getMessage().hasText()) {

                    String str = updateMessageText;
                    String[] words = str.split(":");

                    try {
                        System.out.println(words.length);
                        if (words.length <= 1) {
                            sendWrongData();
                            deleteId = sendMessage("Укажите второе время во сколько необходимо упомянуть \n \n (например 9:00)");
                            waitingType = WaitingType.GET_SECOND_HOUR;
                        } else {
                            int hour = Integer.parseInt(words[0]);
                            int minute = Integer.parseInt(words[1]);
                            if (hour >= 0 && hour <= 23 && minute >= 0 && minute <= 60) {

                                Date dateTime = new Date();
                                dateTime.setHours(hour);
                                dateTime.setMinutes(minute);

                                user = usersRepo.findByChatId(chatId);

                                Reminder reminder = new Reminder();
                                reminder.setUserId(String.valueOf(user.getChatId()));
                                reminder.setHour(hour);
                                reminder.setMinute(minute);
                                reminder = reminderRepo.save(reminder);

                                Timer timer = new Timer(true);
                                Calendar calendar = Calendar.getInstance();
                                calendar.set(Calendar.HOUR_OF_DAY, reminder.getHour());
                                calendar.set(Calendar.MINUTE, reminder.getMinute());
                                calendar.set(Calendar.SECOND, 0);

                                Date date = calendar.getTime();
                                timer.scheduleAtFixedRate(new SaveDocument(bot, reminder), date, 1000 * 60 * 60 * 24); // 1000 = 1 seconds  1000 * 60 * 60 * 12

                                deleteId = sendMessage("Принято, Вам придет сообщение напоминание о необходимости принятия лекарства");
                                return COMEBACK;
                            } else {
                                sendWrongData();
                                deleteId = sendMessage("Укажите второе время во сколько необходимо упомянуть \n \n (например 9:00)");
                                waitingType = WaitingType.GET_SECOND_HOUR;
                                return COMEBACK;
                            }
                        }
                    } catch (NumberFormatException exception) {
                        sendWrongData();
                        deleteId = sendMessage("Укажите второе время во сколько необходимо упомянуть \n \n (например 9:00)");
                        waitingType = WaitingType.GET_SECOND_HOUR;
                        return COMEBACK;
                    }
                    return COMEBACK;
                }
                return COMEBACK;

        }
        return EXIT;
    }

    private boolean getNewDate() throws TelegramApiException {

        deleteId = sendMessage("Напишите во сколько необходимо упомянуть \n \n (например 9:00)");
        waitingType = WaitingType.SET_NEW_DATE;
        return COMEBACK;
    }

    private void getPerDays() throws TelegramApiException {
        List<PerDay> categories = perDayRepo.findAllByOrderById();
        List<String> names = new ArrayList<>();
        List<String> ids = new ArrayList<>();
        for (PerDay testCategory : categories) {
            if (getLangId() == 1)
                names.add(testCategory.getNameRus());
            else names.add(testCategory.getNameKaz());
            ids.add(String.valueOf(testCategory.getId()));
        }
        names.add(buttonRepo.findByIdAndLangId(19, getLangId()).getName());
        ids.add(buttonRepo.findByIdAndLangId(19, getLangId()).getName());
        ButtonsLeaf buttonsLeaf = new ButtonsLeaf(names, ids);

        toDeleteMessage(sendMessageWithKeyboard(getText(17), buttonsLeaf.getListButtonWithDataList()));
        waitingType = WaitingType.CHOICE_QUESTION;
    }

    private void deleteUpdateMess() {
        deleteMessage(updateMessageId);
    }

    private void sendWrongData() throws TelegramApiException {
        toDeleteMessage(sendMessage(18));
    }

    private void deleteMessages() {
        deleteMessage(deleteId);
        deleteMessage(updateMessageId);
        deleteMessage(wrongDeleteId);
    }
}