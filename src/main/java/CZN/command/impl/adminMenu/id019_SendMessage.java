package CZN.command.impl.adminMenu;

import CZN.command.Command;
import CZN.enums.FileType;
import CZN.enums.WaitingType;
import CZN.exceptions.ButtonNotFoundException;
import CZN.exceptions.CommandNotFoundException;
import CZN.exceptions.KeyboardNotFoundException;
import CZN.exceptions.MessageNotFoundException;
import CZN.model.custom.userMenu.AboutProjectOffer;
import CZN.model.standart.User;
import com.itextpdf.text.DocumentException;
import org.telegram.telegrambots.meta.api.methods.send.*;
import org.telegram.telegrambots.meta.api.objects.InputFile;
import org.telegram.telegrambots.meta.api.objects.media.InputMedia;
import org.telegram.telegrambots.meta.api.objects.media.InputMediaDocument;
import org.telegram.telegrambots.meta.api.objects.media.InputMediaPhoto;
import org.telegram.telegrambots.meta.api.objects.media.InputMediaVideo;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.*;

public class id019_SendMessage extends Command {

    Map<String, FileType> files;
    int deleteId;
    AboutProjectOffer offer;

    @Override
    public boolean execute() throws TelegramApiException, IOException, SQLException, FileNotFoundException, MessageNotFoundException, KeyboardNotFoundException, ButtonNotFoundException, CommandNotFoundException, DocumentException {
        if (!isAdmin()){
            sendMessage(5);
            return EXIT;
        }
        switch (waitingType){
            case START:
                if(isButton(104)){
                    sendMessage(21);
                    waitingType = WaitingType.ENTER_OFFER;
                } else return COMEBACK;
                return COMEBACK;
            case ENTER_OFFER:
                if (update.hasMessage() && update.getMessage().hasText()) {
                    files = new HashMap<>();
                    offer = new AboutProjectOffer();
                    User user = usersRepo.findByChatId(chatId);
                    offer.setOffer(update.getMessage().getText());
                    offer.setUser(user);
                    offer.setAccepted(false);
                    offer.setDate(new Date());
                    sendMessageWithKeyboard(getText(71), 30);
                    waitingType = WaitingType.SET_FILEOFFERRR;
                    return COMEBACK;
                } else {
                    sendWrongData();
                    deleteMessages();
                    sendMessage(21);
                    waitingType = WaitingType.ENTER_OFFER;
                    return COMEBACK;
                }
            case SET_FILEOFFERRR:
                if (hasCallbackQuery()) {
                    if (isButton(94)) {
                        System.out.println(getText(131).toLowerCase(Locale.ROOT));
                        sendMessageWithKeyboard(getText(131),33);
                        waitingType = WaitingType.SET_OFFERFILES;
                        return COMEBACK;
                    } else if (isButton(93)) {
                        sendMessage(getText(119));
                        waitingType = WaitingType.SET_FILEOFFERRE;
                        return COMEBACK;
                    }
                } else {
                    sendWrongData();
                    sendMessageWithKeyboard(getText(71), 30);
                    waitingType = WaitingType.SET_FILEOFFERRR;
                    return COMEBACK;
                }
            case SET_FILEOFFERRE:
                if (update.hasMessage() && update.getMessage().hasPhoto()) {
                    if (files.size()<11){
                        files.put(updateMessagePhoto,FileType.photo);
                        sendMessage(120);
                        sendMessageWithKeyboard(getText(71), 30);
                        waitingType = WaitingType.SET_FILEOFFERRR;
                        return COMEBACK;
                    }else {
                        sendMessageWithKeyboard(getText(132),33);
                        waitingType = WaitingType.SET_OFFERFILES;
                        return COMEBACK;
                    }

                } else if (update.hasMessage() && update.getMessage().hasVideo()) {
                    if (files.size()<11){
                        files.put(updateMessageVideo,FileType.video);
                        sendMessage(120);
                        sendMessageWithKeyboard(getText(71), 30);
                        waitingType = WaitingType.SET_FILEOFFERRR;
                        return COMEBACK;
                    }else {
                        sendMessageWithKeyboard(getText(132),33);
                        waitingType = WaitingType.SET_OFFERFILES;
                        return COMEBACK;
                    }
                } else if (update.hasMessage() && update.getMessage().hasDocument()) {
                    if (files.size()<11){
                        files.put(updateMessageDocument,FileType.document);
                        sendMessage(120);
                        sendMessageWithKeyboard(getText(71), 30);
                        waitingType = WaitingType.SET_FILEOFFERRR;
                        return COMEBACK;
                    }else {
                        sendMessageWithKeyboard(getText(132),33);
                        waitingType = WaitingType.SET_OFFERFILES;
                        return COMEBACK;
                    }
                }
            case SET_OFFERFILES:
                if(hasCallbackQuery()){
                    if (isButton(96)){

                        StringBuilder stringBuilder = new StringBuilder();

                        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
                        stringBuilder.append(getText(27)).append(offer.getUser().getFullName()).append(next).
                                append(getText(28)).append(offer.getUser().getPhone()).append(next).
                                append(getText(38)).append(offer.getOffer()).append(next).
                                append(getText(30)).append(simpleDateFormat.format(offer.getDate()));

                        int sizePhoto = 0;
                        int sizeVideo = 0;
                        int sizeDocument = 0;
                        List<String> photos = new ArrayList<>();
                        List<String> videos = new ArrayList<>();
                        List<String> document = new ArrayList<>();


                        for (Map.Entry<String,FileType> s: files.entrySet()){
                            if (s.getValue().equals(FileType.photo)) {
                                sizePhoto++;
                                photos.add(s.getKey());
                            }else if (s.getValue().equals(FileType.video)) {
                                sizeVideo++;
                                videos.add(s.getKey());
                            }else if (s.getValue().equals(FileType.document)) {
                                sizeDocument++;
                                document.add(s.getKey());
                            }
                        }

                        List<InputMedia> media = new ArrayList<>();
                        List<InputMedia> mediaDocument = new ArrayList<>();
                        SendDocument sendDocument = new SendDocument();
                        SendVideo sendVideo = new SendVideo();
                        SendPhoto sendPhoto = new SendPhoto();
                        InputMediaPhoto inputMediaPhoto;
                        InputMediaVideo inputMediaVideo;

                        Set<String> chatIds = new HashSet<>();

                        for (User user : usersRepo.findAll()) {
                            chatIds.add(String.valueOf( user.getChatId()));
                        }

                        if (sizePhoto > 0 && sizeVideo > 0) {
                            for (String s : photos) {
                                inputMediaPhoto = new InputMediaPhoto();
                                inputMediaPhoto.setMedia(s);
                                media.add(inputMediaPhoto);
                            }

                            for (String s : videos) {
                                inputMediaVideo = new InputMediaVideo();
                                inputMediaVideo.setMedia(s);
                                media.add(inputMediaVideo);
                            }
                        }else if (sizePhoto>0 && sizeVideo==0){
                            for (String s : photos) {
                                inputMediaPhoto = new InputMediaPhoto();
                                inputMediaPhoto.setMedia(s);
                                media.add(inputMediaPhoto);
                            }
                        }else if (sizePhoto==0 && sizeVideo>0){
                            for (String s : videos) {
                                inputMediaVideo = new InputMediaVideo();
                                inputMediaVideo.setMedia(s);
                                media.add(inputMediaVideo);
                            }
                        }
                        InputMediaDocument inputMediaDocument;
                        if (sizeDocument > 0) {
                            for (String s : document) {
                                inputMediaDocument = new InputMediaDocument();
                                inputMediaDocument.setMedia(s);
                                mediaDocument.add(inputMediaDocument);
                            }

                        }
                        SendMediaGroup sendMediaGroupDoc = new SendMediaGroup();
                        SendMediaGroup sendMediaGroup = new SendMediaGroup();

                        if (mediaDocument.size()>1) {
                            sendMediaGroupDoc.setMedias(mediaDocument);
                            for (String admin : chatIds) {
                                sendMediaGroupDoc.setChatId(admin);
                                bot.execute(sendMediaGroupDoc);
                            }

                        }else if (sizeDocument==1){
                            sendDocument.setDocument(new InputFile(document.get(0)));
                            for (String admin : chatIds) {
                                sendDocument.setChatId(admin);
                                bot.execute(sendDocument);
                            }
                        }

                        if (media.size() > 0) {
                            if (photos.size() == 0 && videos.size() == 1) {
                                sendVideo.setVideo(new InputFile(videos.get(0)));
                                sendVideo.setParseMode("html");
                                sendVideo.setCaption(stringBuilder.toString());
//                                sendVideo.setReplyMarkup(keyboardMarkUpService.select(9));
                                for (String admin : chatIds) {
                                    sendVideo.setChatId(admin);
                                    bot.execute(sendVideo);
                                }
                                sendMessage("<b>Успешно отправлено!</b>" );
                                return COMEBACK;
                            }else if (photos.size() == 1 && videos.size() == 0){
                                sendPhoto.setPhoto(new InputFile(photos.get(0)));
                                sendPhoto.setParseMode("html");
                                sendPhoto.setCaption(stringBuilder.toString());
//                                sendPhoto.setReplyMarkup(keyboardMarkUpService.select(9));
                                for (String admin : chatIds) {
                                    sendPhoto.setChatId(admin);
                                    bot.execute(sendPhoto);
                                }
                                sendMessage("<b>Успешно отправлено!</b>" );
                                return COMEBACK;
                            }else {
                                sendMediaGroup.setMedias(media);
                                for (String admin : chatIds) {
                                    sendMediaGroup.setChatId(admin);
                                    bot.execute(sendMediaGroup);
                                    sendMessage(stringBuilder.toString(), Long.parseLong(admin));
                                }
                                sendMessage("<b>Успешно отправлено!</b>" );
                                return COMEBACK;
                            }
                        } else {
                            for (String admin : chatIds) {
                                sendMessage(stringBuilder.toString(),  Long.parseLong(admin));
                            }
                            sendMessage("<b>Успешно отправлено!</b>" );
                            return COMEBACK;
                        }
                    }
                }
        }
        return EXIT;
    }

    private void sendWrongData() throws TelegramApiException {
        toDeleteMessage(sendMessage(18));
    }

    private void deleteMessages() {
        deleteMessage(updateMessageId);
        deleteMessage(deleteId);
    }

}
