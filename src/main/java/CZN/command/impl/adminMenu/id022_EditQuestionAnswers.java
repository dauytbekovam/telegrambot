package CZN.command.impl.adminMenu;

import CZN.command.Command;
import CZN.enums.Language;
import CZN.enums.WaitingType;
import CZN.exceptions.ButtonNotFoundException;
import CZN.exceptions.CommandNotFoundException;
import CZN.exceptions.KeyboardNotFoundException;
import CZN.exceptions.MessageNotFoundException;
import CZN.model.custom.userMenu.InfoAnswer;
import CZN.model.custom.userMenu.Information;
import CZN.model.custom.userMenu.InfoQuestion;
import CZN.util.ButtonsLeaf;
import com.itextpdf.text.DocumentException;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class id022_EditQuestionAnswers extends Command {

    int deleteId;
    int wrongDeleteId;
    String categoryId;
    Information category;
    InfoQuestion question;
    InfoAnswer answer;
    Language currentLang;
    Integer editId;
    boolean isOn;

    @Override
    public boolean execute() throws TelegramApiException, IOException, SQLException, FileNotFoundException, MessageNotFoundException, KeyboardNotFoundException, ButtonNotFoundException, CommandNotFoundException, DocumentException {
        if (!isAdmin()){
            sendMessage(5);
            return EXIT;
        }
        switch (waitingType){
            case START:
                deleteMessages();
                if(isButton(98)) {
                    deleteMessages();
                    getAllCategories();
                }else return COMEBACK;
                return COMEBACK;
            case CHOISE_SUB_CATEGORY:
                if(hasCallbackQuery()){
                    if(isButton(19)){
                        deleteMessages();
                        getAllCategories();
                        return COMEBACK;
                    } else if(isButton(9801)){
                        deleteMessages();
                        category = new Information();
                        category.setInfoQuestions(new ArrayList<>());
                        sendMessage(getText(202));
                        long id = getIdForNewInfoCategory();
                        String stringId = String.valueOf(id);
                        category.setId(Integer.parseInt(stringId));
                        waitingType = WaitingType.SET_NAME_RU;
                        return COMEBACK;
                    } else{
                        deleteMessages();
                        categoryId = updateMessageText;
                        System.out.println(categoryId);
                        sendMessageWithKeyboard("Выберите что делать", keyboardMarkUpService.select(144));
                        waitingType = WaitingType.CHANGE_SUB_CATEGORY;
                        return COMEBACK;
                    }
                }
                return COMEBACK;
            case SET_NAME_RU:
                if (hasMessageText()) {
//                    deleteMessages();
                    category.setNameRus(updateMessageText);
                    deleteId = sendMessage(getText(203));
                    waitingType = WaitingType.SET_NAME_KZ;
                    return COMEBACK;
                } else {
                    sendWrongData();
                    deleteId = sendMessage(getText(202));
                    return COMEBACK;
                }
            case SET_NAME_KZ:
                if (hasMessageText()) {
//                        deleteMessages();
                    category.setNameKaz(updateMessageText);
                    informationRepo.save(category);
                    deleteId = sendMessage("Успешно изменено");
                    getAllCategories();
                    return COMEBACK;
                } else {
                    sendWrongData();
                    deleteId = sendMessageWithKeyboard(getText(203), 6);
                    return COMEBACK;
                }
            case CHANGE_SUB_CATEGORY:
//                deleteMessages();
                System.out.println("My message " + updateMessageText);
                System.out.println(categoryId);
                category = getInfoCategoryById(categoryId);
                if(hasCallbackQuery()){
                    if(isButton(19)){
                        deleteMessages();
                        getAllCategories();
                        return COMEBACK;
                    }else if(isButton(1441)){
//                  Изменить название раздела
                        deleteMessages();
                        sendMessage(getText(202));
                        waitingType = WaitingType.SET_NAME_RU;
                        return COMEBACK;
                    }
                    else if(isButton(1442)){
//                  Добавить факю
                        deleteMessages();
                        question = new InfoQuestion();
                        answer = new InfoAnswer();
                        sendMessage(210);
                        waitingType = WaitingType.SET_QUESTION_KAZ;
                        return COMEBACK;
                    }
                    else if(isButton(1443)){
//                  Изменить существующий факю
                        getQuestionsByCategory(categoryId);
                        return COMEBACK;
                    }
                    else {
                        sendWrongData();
                        return COMEBACK;
                    }
                }
                return COMEBACK;
            case CHOICE_OPTION:
                if(hasCallbackQuery()){
                    if(isButton(19)){
                        deleteMessages();
                        getAllCategories();
                        return COMEBACK;
                    } else{
                        question = infoQuestionRepo.findById(Integer.parseInt(updateMessageText));
                        System.out.println("Question " + question);
                        sendMessageWithKeyboard("Выберите действие",keyboardMarkUpService.select(1443));
                        waitingType = WaitingType.CHANGE_EXIT_FAQ;
                        return COMEBACK;
                    }
                }
                return COMEBACK;
            case CHANGE_EXIT_FAQ:
                if(hasCallbackQuery()){
                    if(isButton(19)){
                        deleteMessages();
                        getAllCategories();
                        return COMEBACK;
                    } else if(isButton(14431)){
                        //  Изменить вопрос
                        if(getLangId() == 1){
                            sendMessage("Вы собираетесь изменить вопрос: " +
                                    question.getQuestionRus() + "\n напишите пожалуйста новое имя данного вопроса на каз" ) ;
                        }else if(getLangId() == 2){
                            sendMessage("Мына сурактын атын озгерткели жатырсыз: " +
                                    question.getQuestionKaz() + "\n осы сурактын жана атын казакша жазуынызды отинемин");
                        }
                        waitingType = WaitingType.SET_QUESTION_NAME_KAZ;
                        return COMEBACK;
                    } else if(isButton(14432)){
                        // Изменить ответ
                        answer = infoAnswerRepo.findByInfoQuestionId(question.getId());
                        if(getLangId() == 1){
                            sendMessage("Вы собираетесь изменить ответ: " +
                                    answer.getAnswerRus() + "\n напишите пожалуйста новое имя данного ответа на каз" ) ;
                        }else if(getLangId() == 2){
                            sendMessage("Мына жауаптын атын озгерткели жатырсыз: " +
                                    answer.getAnswerKaz() + "\n осы жауаптын жана атын казакша жазуынызды отинемин");
                        }
                        waitingType = WaitingType.SET_ANSWER_NAME_KAZ;
                        return COMEBACK;
                    } else if(isButton(14433)){
                        // сменить язык
                        currentLang = getLanguage();
                        if(currentLang.getId() == 1){
                            currentLang = Language.kz;
                            String info = getInfoMessages(question, infoAnswerRepo.findByInfoQuestionId(question.getId()));
                            editId = sendMessage(info);
                            return COMEBACK;
                        }else if(currentLang.getId() == 2){
                            currentLang = Language.ru;
                            String info = getInfoMessages(question, infoAnswerRepo.findByInfoQuestionId(question.getId()));
                            editId = sendMessage(info);
                            return COMEBACK;
                        }
                    } else if(isButton(14434)){
                        // включить/отключить
                        isOn = true;
                        String info = getInfoMessages();
                        editId = sendMessageWithKeyboard(info, 6);
                        return COMEBACK;
                    }
                }
                else if (hasMessageText()){
                    System.out.println(updateMessageText);
                    if (updateMessageText.contains("/switchLanguage")){
                        deleteMessage(updateMessageId);
                        if(currentLang.getId() == 1){
                            currentLang = Language.kz;
                            String info = getInfoMessages(question, infoAnswerRepo.findByInfoQuestionId(question.getId()));
                            editMessage(info,editId);
                            return COMEBACK;
                        }else if(currentLang.getId() == 2){
                            currentLang = Language.ru;
                            String info = getInfoMessages(question, infoAnswerRepo.findByInfoQuestionId(question.getId()));
                            editMessage(info,editId);
                            return COMEBACK;
                        }
                    }
                    else if (updateMessageText.contains("/onOff")){
                        deleteMessage(updateMessageId);
                        if(isOn){
                            isOn = false;
                            System.out.println(question.getId());
                            System.out.println(category.getInfoQuestions());
                            for (int i = 0; i < category.getInfoQuestions().size(); i++) {
                                if(question.getId() == category.getInfoQuestions().get(i).getId()){
                                    category.getInfoQuestions().remove(question);
                                    informationRepo.save(category);
                                    String info = getInfoMessages() + " отключили";
                                    editMessage(info,editId);
                                    break;
                                }else if(question.getId() != category.getInfoQuestions().get(i).getId()){
                                    System.out.println("Smth is wrong true");
                                }
                            }
                            return COMEBACK;
                        }else {
                            isOn = true;
                            for (int i = 0; i < category.getInfoQuestions().size(); i++) {
                                if(question.getId() == category.getInfoQuestions().get(i).getId()){
                                    System.out.println("Smth is wrong false");
                                    break;
                                }else if(question.getId() != category.getInfoQuestions().get(i).getId()){
                                    category.getInfoQuestions().add(question);
                                    informationRepo.save(category);
                                    String info = getInfoMessages() + " подключили";
                                    editMessage(info,editId);
                                }
                            }
                            return COMEBACK;
                        }
                    }
                }
                return COMEBACK;
            case SET_QUESTION_NAME_KAZ:
                if (hasMessageText()) {
                    deleteMessages();
                    question.setQuestionKaz(updateMessageText);
                    if(getLangId() == 1){
                        sendMessage("Вы собираетесь изменить вопрос: " +
                                question.getQuestionRus() + "\n напишите пожалуйста новое имя данного вопроса на русс" ) ;
                    }else if(getLangId() == 2){
                        sendMessage("Мына сурактын атын озгерткели жатырсыз: " +
                                question.getQuestionKaz() + "\n осы сурактын жана атын орысша жазуынызды отинемин");
                    }
                    waitingType = WaitingType.SET_QUESTION_NAME_RU;
                    return COMEBACK;
                } else {
                    sendWrongData();
                    deleteId = sendMessage(getText(202));
                    return COMEBACK;
                }
            case SET_QUESTION_NAME_RU:
                if (hasMessageText()) {
                    deleteMessages();
                    question.setQuestionRus(updateMessageText);
                    infoQuestionRepo.save(question);
                    if(getLangId() == 1){
                        sendMessage("Выполнено успешно");
                    }else if(getLangId() == 2){
                        sendMessage("Сатти орындалды");
                    }
                    return COMEBACK;
                } else {
                    sendWrongData();
                    deleteId = sendMessage(getText(202));
                    return COMEBACK;
                }
            case SET_ANSWER_NAME_KAZ:
                if (hasMessageText()) {
                    deleteMessages();
                    answer.setAnswerKaz(updateMessageText);
                    if(getLangId() == 1){
                        sendMessage("Вы собираетесь изменить ответ: " +
                                answer.getAnswerRus() + "\n напишите пожалуйста новое имя данного ответа на русс" ) ;
                    }else if(getLangId() == 2){
                        sendMessage("Мына жауапты атын озгерткели жатырсыз: " +
                                answer.getAnswerKaz() + "\n осы жауапты жана атын орысша жазуынызды отинемин");
                    }
                    waitingType = WaitingType.SET_ANSWER_NAME_RU;
                    return COMEBACK;
                } else {
                    sendWrongData();
                    deleteId = sendMessage(getText(202));
                    return COMEBACK;
                }
            case SET_ANSWER_NAME_RU:
                if (hasMessageText()) {
                    deleteMessages();
                    answer.setAnswerRus(updateMessageText);
                    infoAnswerRepo.save(answer);
                    if(getLangId() == 1){
                        sendMessage("Выполнено успешно");
                        return COMEBACK;
                    }else if(getLangId() == 2){
                        sendMessage("Сатти орындалды");
                        return COMEBACK;
                    }
                    return COMEBACK;
                } else {
                    sendWrongData();
                    deleteId = sendMessage(getText(202));
                    return COMEBACK;
                }
            case SET_QUESTION_KAZ:
                if (hasMessageText()) {
                    deleteMessages();
                    question.setQuestionKaz(updateMessageText);
                    sendMessage(211);
                    waitingType = WaitingType.SET_QUESTION_RU;
                    return COMEBACK;
                } else {
                    sendWrongData();
                    sendMessage(210);
                    waitingType = WaitingType.SET_QUESTION_KAZ;
                    return COMEBACK;
                }
            case SET_QUESTION_RU:
                if (hasMessageText()) {
                    deleteMessages();
                    question.setQuestionRus(updateMessageText);
                    infoQuestionRepo.save(question);
                    category.getInfoQuestions().add(question);
                    informationRepo.save(category);
                    answer.setInfoQuestion(question);
                    sendMessage(212);
                    waitingType = WaitingType.SET_ANSWER_KAZ;
                    return COMEBACK;
                } else {
                    sendWrongData();
                    sendMessage(210);
                    waitingType = WaitingType.SET_QUESTION_KAZ;
                    return COMEBACK;
                }
            case SET_ANSWER_KAZ:
                if (hasMessageText()) {
                    deleteMessages();
                    answer.setAnswerKaz(updateMessageText);
                    System.out.println(answer.getAnswerKaz());
                    sendMessage(213);
                    waitingType = WaitingType.SET_ANSWER_RU;
                    return COMEBACK;
                } else {
                    sendWrongData();
                    sendMessage(210);
                    waitingType = WaitingType.SET_QUESTION_KAZ;
                    return COMEBACK;
                }
            case SET_ANSWER_RU:
                if (hasMessageText()) {
                    deleteMessages();
                    answer.setAnswerRus(updateMessageText);
                    System.out.println(answer);
                    infoAnswerRepo.save(answer);
                    deleteId = sendMessageWithKeyboard("Успешно добавлено", 6);
                    return COMEBACK;
                } else if(isButton(19)){
                    deleteMessages();
                    getAllCategories();
                    return COMEBACK;
                } else {
                    sendWrongData();
                    return COMEBACK;
                }
        }
        return EXIT;
    }

    private void getAllCategories() throws TelegramApiException {
        List<Information> categories = informationRepo.findAllByOrderById();
        List<String> names = new ArrayList<>();
        List<String> ids = new ArrayList<>();

        names.add(buttonRepo.findByIdAndLangId(9801, getLangId()).getName());
        ids.add(buttonRepo.findByIdAndLangId(9801, getLangId()).getName());

        for (Information infoCategory : categories) {
            if (getLangId() == 1)
                names.add(infoCategory.getNameRus());
            else names.add(infoCategory.getNameKaz());
            ids.add(String.valueOf(infoCategory.getId()));
        }

//        names.add(buttonRepo.findByIdAndLangId(19, getLangId()).getName());
//        ids.add(buttonRepo.findByIdAndLangId(19, getLangId()).getName());

        ButtonsLeaf buttonsLeaf = new ButtonsLeaf(names, ids);
        toDeleteMessage(sendMessageWithKeyboard("Выберите какую категорию хотите отредактировать?", buttonsLeaf.getListButtonWithDataList()));
        waitingType = WaitingType.CHOISE_SUB_CATEGORY;
    }

    private void getQuestionsByCategory(String userText) throws TelegramApiException{
        Information category = informationRepo.findById(Integer.parseInt(userText));
        List<InfoQuestion> questions = category.getInfoQuestions();
        List<String> names = new ArrayList<>();
        List<String> ids = new ArrayList<>();
        for (InfoQuestion question : questions) {
            if (getLangId() == 1)
                names.add(question.getQuestionRus());
            else names.add(question.getQuestionKaz());
            ids.add(String.valueOf(question.getId()));
        }
        names.add(buttonRepo.findByIdAndLangId(19, getLangId()).getName());
        ids.add(buttonRepo.findByIdAndLangId(19, getLangId()).getName());
        ButtonsLeaf buttonsLeaf = new ButtonsLeaf(names, ids);
        toDeleteMessage(sendMessageWithKeyboard(getText(16), buttonsLeaf.getListButtonWithDataList()));
        waitingType = WaitingType.CHOICE_OPTION;
    }

    private void deleteMessages() {
        deleteMessage(deleteId);
        deleteMessage(updateMessageId);
        deleteMessage(wrongDeleteId);
    }

    private void sendWrongData() throws TelegramApiException {
        wrongDeleteId = sendMessage(18);
    }

    private long getIdForNewInfoCategory()  {
        List<Information> infoCategories = informationRepo.findAllByOrderById();
        Information infoCategory = infoCategories.get(infoCategories.size()-1);
        long nextId = infoCategory.getId() + 1;
        return nextId;
    }

    private Information getInfoCategoryById(String id){
        Information infoCategory = informationRepo.findById(Integer.parseInt(id));
        return infoCategory;
    }

    private String getInfoMessages(InfoQuestion question, InfoAnswer answer) {

        StringBuilder stringBuilder = new StringBuilder();
        String name = null;
        if(currentLang.getId() == 1){
            stringBuilder.append(question.getQuestionRus())
                    .append(next).append(next)
                    .append(answer.getAnswerRus())
                    .append(next).append(next).append(" /switchLanguage");
            name = "ru";
        }else if(currentLang.getId() == 2){
            stringBuilder.append(question.getQuestionKaz())
                    .append(next).append(next)
                    .append(answer.getAnswerKaz())
                    .append(next).append(next).append(" /switchLanguage");
            name = "kz";
        }
        String info = stringBuilder.toString() + " " + name;
        return  info;
    }

    private String getInfoMessages() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("В данный момент вопрос " + question.getQuestionRus() + " отображается у пользователей, хотите его отключить?")
                .append(next)
                .append(next)
                .append(" /onOff");
        String info = stringBuilder.toString();
        return  info;
    }

}
