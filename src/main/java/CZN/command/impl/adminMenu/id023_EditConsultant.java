package CZN.command.impl.adminMenu;

import CZN.command.Command;
import CZN.enums.WaitingType;
import CZN.exceptions.ButtonNotFoundException;
import CZN.exceptions.CommandNotFoundException;
import CZN.exceptions.KeyboardNotFoundException;
import CZN.exceptions.MessageNotFoundException;
import CZN.model.custom.userMenu.Information;
import CZN.model.standart.Employee;
import CZN.model.standart.Role;
import CZN.model.standart.User;
import CZN.util.ButtonsLeaf;
import com.itextpdf.text.DocumentException;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class id023_EditConsultant extends Command {

    private int deleteId;
    private int wrongDeleteId;
    private Role role;
    private Employee employee;
    private int notRegisteredMessId;
    Integer editId;
    boolean isOn;

    @Override
    public boolean execute() throws TelegramApiException, IOException, SQLException, FileNotFoundException, MessageNotFoundException, KeyboardNotFoundException, ButtonNotFoundException, CommandNotFoundException, DocumentException {

        if (!isAdmin()) {
            sendMessage(5);
            return EXIT;
        }
        switch (waitingType) {
            case START:
                if(isButton(79)) {
                    deleteMessages();
                    getAllRoles();
                }else if(isButton(19)){
                    deleteMessages();
                    getAllRoles();
                    return COMEBACK;
                } else return COMEBACK;
                return COMEBACK;
            case CHOICE_QUESTION:
                if(hasCallbackQuery()) {
                    if (isButton(19)) {
                        deleteMessages();
                        getAllRoles();
                        return COMEBACK;
                    } else if (isButton(9801)) {
                        deleteMessages();
                        role = new Role();
                        sendMessage("Введите название раздела на русском языке");
                        long id = getIdForNewInfoCategory();
                        String stringId = String.valueOf(id);
                        role.setId(Integer.parseInt(stringId));
                        waitingType = WaitingType.SET_NAME_RU;
                    }else {
                        deleteMessages();
                        role = roleRepo.findById(Integer.parseInt(updateMessageText));
                        sendMessageWithKeyboard("Выберите действие", keyboardMarkUpService.select(150));
                        waitingType = WaitingType.CHOICE_OPTION;
                        return COMEBACK;
                    }
                }
                return COMEBACK;
            case SET_NAME_RU:
                if (hasMessageText()) {
                    deleteMessages();
                    role.setRussianName(updateMessageText);
                    deleteId = sendMessage("Введите название раздела на казахском языке");
                    waitingType = WaitingType.SET_NAME_KZ;
                    return COMEBACK;
                } else {
                    deleteMessages();
                    sendWrongData();
                    deleteId = sendMessage(getText(202));
                    return COMEBACK;
                }
            case SET_NAME_KZ:
                if (hasMessageText()) {
                    deleteMessages();
                    role.setKazName(updateMessageText);
                    roleRepo.save(role);
                    deleteId = sendMessage("Успешно добавлено");
                    getAllRoles();
                    return COMEBACK;
                } else {
                    deleteMessages();
                    sendWrongData();
                    deleteId = sendMessage(getText(202));
                    return COMEBACK;
                }
            case CHOICE_OPTION:
                if(isButton(19)){
                    deleteMessages();
                    getAllRoles();
                    return COMEBACK;
                }else if(isButton(152)){
                    deleteMessages();
                    System.out.println(role.getId() + "     " + role.getKazName());
                    getAllEmployeeByRole(String.valueOf(role.getId()));
                    return COMEBACK;
                }else if(isButton(151)){
                    deleteMessages();
                    sendMessage("Чтобы добавить консультанта введите номер телефона");
                    waitingType = WaitingType.SAVE_EMPLOYEE;
                    return COMEBACK;
                }else if(isButton(153)){
                    deleteMessages();
                    isOn = true;
                    String info = getInfoMessages();
                    editId = sendMessageWithKeyboard(info, 6);
                    System.out.println(role.getRussianName());
                    return COMEBACK;
                } else if (hasMessageText()){
                    if (updateMessageText.contains("/onOff")) {
                        deleteMessage(updateMessageId);
                        if (isOn) {
                            isOn = false;
                            List<Employee> employees = employeeRepo.findAll();
                            for (int i = 0; i < employees.size(); i++) {
                                List<Role> roles = employees.get(i).getRoleList();
                                for (int j = 0; j < roles.size(); j++) {
                                    if(roles.get(j).getId() == role.getId()){
                                        employees.get(i).getRoleList().remove(roleRepo.findById(role.getId()));
                                        employeeRepo.save(employees.get(i));
                                    }
                                }
                            }
                            roleRepo.delete(role);
                            String info = getInfoMessages() + " отключили";
                            editMessage(info, editId);
                            return COMEBACK;
                        }else {
                            isOn = true;
                            roleRepo.save(role);
                            String info = getInfoMessages() + " подключили";
                            editMessage(info, editId);
                            return COMEBACK;
                        }
                    }
                }
            case SAVE_EMPLOYEE:
                if (hasContact()) {
                    deleteMessages();
                    String phone = update.getMessage().getContact().getPhoneNumber();
                    if (phone.charAt(0) == '8') {
                        phone = phone.replaceFirst("8", "+7");
                    } else if (phone.charAt(0) == '7') {
                        phone = phone.replaceFirst("7", "+7");
                    }
                    saveEmployee(phone, String.valueOf(role.getId()));
                    return COMEBACK;
                }
                else if (hasMessageText() && isPhoneNumber(updateMessageText)) {
                    deleteMessages();
                    String phone = updateMessageText;

                    if (phone.charAt(0) == '8') {
                        phone = phone.replaceFirst("8", "+7");
                    } else if (phone.charAt(0) == '7') {
                        phone = phone.replaceFirst("7", "+7");
                    }
                    saveEmployee(phone, String.valueOf(role.getId()));
                    return COMEBACK;
                }
            case CHOICE_CONSULTANT:
                if(hasCallbackQuery()){
                    deleteMessages();
                    if(isButton(19)){
                        getAllRoles();
                        return COMEBACK;
                    }else {
                        deleteMessages();
                        System.out.println(updateMessageText);
                        employee = employeeRepo.findById(Integer.parseInt(updateMessageText));
                        sendMessage("Введите новое ФИО консультанта");
                        waitingType = WaitingType.ENTER_OFFER;
                        return COMEBACK;
                    }
                }
                return COMEBACK;
            case ENTER_OFFER:
                if (hasMessageText()) {
                    deleteMessages();
                    employee.setFullName(updateMessageText);
                    employeeRepo.save(employee);
                    deleteId = sendMessage("Успешно изменено");
                    return COMEBACK;
                } else {
                    deleteMessages();
                    sendWrongData();
                    deleteId = sendMessage(getText(202));
                    return COMEBACK;
                }

        }
        return EXIT;
    }

    private boolean saveEmployee(String phone, String roleId) throws TelegramApiException {

        Role employeeType = roleRepo.findById(Integer.parseInt(roleId));

        User newAdmin = usersRepo.findByPhone(phone);
        List<Role> roleList = new ArrayList<>();
        if (newAdmin == null) {
            deleteMessage(notRegisteredMessId);
            notRegisteredMessId = sendMessageWithKeyboard(getText(14), 6);
            return COMEBACK;
        }
        Employee employee = null;
        if(employeeRepo.findByChatId(newAdmin.getChatId()) == null){
            employee = new Employee(usersRepo.findByPhone(phone).getChatId(), newAdmin.getFullName(), roleList);;
            employee.getRoleList().add(employeeType);
            employeeRepo.save(employee);
        }else {
            employee = employeeRepo.findByChatId(newAdmin.getChatId());

            for (int i = 0; i < employee.getRoleList().size(); i++) {
                if(employeeType.getId() == employee.getRoleList().get(i).getId()){
                    String mess = "<b> Такой пользователь с таким ролем уже существует </b> \n \n";
                    return COMEBACK;
                }
            }

            employee.getRoleList().add(employeeType);
            employeeRepo.save(employee);
        }
        sendMessageWithKeyboard("Успешно сохранено", 6);
        return COMEBACK;
    }

    private void getAllRoles() throws TelegramApiException {
        List<Role> categories = roleRepo.findAllByOrderById();
        List<String> names = new ArrayList<>();
        List<String> ids = new ArrayList<>();

        names.add(buttonRepo.findByIdAndLangId(9801, getLangId()).getName());
        ids.add(buttonRepo.findByIdAndLangId(9801, getLangId()).getName());

        for (Role role : categories) {
            if (getLangId() == 1)
                names.add(role.getRussianName());
            else names.add(role.getKazName());
            ids.add(String.valueOf(role.getId()));
        }

//        names.add(buttonRepo.findByIdAndLangId(19, getLangId()).getName());
//        ids.add(buttonRepo.findByIdAndLangId(19, getLangId()).getName());

        ButtonsLeaf buttonsLeaf = new ButtonsLeaf(names, ids);
        toDeleteMessage(sendMessageWithKeyboard(getText(15), buttonsLeaf.getListButtonWithDataList()));
        waitingType = WaitingType.CHOICE_QUESTION;
    }

    private void getAllEmployeeByRole(String userText) throws TelegramApiException{

        Role role = roleRepo.findById(Integer.parseInt(userText));
        List<Employee> allEmployees = employeeRepo.findAll();

        Set<Employee> employeesWithRole = new HashSet<>();

        List<String> names = new ArrayList<>();
        List<String> ids = new ArrayList<>();

        for (int i = 0; i < allEmployees.size(); i++) {
            Employee employee = allEmployees.get(i);
            System.out.println(employee);
            List<Role> employeeRoles = employee.getRoleList();
            System.out.println(employeeRoles);
            for (int j = 0; j < employeeRoles.size(); j++) {
                if(role.getId() == employeeRoles.get(j).getId()){
                    employeesWithRole.add(employee);
                }
            }
        }

        for (Employee employee : employeesWithRole) {
            if (getLangId() == 1)
                names.add(employee.getFullName());
            ids.add(String.valueOf(employee.getId()));
        }

        names.add(buttonRepo.findByIdAndLangId(19, getLangId()).getName());
        ids.add(buttonRepo.findByIdAndLangId(19, getLangId()).getName());

        ButtonsLeaf buttonsLeaf = new ButtonsLeaf(names, ids);
        toDeleteMessage(sendMessageWithKeyboard(getText(16), buttonsLeaf.getListButtonWithDataList()));
        waitingType = WaitingType.CHOICE_CONSULTANT;
    }

    private void deleteMessages() {
        deleteMessage(deleteId);
        deleteMessage(updateMessageId);
        deleteMessage(wrongDeleteId);
    }

    private void sendWrongData() throws TelegramApiException {
        wrongDeleteId = sendMessage(18);
    }

    private long getIdForNewInfoCategory()  {
        List<Role> infoCategories = roleRepo.findAllByOrderById();
        Role infoCategory = infoCategories.get(infoCategories.size()-1);
        long nextId = infoCategory.getId() + 1;
        return nextId;
    }

    private boolean isPhoneNumber(String phone) {

        if (phone.charAt(0) == '8') {
            phone = phone.replaceFirst("8", "+7");
        } else if (phone.charAt(0) == '7') {
            phone = phone.replaceFirst("7", "+7");
        }
        return phone.charAt(0) == '+' && phone.charAt(1) == '7' && phone.substring(2).length() == 10 && isLong(phone.substring(2));
    }

    protected boolean isLong(String mess) {
        try {
            Long.parseLong(mess);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    private String getInfoMessages() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("В данный момент раздел " + role.getRussianName() + " отображается у пользователей, хотите его отключить?")
                .append(next)
                .append(next)
                .append(" /onOff");
        String info = stringBuilder.toString();
        return  info;
    }
}
