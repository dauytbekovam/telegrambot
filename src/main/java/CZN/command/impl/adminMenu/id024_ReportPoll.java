package CZN.command.impl.adminMenu;

import CZN.command.Command;
import CZN.enums.WaitingType;
import CZN.exceptions.ButtonNotFoundException;
import CZN.exceptions.CommandNotFoundException;
import CZN.exceptions.KeyboardNotFoundException;
import CZN.exceptions.MessageNotFoundException;
import CZN.model.custom.userMenu.Poll;
import CZN.model.custom.userMenu.PollInterview;
import CZN.service.PollService;
import CZN.util.ButtonsLeaf;
import CZN.util.Const;
import CZN.util.DateKeyboard;
import com.itextpdf.text.DocumentException;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class id024_ReportPoll extends Command {

    private DateKeyboard dateKeyboard;
    private Date start;
    private Date end;
    private int deleteId;
    private PollInterview interview;
    private List<Poll> polls;


    @Override
    public boolean execute() throws TelegramApiException, IOException, SQLException, FileNotFoundException,
            MessageNotFoundException, KeyboardNotFoundException, ButtonNotFoundException, CommandNotFoundException, DocumentException {

        if (!isAdmin() && !isAdmin()) {
            sendMessage(Const.NO_ACCESS);
            return EXIT;
        }
        switch (waitingType) {
            case START:
              if (isButton(102)){
                  deleteMessage();
                  return getInterviews();
              }
            case CHOOSE_OPTION:
                if (hasCallbackQuery()) {
                    deleteMessage(updateMessageId);
                    if (isButton(39)) {
                        return EXIT;
                    } else {
                        interview = interviewRepo.findById(Integer.parseInt(updateMessageText));
                        deleteMessage(updateMessageId);
                        dateKeyboard = new DateKeyboard();
                        sendStartDate();
                        waitingType = WaitingType.START_DATE;
                        return COMEBACK;
                    }
                } else {
                    deleteMessage();
                    deleteMessage(updateMessageId);
                    return getInterviews();
                }
            case START_DATE:
                deleteMessage(updateMessageId);
                if (hasCallbackQuery()) {
                    if (dateKeyboard.isNext(updateMessageText)) {
                        sendStartDate();
                        return COMEBACK;
                    }
                    start = dateKeyboard.getDateDate(updateMessageText);
                    start.setHours(0);
                    start.setMinutes(0);
                    start.setSeconds(0);
                    sendEndDate();
                    waitingType = WaitingType.END_DATE;
                }
                return COMEBACK;
            case END_DATE:
                deleteMessage(updateMessageId);
                if (hasCallbackQuery()) {
                    if (dateKeyboard.isNext(updateMessageText)) {
                        sendStartDate();
                        return COMEBACK;
                    }
                    end = dateKeyboard.getDateDate(updateMessageText);
                    end.setHours(23);
                    end.setMinutes(59);
                    end.setSeconds(59);
                    sendReport();
                    waitingType = WaitingType.END_DATE;
                    return COMEBACK;
                }

                return COMEBACK;
        }
        return EXIT;
    }

    private boolean getInterviews() throws TelegramApiException {

        List<PollInterview> interviewList = interviewRepo.findAllByOrderById();

        List<String> names = new ArrayList<>();
        List<String> ids = new ArrayList<>();

        for (PollInterview interview : interviewList) {
            if (getLangId() == 1) {
                names.add(interview.getNameRU());
                ids.add(String.valueOf(interview.getId()));
            } else if (getLangId() == 2) {
                names.add(interview.getNameKZ());
                ids.add(String.valueOf(interview.getId()));
            }
        }

        names.add(buttonRepo.findByIdAndLangId(39, getLangId()).getName());
        ids.add(buttonRepo.findByIdAndLangId(39, getLangId()).getName());

        ButtonsLeaf buttonsLeaf = new ButtonsLeaf(names, ids);

        deleteId = sendMessageWithKeyboard("ВЫберите вид отчета", buttonsLeaf.getListButtonWithDataList());

        waitingType = WaitingType.CHOOSE_OPTION;

        return COMEBACK;

    }

    private int sendStartDate() throws TelegramApiException {
        return toDeleteKeyboard(sendMessageWithKeyboard(getText(113),
                dateKeyboard.getCalendarKeyboard()));
    }

    private int sendEndDate() throws TelegramApiException {
        return toDeleteKeyboard(sendMessageWithKeyboard(114, dateKeyboard.getCalendarKeyboard()));
    }

    private void sendReport() throws TelegramApiException {
        int preview = sendMessage("Отчет подготавливается...");
        polls = pollRepo.findAllByDateBetweenAndInterview(start, end, interview.getId());
        PollService complaintsService = new PollService();
        complaintsService.sendPollService(chatId, bot, start, end, polls, interview);
        deleteMessage(preview);
    }
}
