package CZN.command.impl.adminMenu;

import CZN.command.Command;
import CZN.enums.WaitingType;
import CZN.exceptions.ButtonNotFoundException;
import CZN.exceptions.CommandNotFoundException;
import CZN.exceptions.KeyboardNotFoundException;
import CZN.exceptions.MessageNotFoundException;
import CZN.model.standart.Employee;
import CZN.model.standart.Role;
import CZN.model.standart.User;
import CZN.util.ButtonsLeaf;
import com.itextpdf.text.DocumentException;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class id018_EditEmployee extends Command {

    private int messId;
    private int notRegisteredMessId;
    private int alreadyAdminMessId;
    private Role chosen;


    @Override
    public boolean execute() throws TelegramApiException, IOException, SQLException, FileNotFoundException, MessageNotFoundException, KeyboardNotFoundException, ButtonNotFoundException, CommandNotFoundException, DocumentException {
        if (!isRegistered()) {
            deleteUpdateMess();
            deleteNotRegisteredMessId();
            deleteAlreadyAdminMessId();
            sendMessageWithKeyboard(getText(10), 5);
            return EXIT;
        }
        if (!isAdmin()){
            deleteUpdateMess();
            deleteNotRegisteredMessId();
            deleteAlreadyAdminMessId();
            sendMessage(5);
            return EXIT;
        }
        switch (waitingType){
            case START:
                deleteUpdateMess();
                deleteNotRegisteredMessId();
                deleteAlreadyAdminMessId();
                getAllRoles();
                return COMEBACK;
            case CHOICE_QUESTION:
                deleteUpdateMess();
                deleteNotRegisteredMessId();
                deleteAlreadyAdminMessId();
                if(hasCallbackQuery()){
                    if(isButton(19)){
                        getAllRoles();
                        return COMEBACK;
                    }else {
                        deleteUpdateMess();
                        deleteNotRegisteredMessId();
                        deleteAlreadyAdminMessId();
                        chosen = roleRepo.findById(Integer.parseInt(updateMessageText));
                        sendMessageWithKeyboard(getEmployeeList(String.valueOf(chosen.getId())), 6);
                        waitingType = WaitingType.CHOICE_CONSULTANT;
                    }
                }
                return COMEBACK;
            case CHOICE_CONSULTANT:
                deleteUpdateMess();
                deleteNotRegisteredMessId();
                deleteAlreadyAdminMessId();
                if(hasCallbackQuery()){
                    if(isButton(19)){
                        getAllRoles();
                        return COMEBACK;
                    }
                }
                if (hasContact()) {
                    deleteMessage(messId);
                    String phone = update.getMessage().getContact().getPhoneNumber();
                    if (phone.charAt(0) == '8') {
                        phone = phone.replaceFirst("8", "+7");
                    } else if (phone.charAt(0) == '7') {
                        phone = phone.replaceFirst("7", "+7");
                    }
                    saveEmployee(phone, String.valueOf(chosen.getId()));
                    return COMEBACK;
                }
                else if (hasMessageText() && isPhoneNumber(updateMessageText)) {
                    deleteMessage(messId);
                    String phone = updateMessageText;

                    if (phone.charAt(0) == '8') {
                        phone = phone.replaceFirst("8", "+7");
                    } else if (phone.charAt(0) == '7') {
                        phone = phone.replaceFirst("7", "+7");
                    }
                    saveEmployee(phone, String.valueOf(chosen.getId()));
                    return COMEBACK;
                }
                else if (hasMessageText() && updateMessageText.contains("/del")) {
                    deleteMessage(messId);
                    System.out.println(updateMessageText);
                    if (employeeRepo.findAll().size() == 1) {
                        return COMEBACK;
                    }
                    int delAdminId = getDelEmployeeId(updateMessageText);
                    System.out.println(delAdminId);
                    System.out.println(chosen);
                    Employee employee = employeeRepo.findById(delAdminId);
                    if(employee != null){
                        employee.getRoleList().remove(chosen);
                        employeeRepo.save(employee);
                        deleteUpdateMess();
                        deleteNotRegisteredMessId();
                        deleteAlreadyAdminMessId();
                        messId = sendMessageWithKeyboard(getEmployeeList(String.valueOf(chosen.getId())), 6);
                        return COMEBACK;
                    }
                    return COMEBACK;
                }
                else {
                    deleteMessage(messId);
                    if (messId == 0) {
                        deleteUpdateMess();
                        deleteNotRegisteredMessId();
                        deleteAlreadyAdminMessId();
                        messId = sendMessageWithKeyboard(getEmployeeList(String.valueOf(chosen.getId())), 6);
                    }
                }
                return COMEBACK;
        }
        return EXIT;
    }





    private void deleteUpdateMess() {
        deleteMessage(updateMessageId);
    }

    private void deleteNotRegisteredMessId() {
        if (notRegisteredMessId != 0)
            deleteMessage(notRegisteredMessId);



    }

    private void deleteAlreadyAdminMessId() {
        if (alreadyAdminMessId != 0)
            deleteMessage(alreadyAdminMessId);
    }

    private boolean isPhoneNumber(String phone) {

        if (phone.charAt(0) == '8') {
            phone = phone.replaceFirst("8", "+7");
        } else if (phone.charAt(0) == '7') {
            phone = phone.replaceFirst("7", "+7");
        }
        return phone.charAt(0) == '+' && phone.charAt(1) == '7' && phone.substring(2).length() == 10 && isLong(phone.substring(2));
    }

    protected boolean isLong(String mess) {
        try {
            Long.parseLong(mess);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    private int getDelEmployeeId(String updateMessageText) {
        try {
            return Integer.parseInt(updateMessageText.substring(4));
        } catch (Exception e) {
            return -1;
        }
    }

    private boolean saveEmployee(String phone, String roleId) throws TelegramApiException {

        Role employeeType = roleRepo.findById(Integer.parseInt(roleId));

        User newAdmin = usersRepo.findByPhone(phone);
        List<Role> roleList = new ArrayList<>();
        if (newAdmin == null) {
            deleteMessage(notRegisteredMessId);
            deleteUpdateMess();
            deleteNotRegisteredMessId();
            deleteAlreadyAdminMessId();
            notRegisteredMessId = sendMessage(14);
            return COMEBACK;
        }
        Employee employee = null;
        if(employeeRepo.findByChatId(newAdmin.getChatId()) == null){
            employee = new Employee(usersRepo.findByPhone(phone).getChatId(), newAdmin.getFullName(), roleList);;
            employee.getRoleList().add(employeeType);
            employeeRepo.save(employee);
        }else {
            employee = employeeRepo.findByChatId(newAdmin.getChatId());

            for (int i = 0; i < employee.getRoleList().size(); i++) {
                if(employeeType.getId() == employee.getRoleList().get(i).getId()){
                    deleteUpdateMess();
                    deleteNotRegisteredMessId();
                    deleteAlreadyAdminMessId();
                    String mess = "<b> Такой пользователь с таким ролем уже существует </b> \n \n";
                    messId = sendMessage(mess + getEmployeeList(String.valueOf(employeeType.getId())), chatId);
                    //editMessage(mess + getEmployeeList(String.valueOf(employeeType.getId())), chatId, messId);
                    return COMEBACK;
                }
            }

            employee.getRoleList().add(employeeType);
            employeeRepo.save(employee);
        }
        deleteUpdateMess();
        deleteNotRegisteredMessId();
        deleteAlreadyAdminMessId();

        messId = sendMessageWithKeyboard(getEmployeeList(String.valueOf(chosen.getId())), 6);
        //editMessage(getEmployeeList(String.valueOf(chosen.getId())), chatId, messId);
        return COMEBACK;
    }

    private String getEmployeeList(String userText) throws TelegramApiException {
        StringBuilder employees = new StringBuilder();
        employees.append(getText(1803)).append(next).append(next);

        Set<Employee> adminList = getAllEmployeeByRole(userText);

        for (Employee admin : adminList) {
            try {
                employees.append(admin.getFullName()).append(" ");
                if (adminList.size() > 1)
                    employees.append("❌ /del").append(admin.getId()).append(next);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        employees.append(next).append(next).append(getText(11));
        adminList.clear();
        return employees.toString();
    }

    private Set<Employee> getAllEmployeeByRole(String userText) throws TelegramApiException{

        Role role = roleRepo.findById(Integer.parseInt(userText));
        List<Employee> allEmployees = employeeRepo.findAll();

        Set<Employee> employeesWithRole = new HashSet<>();

        for (int i = 0; i < allEmployees.size(); i++) {
            Employee employee = allEmployees.get(i);
            List<Role> employeeRoles = employee.getRoleList();
            for (int j = 0; j < employeeRoles.size(); j++) {
                if(role.getId() == employeeRoles.get(j).getId()){
                    employeesWithRole.add(employee);
                }
            }
        }
        return employeesWithRole;
    }

    private void getAllRoles() throws TelegramApiException {
        List<Role> categories = roleRepo.findAllByOrderById();
        List<String> names = new ArrayList<>();
        List<String> ids = new ArrayList<>();
        for (Role role : categories) {
            if (getLangId() == 1)
                names.add("Редактор " + role.getRussianName());
            else names.add(role.getKazName());
            ids.add(String.valueOf(role.getId()));
        }
        names.add(buttonRepo.findByIdAndLangId(19, getLangId()).getName());
        ids.add(buttonRepo.findByIdAndLangId(19, getLangId()).getName());
        ButtonsLeaf buttonsLeaf = new ButtonsLeaf(names, ids);
        deleteUpdateMess();
        deleteNotRegisteredMessId();
        deleteAlreadyAdminMessId();
        toDeleteMessage(sendMessageWithKeyboard(getText(15), buttonsLeaf.getListButtonWithDataList()));
        waitingType = WaitingType.CHOICE_QUESTION;
    }
}
