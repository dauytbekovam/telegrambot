package CZN.command.impl.adminMenu;

import CZN.command.Command;
import CZN.enums.FileType;
import CZN.enums.WaitingType;
import CZN.exceptions.ButtonNotFoundException;
import CZN.exceptions.CommandNotFoundException;
import CZN.exceptions.KeyboardNotFoundException;
import CZN.exceptions.MessageNotFoundException;
import CZN.model.custom.userMenu.AboutProjectOffer;
import CZN.model.custom.userMenu.Reminder;
import CZN.model.standart.User;
import CZN.service.SaveDocument;
import CZN.util.DateKeyboard;
import com.itextpdf.text.DocumentException;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.telegram.telegrambots.meta.api.methods.send.SendDocument;
import org.telegram.telegrambots.meta.api.methods.send.SendMediaGroup;
import org.telegram.telegrambots.meta.api.methods.send.SendPhoto;
import org.telegram.telegrambots.meta.api.methods.send.SendVideo;
import org.telegram.telegrambots.meta.api.objects.InputFile;
import org.telegram.telegrambots.meta.api.objects.media.InputMedia;
import org.telegram.telegrambots.meta.api.objects.media.InputMediaDocument;
import org.telegram.telegrambots.meta.api.objects.media.InputMediaPhoto;
import org.telegram.telegrambots.meta.api.objects.media.InputMediaVideo;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.*;

@Slf4j
public class id020_SendMessageSheduled extends Command {

    Map<String, FileType> files;
    int deleteId;
    AboutProjectOffer offer;
    int hour;
    int minute;
    private DateKeyboard dateKeyboard;
    private Date startDate;

    @Override
    public boolean execute() throws TelegramApiException, IOException, SQLException, FileNotFoundException, MessageNotFoundException, KeyboardNotFoundException, ButtonNotFoundException, CommandNotFoundException, DocumentException {

        if (!isAdmin()) {
            sendMessage(5);
            return EXIT;
        }

        switch (waitingType) {
            case START:
                if (isButton(105)) {
                    dateKeyboard = new DateKeyboard();
                    sendStartDate();
                    waitingType = WaitingType.START_DATE;
                    return COMEBACK;
                } else return COMEBACK;
            case START_DATE:
                deleteMessage(updateMessageId);
                if (hasCallbackQuery()) {
                    if (dateKeyboard.isNext(updateMessageText)) {
                        sendStartDate();
                        return COMEBACK;
                    }
                    startDate = dateKeyboard.getDateDate(updateMessageText);
                    deleteId = sendMessage("Напишите во сколько необходимо отправить рассылку через '<b> : </b>' \n \n (например 9:00)");
                    waitingType = WaitingType.SET_SCHEDULE;
                    return COMEBACK;
                }
                return COMEBACK;
            case SET_SCHEDULE:
                if (hasMessageText()) {
                    String str = updateMessageText;
                    String[] words = str.split(":");
                    try {
                        System.out.println(words.length);
                        if (words.length <= 1) {
                            sendWrongData();
                            deleteId = sendMessage("Напишите во сколько необходимо отправить рассылку через '<b> : </b>' \n \n (например 9:00)");
                            waitingType = WaitingType.SET_SCHEDULE;
                        } else {
                            int enteredHour = Integer.parseInt(words[0]);
                            int enteredMinute = Integer.parseInt(words[1]);
                            System.out.println(words[1]);
                            if (hour >= 0 && hour <= 23 && minute >= 0 && minute <= 60) {
                                hour = enteredHour;
                                minute = enteredMinute;
                            } else {
                                sendWrongData();
                                deleteId = sendMessage("Напишите во сколько необходимо отправить рассылку через '<b> : </b>' \n \n (например 9:00)");
                                waitingType = WaitingType.SET_SCHEDULE;
                                return COMEBACK;
                            }
                        }
                    } catch (NumberFormatException exception) {
                        sendWrongData();
                        deleteId = sendMessage("Напишите во сколько необходимо упомянуть \n \n (например 9:00)");
                        waitingType = WaitingType.GET_HOUR;
                        return COMEBACK;
                    }
                    sendMessage(21);
                    waitingType = WaitingType.ENTER_OFFER;
                } else return COMEBACK;
                return COMEBACK;
            case ENTER_OFFER:
                if (update.hasMessage() && update.getMessage().hasText()) {
                    files = new HashMap<>();
                    offer = new AboutProjectOffer();
                    User user = usersRepo.findByChatId(chatId);
                    offer.setOffer(update.getMessage().getText());
                    offer.setUser(user);
                    offer.setAccepted(false);
                    offer.setDate(new Date());
                    sendMessageWithKeyboard(getText(71), 30);
                    waitingType = WaitingType.SET_FILEOFFERRR;
                    return COMEBACK;
                } else {
                    sendWrongData();
                    deleteMessages();
                    sendMessage(21);
                    waitingType = WaitingType.ENTER_OFFER;
                    return COMEBACK;
                }
            case SET_FILEOFFERRR:
                if (hasCallbackQuery()) {
                    if (isButton(94)) {
                        System.out.println(getText(131).toLowerCase(Locale.ROOT));
                        sendMessageWithKeyboard(getText(131), 33);
                        waitingType = WaitingType.SET_OFFERFILES;
                        return COMEBACK;
                    } else if (isButton(93)) {
                        sendMessage(getText(119));
                        waitingType = WaitingType.SET_FILEOFFERRE;
                        return COMEBACK;
                    }
                } else {
                    sendWrongData();
                    sendMessageWithKeyboard(getText(71), 30);
                    waitingType = WaitingType.SET_FILEOFFERRR;
                    return COMEBACK;
                }
            case SET_FILEOFFERRE:
                if (update.hasMessage() && update.getMessage().hasPhoto()) {
                    if (files.size() < 11) {
                        files.put(updateMessagePhoto, FileType.photo);
                        sendMessage(120);
                        sendMessageWithKeyboard(getText(71), 30);
                        waitingType = WaitingType.SET_FILEOFFERRR;
                        return COMEBACK;
                    } else {
                        sendMessageWithKeyboard(getText(132), 33);
                        waitingType = WaitingType.SET_OFFERFILES;
                        return COMEBACK;
                    }

                } else if (update.hasMessage() && update.getMessage().hasVideo()) {
                    if (files.size() < 11) {
                        files.put(updateMessageVideo, FileType.video);
                        sendMessage(120);
                        sendMessageWithKeyboard(getText(71), 30);
                        waitingType = WaitingType.SET_FILEOFFERRR;
                        return COMEBACK;
                    } else {
                        sendMessageWithKeyboard(getText(132), 33);
                        waitingType = WaitingType.SET_OFFERFILES;
                        return COMEBACK;
                    }
                } else if (update.hasMessage() && update.getMessage().hasDocument()) {
                    if (files.size() < 11) {
                        files.put(updateMessageDocument, FileType.document);
                        sendMessage(120);
                        sendMessageWithKeyboard(getText(71), 30);
                        waitingType = WaitingType.SET_FILEOFFERRR;
                        return COMEBACK;
                    } else {
                        sendMessageWithKeyboard(getText(132), 33);
                        waitingType = WaitingType.SET_OFFERFILES;
                        return COMEBACK;
                    }
                }
            case SET_OFFERFILES:
                if (hasCallbackQuery()) {
                    if (isButton(96)) {
                        Set<String> chatIds = new HashSet<>();

                        for (User user : usersRepo.findAll()) {
                            chatIds.add(String.valueOf(user.getChatId()));
                        }

                        TimerTask timerTask = new TimerTask() {
                            @SneakyThrows
                            @Override
                            public void run() {
                                StringBuilder stringBuilder = new StringBuilder();

                                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
                                stringBuilder.append(getText(27)).append(offer.getUser().getFullName()).append(next).
                                        append(getText(28)).append(offer.getUser().getPhone()).append(next).
                                        append(getText(38)).append(offer.getOffer()).append(next).
                                        append(getText(30)).append(simpleDateFormat.format(offer.getDate()));

                                int sizePhoto = 0;
                                int sizeVideo = 0;
                                int sizeDocument = 0;
                                List<String> photos = new ArrayList<>();
                                List<String> videos = new ArrayList<>();
                                List<String> document = new ArrayList<>();


                                for (Map.Entry<String, FileType> s : files.entrySet()) {
                                    if (s.getValue().equals(FileType.photo)) {
                                        sizePhoto++;
                                        photos.add(s.getKey());
                                    } else if (s.getValue().equals(FileType.video)) {
                                        sizeVideo++;
                                        videos.add(s.getKey());
                                    } else if (s.getValue().equals(FileType.document)) {
                                        sizeDocument++;
                                        document.add(s.getKey());
                                    }
                                }

                                List<InputMedia> media = new ArrayList<>();
                                List<InputMedia> mediaDocument = new ArrayList<>();
                                SendDocument sendDocument = new SendDocument();
                                SendVideo sendVideo = new SendVideo();
                                SendPhoto sendPhoto = new SendPhoto();
                                InputMediaPhoto inputMediaPhoto;
                                InputMediaVideo inputMediaVideo;


                                if (sizePhoto > 0 && sizeVideo > 0) {
                                    for (String s : photos) {
                                        inputMediaPhoto = new InputMediaPhoto();
                                        inputMediaPhoto.setMedia(s);
                                        media.add(inputMediaPhoto);
                                    }

                                    for (String s : videos) {
                                        inputMediaVideo = new InputMediaVideo();
                                        inputMediaVideo.setMedia(s);
                                        media.add(inputMediaVideo);
                                    }
                                } else if (sizePhoto > 0 && sizeVideo == 0) {
                                    for (String s : photos) {
                                        inputMediaPhoto = new InputMediaPhoto();
                                        inputMediaPhoto.setMedia(s);
                                        media.add(inputMediaPhoto);
                                    }
                                } else if (sizePhoto == 0 && sizeVideo > 0) {
                                    for (String s : videos) {
                                        inputMediaVideo = new InputMediaVideo();
                                        inputMediaVideo.setMedia(s);
                                        media.add(inputMediaVideo);
                                    }
                                }
                                InputMediaDocument inputMediaDocument;
                                if (sizeDocument > 0) {
                                    for (String s : document) {
                                        inputMediaDocument = new InputMediaDocument();
                                        inputMediaDocument.setMedia(s);
                                        mediaDocument.add(inputMediaDocument);
                                    }

                                }
                                SendMediaGroup sendMediaGroupDoc = new SendMediaGroup();
                                SendMediaGroup sendMediaGroup = new SendMediaGroup();

                                if (mediaDocument.size() > 1) {
                                    sendMediaGroupDoc.setMedias(mediaDocument);
                                    for (String admin : chatIds) {
                                        sendMediaGroupDoc.setChatId(admin);
                                        try {
                                            bot.execute(sendMediaGroupDoc);
                                        } catch (Exception e) {
                                            log.info("This bitch deleted our bot or blocked -> " + admin);
                                        }
                                    }

                                } else if (sizeDocument == 1) {
                                    sendDocument.setDocument(new InputFile(document.get(0)));
                                    for (String admin : chatIds) {
                                        sendDocument.setChatId(admin);
                                        try {
                                            bot.execute(sendDocument);
                                        } catch (Exception e) {
                                            log.info("This bitch deleted our bot or blocked -> " + admin);
                                        }
                                    }
                                }

                                if (media.size() > 0) {
                                    if (photos.size() == 0 && videos.size() == 1) {
                                        sendVideo.setVideo(new InputFile(videos.get(0)));
                                        sendVideo.setParseMode("html");
                                        sendVideo.setCaption(stringBuilder.toString());
//                                        sendVideo.setReplyMarkup(keyboardMarkUpService.select(9));

                                        for (String admin : chatIds) {
                                            sendVideo.setChatId(admin);
                                            try {
                                                bot.execute(sendVideo);
                                            } catch (Exception e) {
                                                log.info("This bitch deleted our bot or blocked -> " + admin);
                                            }
                                        }
                                        sendMessage("<b>Успешно отправлено!</b>");

                                    } else if (photos.size() == 1 && videos.size() == 0) {
                                        sendPhoto.setPhoto(new InputFile(photos.get(0)));
                                        sendPhoto.setParseMode("html");
                                        sendPhoto.setCaption(stringBuilder.toString());
//                                        sendPhoto.setReplyMarkup(keyboardMarkUpService.select(9));

                                        for (String admin : chatIds) {
                                            sendPhoto.setChatId(admin);
                                            try {
                                                bot.execute(sendPhoto);
                                            } catch (Exception e) {
                                                log.info("This bitch deleted our bot or blocked -> " + admin);
                                            }
                                        }
                                        sendMessage("<b>Успешно отправлено!</b>");

                                    } else {
                                        sendMediaGroup.setMedias(media);

                                        for (String admin : chatIds) {
                                            sendMediaGroup.setChatId(admin);
                                            try {
                                                bot.execute(sendMediaGroup);
                                            } catch (Exception e) {
                                                log.info("This bitch deleted our bot or blocked -> " + admin);
                                            }
                                            sendMessage(stringBuilder.toString(), Long.parseLong(admin));
                                        }
                                        sendMessage("<b>Успешно отправлено!</b>");

                                    }
                                } else {
                                    for (String admin : chatIds) {
                                        sendMessage(stringBuilder.toString(), Long.parseLong(admin));
                                    }
                                    sendMessage("<b>Успешно отправлено!</b>");
                                }
                            }
                        };
                        Timer timer = new Timer();
                        startDate.setHours(hour);
                        startDate.setMinutes(minute);
                        timer.schedule(timerTask, startDate);

                        sendMessage("Ваша рассылка будет отправлено в " + startDate.getHours() + ":" +
                                startDate.getMinutes() + " , " + startDate.getDate() + "." + startDate.getMonth()+ "." + startDate.getYear() + " года");
                        return COMEBACK;
                    }

                }
        }
        return EXIT;
    }


    private void sendWrongData() throws TelegramApiException {
        toDeleteMessage(sendMessage(18));
    }

    private void deleteMessages() {
        deleteMessage(updateMessageId);
        deleteMessage(deleteId);
    }

    private int sendStartDate() throws TelegramApiException {
        return toDeleteKeyboard(sendMessageWithKeyboard("Выберите день когда необходимо отправить рассылку", dateKeyboard.getCalendarKeyboard()));
    }
}
