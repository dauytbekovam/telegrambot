package CZN.command.impl.adminMenu;

import CZN.command.Command;
import CZN.enums.WaitingType;
import CZN.exceptions.ButtonNotFoundException;
import CZN.exceptions.CommandNotFoundException;
import CZN.exceptions.KeyboardNotFoundException;
import CZN.exceptions.MessageNotFoundException;
import CZN.service.ComplaintsService;
import CZN.util.Const;
import CZN.util.DateKeyboard;
import com.itextpdf.text.DocumentException;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Date;

public class id025_ReportConsulting extends Command {

    private DateKeyboard dateKeyboard;


    private Date start;
    private Date end;


    @Override
    public boolean execute() throws TelegramApiException, IOException, SQLException, FileNotFoundException, MessageNotFoundException, KeyboardNotFoundException, ButtonNotFoundException, CommandNotFoundException, DocumentException {
        if (usersRepo.findByChatId(chatId) == null) {
            sendMessageWithKeyboard("Вы не прошли регистрацию", 57);
            return EXIT;
        }
        if (!isAdmin() && !isAdmin()) {
            sendMessage(Const.NO_ACCESS);
            return EXIT;
        }
        switch (waitingType) {
            case START:
                if (isButton(300)) {
                    deleteMessage(updateMessageId);
                    dateKeyboard = new DateKeyboard();
                    sendStartDate();
                    waitingType = WaitingType.START_DATE;
                    return COMEBACK;
                }
            case START_DATE:
                deleteMessage(updateMessageId);
                if (hasCallbackQuery()) {
                    if (dateKeyboard.isNext(updateMessageText)) {
                        sendStartDate();
                        return COMEBACK;
                    }
                    start = dateKeyboard.getDateDate(updateMessageText);
                    start.setHours(0);
                    start.setMinutes(0);
                    start.setSeconds(0);
                    sendEndDate();
                    waitingType = WaitingType.END_DATE;
                }
                return COMEBACK;
            case END_DATE:
                deleteMessage(updateMessageId);
                if (hasCallbackQuery()) {
                    if (dateKeyboard.isNext(updateMessageText)) {
                        sendStartDate();
                        return COMEBACK;
                    }
                    end = dateKeyboard.getDateDate(updateMessageText);
                    end.setHours(23);
                    end.setMinutes(59);
                    end.setSeconds(59);
                    sendReport();
                    waitingType = WaitingType.END_DATE;
                    return COMEBACK;
                }
                return COMEBACK;
        }
        return EXIT;
    }


    private int sendStartDate() throws TelegramApiException {
        return toDeleteKeyboard(sendMessageWithKeyboard(getText(113), dateKeyboard.getCalendarKeyboard()));
    }

    private int sendEndDate() throws TelegramApiException {
        return toDeleteKeyboard(sendMessageWithKeyboard(114, dateKeyboard.getCalendarKeyboard()));
    }

    private void sendReport() throws TelegramApiException {
        int preview = sendMessage("Отчет подготавливается...");
        ComplaintsService complaintsService = new ComplaintsService(messageRepo, messageToConsultantRepo);
        complaintsService.sendComplaitsService(chatId, bot, start, end, preview);
    }

}
