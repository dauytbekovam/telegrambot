package CZN.command.impl.adminMenu;

import CZN.command.Command;
import CZN.enums.Language;
import CZN.enums.WaitingType;
import CZN.exceptions.ButtonNotFoundException;
import CZN.exceptions.CommandNotFoundException;
import CZN.exceptions.KeyboardNotFoundException;
import CZN.exceptions.MessageNotFoundException;
import CZN.model.standart.Button;
import com.itextpdf.text.DocumentException;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class id017_EditMenuButtons extends Command {

    private int inlineMessId;
    private int wrongMessId;
    private int infoMessId;
    private int notFoundMess;
    private Button currentButton;
    private Language currentLang;
    private List<Button> searchResultButtons;

    @Override
    public boolean execute() throws TelegramApiException, IOException, SQLException, FileNotFoundException, MessageNotFoundException, KeyboardNotFoundException, ButtonNotFoundException, CommandNotFoundException, DocumentException {
        if (!isAdmin()){
            sendMessage(5);
            return EXIT;
        }
        switch (waitingType) {
            case START:
                deleteUpdateMess();
                infoMessId = sendMessage(105);
                waitingType = WaitingType.SEARCH_BUTTON;
                return COMEBACK;
            case SEARCH_BUTTON:
                currentLang = getLanguage();
                deleteUpdateMess();
                deleteNotFoundMess();
                if (hasMessageText()){
                    searchResultButtons = buttonRepo.findAllByNameContainingAndLangIdOrderById(updateMessageText, currentLang.getId());
                    if (searchResultButtons.size() != 0){
                        deleteMessage(infoMessId);
                        inlineMessId = sendMessage(getInfoMessages(searchResultButtons));
                        waitingType = WaitingType.CHOOSE_OPTION;
                    }
                    else {
                        sendNotFound();
                    }
                }
                else {
                    sendNotFound();
                }
                return COMEBACK;

            case CHOOSE_OPTION:
                deleteUpdateMess();
                deleteWrongMess();
                if (updateMessageText.contains("/editName")){ //edit name
                    currentButton = buttonRepo.findByIdAndLangId(getInt(updateMessageText.substring(9)), currentLang.getId());
                    if (currentButton == null) {
                        sendWrongData();
                        return COMEBACK;
                    }

                    deleteMessage(inlineMessId);
                    inlineMessId = sendMessage(getInfoForEdit(currentButton));
//                    editMessage(getInfoMessage(currentMessage), inlineMessId);
//                    infoMessId = sendMessage(57);
                    waitingType = WaitingType.SET_TEXT;
                }
                else if (updateMessageText.contains("/back")){ // back
                    deleteMessage(infoMessId);
                    deleteMessage(inlineMessId);
                    infoMessId = sendMessage(105);
                    waitingType = WaitingType.SEARCH_BUTTON;
                }
                else if (updateMessageText.contains("/swapLanguage")){ //swap lang
                    if (currentLang.getId() == 1) {
                        currentLang = Language.kz;
                    }else if (currentLang.getId() == 2) {
                        currentLang = Language.ru;
                    }
                    List<Button> newSearchRes = new ArrayList<>();
                    for (Button message : searchResultButtons){
                        newSearchRes.add(buttonRepo.findByIdAndLangId(message.getId(), currentLang.getId()));
                    }

                    searchResultButtons = newSearchRes;
                    if (currentButton != null)
                        currentButton = buttonRepo.findByIdAndLangId(currentButton.getId(), currentLang.getId());

                    newSearchRes = null;

                    System.out.println(getInfoMessages(searchResultButtons));
                    editMessage(getInfoMessages(searchResultButtons), inlineMessId);

                }
                else{
                    sendWrongData();
                }
                return COMEBACK;

            case SET_TEXT:
                deleteUpdateMess();
                deleteWrongMess();
                if (hasMessageText()){
                    if (updateMessageText.equals("/cancel")){
                        deleteMessage(infoMessId);
                        deleteMessage(inlineMessId);
                        inlineMessId = sendMessage(getInfoMessages(searchResultButtons));
                        waitingType = WaitingType.CHOOSE_OPTION;
                        return COMEBACK;
                    }
                    else {
                        buttonRepo.update(updateMessageText, currentButton.getId(), currentLang.getId());
                        deleteMessage(inlineMessId);
                        deleteMessage(infoMessId);
                        currentButton = buttonRepo.findByIdAndLangId(currentButton.getId(), currentLang.getId());
                        searchResultButtons =  updateMessages(searchResultButtons);

                        inlineMessId = sendMessage(getInfoMessages(searchResultButtons));
                        waitingType = WaitingType.CHOOSE_OPTION;
                    }
                }
                else{
                    sendWrongData();
                }
                return COMEBACK;
        }
        return EXIT;
    }

    private List<Button> updateMessages(List<Button> searchResultMessage) {
        List<Button> newSearchRes = new ArrayList<>();
        for (Button message : searchResultMessage){
            newSearchRes.add(buttonRepo.findByIdAndLangId(message.getId(), currentLang.getId()));
        }

        return newSearchRes;
    }

    private String getInfoForEdit(Button currentMessage) {
        return getText(107) + currentMessage.getName() + next +
                getText(108);
    }

    private String getInfoMessages(List<Button> searchResultMessages) {
        StringBuilder stringBuilder = new StringBuilder();
        for (Button message : searchResultMessages){
            stringBuilder.append(message.getName()).append(" \uD83D\uDD8A /editName").append(message.getId()).append(next).append(next);
        }

        return  String.format(getText(110), stringBuilder.toString(), currentLang.name());
    }

    private void deleteNotFoundMess() {
        if (notFoundMess != 0){
            deleteMessage(notFoundMess);
        }
    }

    private void sendNotFound() throws TelegramApiException {
        deleteMessage(updateMessageId);
        deleteNotFoundMess();
        notFoundMess = sendMessage(109, chatId);
    }

    private Integer getInt(String updateMessageText) {
        try {
            return Integer.parseInt(updateMessageText);
        }catch (Exception e){
            return -1;
        }
    }

    private void deleteUpdateMess() {
        deleteMessage(updateMessageId);
    }

    private void deleteWrongMess(){
        if (wrongMessId != 0)
            deleteMessage(wrongMessId);
    }

    private void sendWrongData() throws TelegramApiException {
        deleteMessage(updateMessageId);
        deleteWrongMess();
        wrongMessId = sendMessage(3, chatId);

    }
}
