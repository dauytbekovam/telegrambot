package CZN.command.impl;

import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import CZN.command.Command;

public class id001_ShowInfo extends Command {

    @Override
    public boolean execute() throws TelegramApiException {
        deleteMessage(updateMessageId);

        sendMessageWithAddition();

        return EXIT;
    }
}
