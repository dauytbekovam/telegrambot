package CZN.command.impl.consultantMenu;

import CZN.command.Command;
import CZN.enums.FileType;
import CZN.enums.WaitingType;
import CZN.exceptions.ButtonNotFoundException;
import CZN.exceptions.CommandNotFoundException;
import CZN.exceptions.KeyboardNotFoundException;
import CZN.exceptions.MessageNotFoundException;
import CZN.model.custom.userMenu.MessageToConsultant;
import CZN.util.ButtonsLeaf;
import com.itextpdf.text.DocumentException;
import org.telegram.telegrambots.meta.api.methods.send.SendDocument;
import org.telegram.telegrambots.meta.api.methods.send.SendMediaGroup;
import org.telegram.telegrambots.meta.api.methods.send.SendPhoto;
import org.telegram.telegrambots.meta.api.methods.send.SendVideo;
import org.telegram.telegrambots.meta.api.objects.InputFile;
import org.telegram.telegrambots.meta.api.objects.media.InputMedia;
import org.telegram.telegrambots.meta.api.objects.media.InputMediaDocument;
import org.telegram.telegrambots.meta.api.objects.media.InputMediaPhoto;
import org.telegram.telegrambots.meta.api.objects.media.InputMediaVideo;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboard;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.*;

public class id016_Active extends Command {
    Map<String, FileType> files;
    MessageToConsultant offer = new MessageToConsultant();
    List<MessageToConsultant> messageToConsultants;
    private ButtonsLeaf buttonsLeaf;
    int deleteId;

    @Override
    public boolean execute() throws TelegramApiException, IOException, SQLException, FileNotFoundException, MessageNotFoundException, KeyboardNotFoundException, ButtonNotFoundException, CommandNotFoundException, DocumentException {
        switch (waitingType) {
            case START:
                deleteMessages();
                if (isButton(51)) {
                    deleteId = sendMessageWithKeyboard(getText(68), getKeyboardDuring());
                    waitingType = WaitingType.GET_DURING;
                    return COMEBACK;
                } else if (isButton(52)) {
                    deleteMessages();
                    deleteId = sendMessageWithKeyboard(getText(67), getKeyboardCompleted());
                    waitingType = WaitingType.GET_COMPLETED;
                    return COMEBACK;
                } else {
                    sendWrongData();
                }
            case GET_COMPLETED:
                deleteMessages();
                if (hasCallbackQuery()) {
                    if (buttonsLeaf.isNext(">>")) {
                        sendMessageWithKeyboard(getText(68), buttonsLeaf.getListButtonWithDataList());
                    }
                }

                if (isButton(65)) {
                    deleteMessages();
                    sendMessageWithKeyboard(getText(66), 13);
                    waitingType = WaitingType.SET_COMP;
                    return COMEBACK;
                } else {
                    deleteMessages();
                    MessageToConsultant offers = messageToConsultantRepo.findById(Long.parseLong(updateMessageText));
                    if (offers != null) {

                        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
                        StringBuilder stringBuilder = new StringBuilder();

                        stringBuilder.
                                append("№").append(offers.getId()).append(next).
                                append(getText(27)).append(offers.getUser().getFullName()).append(next).
                                append(getText(28)).append(offers.getUser().getPhone()).append(next).
                                append(getText(29)).append(offers.getOffer()).append(next).
                                append(getText(30)).append(simpleDateFormat.format(offers.getDate())).append(next).
                                append(getText(33)).append(offers.getAnswer()).append(next).
                                append(getText(32)).append(simpleDateFormat.format(offers.getDateEmplyee()));

                        if (offers.getFile() != null) {
                            int sizePhoto = 0;
                            int sizeVideo = 0;
                            int sizeDocument = 0;
                            List<String> photos = new ArrayList<>();
                            List<String> videos = new ArrayList<>();
                            List<String> document = new ArrayList<>();
                            for (Map.Entry<String, FileType> s : offers.getFile().entrySet()) {
                                if (s.getValue().equals(FileType.photo)) {
                                    sizePhoto++;
                                    photos.add(s.getKey());
                                } else if (s.getValue().equals(FileType.video)) {
                                    sizeVideo++;
                                    videos.add(s.getKey());
                                } else if (s.getValue().equals(FileType.document)) {
                                    sizeDocument++;
                                    document.add(s.getKey());
                                }
                            }

                            for (Map.Entry<String, FileType> s : offers.getFileEmp().entrySet()) {
                                if (s.getValue().equals(FileType.photo)) {
                                    sizePhoto++;
                                    photos.add(s.getKey());
                                } else if (s.getValue().equals(FileType.video)) {
                                    sizeVideo++;
                                    videos.add(s.getKey());
                                } else if (s.getValue().equals(FileType.document)) {
                                    sizeDocument++;
                                    document.add(s.getKey());
                                }
                            }


                            List<InputMedia> media = new ArrayList<>();
                            List<InputMedia> mediaDocument = new ArrayList<>();
                            SendDocument sendDocument = new SendDocument();
                            SendVideo sendVideo = new SendVideo();
                            SendPhoto sendPhoto = new SendPhoto();
                            InputMediaPhoto inputMediaPhoto;
                            InputMediaVideo inputMediaVideo;
                            if (sizePhoto > 0 && sizeVideo > 0) {
                                for (String s : photos) {
                                    inputMediaPhoto = new InputMediaPhoto();
                                    inputMediaPhoto.setMedia(s);
                                    media.add(inputMediaPhoto);
                                }

                                for (String s : videos) {
                                    inputMediaVideo = new InputMediaVideo();
                                    inputMediaVideo.setMedia(s);
                                    media.add(inputMediaVideo);
                                }
                            } else if (sizePhoto > 0 && sizeVideo == 0) {
                                for (String s : photos) {
                                    inputMediaPhoto = new InputMediaPhoto();
                                    inputMediaPhoto.setMedia(s);
                                    media.add(inputMediaPhoto);
                                }
                            } else if (sizePhoto == 0 && sizeVideo > 0) {
                                for (String s : videos) {
                                    inputMediaVideo = new InputMediaVideo();
                                    inputMediaVideo.setMedia(s);
                                    media.add(inputMediaVideo);
                                }
                            }
                            InputMediaDocument inputMediaDocument;
                            if (sizeDocument > 0) {
                                for (String s : document) {
                                    inputMediaDocument = new InputMediaDocument();
                                    inputMediaDocument.setMedia(s);
                                    mediaDocument.add(inputMediaDocument);
                                }

                            }

                            SendMediaGroup sendMediaGroupDoc = new SendMediaGroup();
                            SendMediaGroup sendMediaGroup = new SendMediaGroup();

                            if (mediaDocument.size() > 1) {
                                sendMediaGroupDoc.setMedias(mediaDocument);
                                sendMediaGroupDoc.setChatId(String.valueOf(chatId));
                                bot.execute(sendMediaGroupDoc);
                            } else if (sizeDocument == 1) {
                                sendDocument.setDocument(new InputFile(document.get(0)));
                                sendDocument.setChatId(String.valueOf(chatId));
                                bot.execute(sendDocument);
                            }

                            if (media.size() >= 1) {
                                if (photos.size() == 0 && videos.size() == 1) {
                                    sendVideo.setVideo(new InputFile(videos.get(0)));
                                    sendVideo.setReplyMarkup(keyboardMarkUpService.select(16));
                                    sendVideo.setParseMode("html");
                                    sendVideo.setCaption(stringBuilder.toString());
                                    sendVideo.setChatId(String.valueOf(chatId));
                                    bot.execute(sendVideo);
                                    waitingType = WaitingType.GO_TO_BACK;
                                    return COMEBACK;
                                } else if (photos.size() == 1 && videos.size() == 0) {
                                    sendPhoto.setPhoto(new InputFile(photos.get(0)));
                                    sendPhoto.setReplyMarkup(keyboardMarkUpService.select(16));
                                    sendPhoto.setParseMode("html");
                                    sendPhoto.setCaption(stringBuilder.toString());
                                    sendPhoto.setChatId(String.valueOf(chatId));
                                    bot.execute(sendPhoto);
                                    waitingType = WaitingType.GO_TO_BACK;
                                    return COMEBACK;
                                } else {
                                    sendMediaGroup.setMedias(media);
                                    sendMediaGroup.setChatId(String.valueOf(chatId));
                                    bot.execute(sendMediaGroup);
                                    sendMessageWithKeyboard(stringBuilder.toString(), 16);
                                    waitingType = WaitingType.GO_TO_BACK;
                                    return COMEBACK;
                                }
                            } else {
                                sendMessageWithKeyboard(stringBuilder.toString(), 16);
                                waitingType = WaitingType.GO_TO_BACK;
                                return COMEBACK;
                            }
                        } else {
                            sendMessageWithKeyboard(stringBuilder.toString(), 16);
                            waitingType = WaitingType.GO_TO_BACK;
                            return COMEBACK;
                        }
                    }
                }
            case GET_DURING:
                deleteMessages();
                if (hasCallbackQuery()) {
                    if (buttonsLeaf.isNext(">>")) {
                        sendMessageWithKeyboard(getText(68), buttonsLeaf.getListButtonWithDataList());
                    }
                }

                MessageToConsultant offers = messageToConsultantRepo.findById(Long.parseLong(updateMessageText));

                if (offers != null) {
                    StringBuilder stringBuilder = new StringBuilder();
                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");

                    stringBuilder.append("№").append(offers.getId()).append(next).
                            append(getText(27)).append(offers.getUser().getFullName()).append(next).
                            append(getText(28)).append(offers.getUser().getPhone()).append(next).
                            append(getText(72)).append(offers.getOffer()).append(next).
                            append(getText(30)).append(simpleDateFormat.format(offers.getDate()));

                    if (offers.getFile() != null) {
                        int sizePhoto = 0;
                        int sizeVideo = 0;
                        int sizeDocument = 0;
                        List<String> photos = new ArrayList<>();
                        List<String> videos = new ArrayList<>();
                        List<String> document = new ArrayList<>();
                        for (Map.Entry<String, FileType> s : offers.getFile().entrySet()) {
                            if (s.getValue().equals(FileType.photo)) {
                                sizePhoto++;
                                photos.add(s.getKey());
                            } else if (s.getValue().equals(FileType.video)) {
                                sizeVideo++;
                                videos.add(s.getKey());
                            } else if (s.getValue().equals(FileType.document)) {
                                sizeDocument++;
                                document.add(s.getKey());
                            }
                        }


                        List<InputMedia> media = new ArrayList<>();
                        List<InputMedia> mediaDocument = new ArrayList<>();
                        SendDocument sendDocument = new SendDocument();
                        SendVideo sendVideo = new SendVideo();
                        SendPhoto sendPhoto = new SendPhoto();
                        InputMediaPhoto inputMediaPhoto;
                        InputMediaVideo inputMediaVideo;
                        if (sizePhoto > 0 && sizeVideo > 0) {
                            for (String s : photos) {
                                inputMediaPhoto = new InputMediaPhoto();
                                inputMediaPhoto.setMedia(s);
                                media.add(inputMediaPhoto);
                            }

                            for (String s : videos) {
                                inputMediaVideo = new InputMediaVideo();
                                inputMediaVideo.setMedia(s);
                                media.add(inputMediaVideo);
                            }
                        } else if (sizePhoto > 0 && sizeVideo == 0) {
                            for (String s : photos) {
                                inputMediaPhoto = new InputMediaPhoto();
                                inputMediaPhoto.setMedia(s);
                                media.add(inputMediaPhoto);
                            }
                        } else if (sizePhoto == 0 && sizeVideo > 0) {
                            for (String s : videos) {
                                inputMediaVideo = new InputMediaVideo();
                                inputMediaVideo.setMedia(s);
                                media.add(inputMediaVideo);
                            }
                        }
                        InputMediaDocument inputMediaDocument;
                        if (sizeDocument > 0) {
                            for (String s : document) {
                                inputMediaDocument = new InputMediaDocument();
                                inputMediaDocument.setMedia(s);
                                mediaDocument.add(inputMediaDocument);
                            }

                        }

                        SendMediaGroup sendMediaGroupDoc = new SendMediaGroup();
                        SendMediaGroup sendMediaGroup = new SendMediaGroup();

                        if (mediaDocument.size() > 1) {
                            sendMediaGroupDoc.setMedias(mediaDocument);
                            sendMediaGroupDoc.setChatId(String.valueOf(chatId));
                            bot.execute(sendMediaGroupDoc);
                        } else if (sizeDocument == 1) {
                            sendDocument.setDocument(new InputFile(document.get(0)));
                            sendDocument.setChatId(String.valueOf(chatId));
                            bot.execute(sendDocument);
                        }
                        if (media.size() >= 1) {
                            if (photos.size() == 0 && videos.size() == 1) {
                                sendVideo.setVideo(new InputFile(videos.get(0)));
                                sendVideo.setReplyMarkup(keyboardMarkUpService.select(17));
                                sendVideo.setParseMode("html");
                                sendVideo.setCaption(stringBuilder.toString());
                                sendVideo.setChatId(String.valueOf(chatId));
                                bot.execute(sendVideo);
                                waitingType = WaitingType.GO_BACK;
                                return COMEBACK;
                            } else if (photos.size() == 1 && videos.size() == 0) {
                                sendPhoto.setPhoto(new InputFile(photos.get(0)));
                                sendPhoto.setReplyMarkup(keyboardMarkUpService.select(17));
                                sendPhoto.setParseMode("html");
                                sendPhoto.setCaption(stringBuilder.toString());
                                sendPhoto.setChatId(String.valueOf(chatId));
                                bot.execute(sendPhoto);
                                waitingType = WaitingType.GO_BACK;
                                return COMEBACK;
                            } else {
                                sendMediaGroup.setMedias(media);
                                sendMediaGroup.setChatId(String.valueOf(chatId));
                                bot.execute(sendMediaGroup);
                                sendMessageWithKeyboard(stringBuilder.toString(), 17);
                                waitingType = WaitingType.GO_BACK;
                                return COMEBACK;
                            }
                        } else {
                            sendMessageWithKeyboard(stringBuilder.toString(), 17);
                            waitingType = WaitingType.GO_BACK;
                            return COMEBACK;
                        }
                    } else {
                        sendMessageWithKeyboard(stringBuilder.toString(), 17);
                        waitingType = WaitingType.GO_BACK;
                        return COMEBACK;
                    }

                }
            case GO_BACK:
                deleteMessages();
                if (hasCallbackQuery()) {
                    if (isButton(67)) {
                        deleteMessages();
                        deleteId = sendMessageWithKeyboard(getText(68), getKeyboardDuring());
                        waitingType = WaitingType.GET_DURING;
                        return COMEBACK;
                    } else if (isButton(66)) {
                        int id;
                        try {
                            id = Integer.parseInt(update.getCallbackQuery().getMessage().getText().split("№")[1].split("\n")[0]);
                        } catch (Exception e) {
                            try {
                                id = Integer.parseInt(update.getCallbackQuery().getMessage().getCaption().split("№")[1].split("\n")[0]);
                            } catch (Exception exception) {
                                id = 0;
                            }
                        }
                        if (id == 0) {
                            deleteId = sendMessage("Ошибка при ответе");
                            return EXIT;
                        } else {
                            deleteMessages();
                            offer = messageToConsultantRepo.findById(id);
                            deleteId = sendMessage(getText(36));
                            waitingType = WaitingType.WRITE_ANSWER;
                            return COMEBACK;
                        }
                    }
                }
            case WRITE_ANSWER:
                deleteMessages();
                if (update.hasMessage() && update.getMessage().hasText()) {
                    deleteMessages();
                    files = new HashMap<>();
                    offer.setAnswer(update.getMessage().getText());
                    offer.setDateEmplyee(new Date());
                    offer.setAccepted(true);
                    sendMessageWithKeyboard(getText(119), 28);
                    waitingType = WaitingType.SET_APL;
                    return COMEBACK;
                } else {
                    sendWrongData();
                    deleteId = sendMessage(getText(36));
                    waitingType = WaitingType.WRITE_ANSWER;
                    return COMEBACK;
                }
            case SET_APL:
                deleteMessages();
                if (hasCallbackQuery()) {
                    if (isButton(92)) {
                        sendMessageWithKeyboard(getText(131), 34);
                        waitingType = WaitingType.SET_EMPFILEAPL;
                        return COMEBACK;
                    } else if (isButton(91)) {
                        sendMessage(getText(119));
                        waitingType = WaitingType.SET_FILEAPL;
                        return COMEBACK;
                    }
                } else {
                    sendWrongData();
                    sendMessageWithKeyboard(getText(119), 28);
                    waitingType = WaitingType.SET_APL;
                    return COMEBACK;
                }
            case SET_FILEAPL:
                deleteMessages();
                if (update.hasMessage() && update.getMessage().hasPhoto()) {
                    if (files.size() < 11) {
                        files.put(updateMessagePhoto, FileType.photo);
                        sendMessage(120);
                        sendMessageWithKeyboard(getText(119), 28);
                        waitingType = WaitingType.SET_APL;
                        return COMEBACK;
                    } else {
                        sendMessageWithKeyboard(getText(131), 34);
                        waitingType = WaitingType.SET_EMPFILEAPL;
                        return COMEBACK;
                    }

                } else if (update.hasMessage() && update.getMessage().hasVideo()) {
                    if (files.size() < 11) {
                        files.put(updateMessageVideo, FileType.video);
                        sendMessage(120);
                        sendMessageWithKeyboard(getText(119), 28);
                        waitingType = WaitingType.SET_APL;
                        return COMEBACK;
                    } else {
                        sendMessageWithKeyboard(getText(131), 34);
                        waitingType = WaitingType.SET_EMPFILEAPL;
                        return COMEBACK;
                    }

                } else if (update.hasMessage() && update.getMessage().hasDocument()) {
                    if (files.size() < 11) {
                        files.put(updateMessageDocument, FileType.document);
                        sendMessage(120);
                        sendMessageWithKeyboard(getText(119), 28);
                        waitingType = WaitingType.SET_APL;
                        return COMEBACK;
                    } else {
                        sendMessageWithKeyboard(getText(131), 34);
                        waitingType = WaitingType.SET_EMPFILEAPL;
                        return COMEBACK;
                    }
                } else {
                    sendMessageWithKeyboard(getText(119), 28);
                    waitingType = WaitingType.SET_APL;
                    return COMEBACK;
                }
            case SET_EMPFILEAPL:
                deleteMessages();
                if (hasCallbackQuery()) {
                    if (isButton(97)) {
                        offer.setFileEmp(files);
                        messageToConsultantRepo.save(offer);
                        StringBuilder stringBuilder = new StringBuilder();
                        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");

                        stringBuilder.append("№: ").append(offer.getId()).append(next).
                                append(getText(37)).append(next).
                                append(getText(38)).append(offer.getAnswer()).append(next).
                                append(getText(30)).append(simpleDateFormat.format(offer.getDate()));

                        int sizePhoto = 0;
                        int sizeVideo = 0;
                        int sizeDocument = 0;
                        List<String> photos = new ArrayList<>();
                        List<String> videos = new ArrayList<>();
                        List<String> document = new ArrayList<>();

                        for (Map.Entry<String, FileType> s : files.entrySet()) {
                            if (s.getValue().equals(FileType.photo)) {
                                sizePhoto++;
                                photos.add(s.getKey());
                            } else if (s.getValue().equals(FileType.video)) {
                                sizeVideo++;
                                videos.add(s.getKey());
                            } else if (s.getValue().equals(FileType.document)) {
                                sizeDocument++;
                                document.add(s.getKey());
                            }
                        }

                        List<InputMedia> media = new ArrayList<>();
                        List<InputMedia> mediaDocument = new ArrayList<>();
                        SendDocument sendDocument = new SendDocument();
                        SendVideo sendVideo = new SendVideo();
                        SendPhoto sendPhoto = new SendPhoto();
                        InputMediaPhoto inputMediaPhoto;
                        InputMediaVideo inputMediaVideo;
                        if (sizePhoto > 0 && sizeVideo > 0) {
                            for (String s : photos) {
                                inputMediaPhoto = new InputMediaPhoto();
                                inputMediaPhoto.setMedia(s);
                                media.add(inputMediaPhoto);
                            }

                            for (String s : videos) {
                                inputMediaVideo = new InputMediaVideo();
                                inputMediaVideo.setMedia(s);
                                media.add(inputMediaVideo);
                            }
                        } else if (sizePhoto > 0 && sizeVideo == 0) {
                            for (String s : photos) {
                                inputMediaPhoto = new InputMediaPhoto();
                                inputMediaPhoto.setMedia(s);
                                media.add(inputMediaPhoto);
                            }
                        } else if (sizePhoto == 0 && sizeVideo > 0) {
                            for (String s : videos) {
                                inputMediaVideo = new InputMediaVideo();
                                inputMediaVideo.setMedia(s);
                                media.add(inputMediaVideo);
                            }
                        }
                        InputMediaDocument inputMediaDocument;
                        if (sizeDocument > 0) {
                            for (String s : document) {
                                inputMediaDocument = new InputMediaDocument();
                                inputMediaDocument.setMedia(s);
                                mediaDocument.add(inputMediaDocument);
                            }

                        }

                        SendMediaGroup sendMediaGroupDoc = new SendMediaGroup();
                        SendMediaGroup sendMediaGroup = new SendMediaGroup();

                        if (mediaDocument.size() > 1) {
                            sendMediaGroupDoc.setMedias(mediaDocument);
                            sendMediaGroupDoc.setChatId(String.valueOf(offer.getUser().getChatId()));
                            bot.execute(sendMediaGroupDoc);
                        } else if (sizeDocument == 1) {
                            sendDocument.setDocument(new InputFile(document.get(0)));
                            sendDocument.setChatId(String.valueOf(offer.getUser().getChatId()));
                            bot.execute(sendDocument);
                        }

                        if (media.size() >= 1) {
                            if (photos.size() == 0 && videos.size() == 1) {
                                sendVideo.setVideo(new InputFile(videos.get(0)));
                                sendVideo.setParseMode("html");
                                sendVideo.setCaption(stringBuilder.toString());
                                sendVideo.setChatId(String.valueOf(offer.getUser().getChatId()));
                                bot.execute(sendVideo);
                                deleteId = sendMessage(34);
                                deleteId = sendMessageWithKeyboard(getText(68), getKeyboardDuring());
                                waitingType = WaitingType.GET_DURING;
                                return COMEBACK;
                            } else if (photos.size() == 1 && videos.size() == 0) {
                                sendPhoto.setPhoto(new InputFile(photos.get(0)));
                                sendPhoto.setParseMode("html");
                                sendPhoto.setCaption(stringBuilder.toString());
                                sendPhoto.setChatId(String.valueOf(offer.getUser().getChatId()));
                                bot.execute(sendPhoto);
                                deleteId = sendMessage(34);
                                deleteId = sendMessageWithKeyboard(getText(68), getKeyboardDuring());
                                waitingType = WaitingType.GET_DURING;
                                return COMEBACK;
                            } else {
                                sendMediaGroup.setMedias(media);
                                sendMediaGroup.setChatId(String.valueOf(offer.getUser().getChatId()));
                                bot.execute(sendMediaGroup);
                                sendMessageWithKeyboard(stringBuilder.toString(), keyboardMarkUpService.select(10), offer.getUser().getChatId());
                                deleteId = sendMessage(34);
                                deleteId = sendMessageWithKeyboard(getText(68), getKeyboardDuring());
                                waitingType = WaitingType.GET_DURING;
                                return COMEBACK;
                            }
                        } else {
                            sendMessageWithKeyboard(stringBuilder.toString(), keyboardMarkUpService.select(10), offer.getUser().getChatId());
                            deleteId = sendMessage(34);
                            deleteId = sendMessageWithKeyboard(getText(68), getKeyboardDuring());
                            waitingType = WaitingType.GET_DURING;
                            return COMEBACK;
                        }

                    }
                }
        }
        return EXIT;
    }

    private void deleteMessages() {
        deleteMessage(updateMessageId);
        deleteMessage(deleteId);
    }

    private ReplyKeyboard getKeyboardDuring() {
        messageToConsultants = messageToConsultantRepo.findAll();
        List<String> buttonComp = new ArrayList<>();
        List<String> id = new ArrayList<>();


        for (MessageToConsultant offer : messageToConsultants) {
            if (!offer.getAccepted()) {
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
                buttonComp.add(simpleDateFormat.format(offer.getDate()));
                id.add(String.valueOf(offer.getId()));
            }
        }


        buttonsLeaf = new ButtonsLeaf(buttonComp, id, 3);

        return buttonsLeaf.getListButtonWithDataList();
    }


    private ReplyKeyboard getKeyboardCompleted() {
        messageToConsultants = messageToConsultantRepo.findAll();
        List<String> buttonComp = new ArrayList<>();
        List<String> id = new ArrayList<>();

        for (MessageToConsultant offer : messageToConsultants) {
            if (offer.getAccepted()) {
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
                buttonComp.add(simpleDateFormat.format(offer.getDate()));
                id.add(String.valueOf(offer.getId()));
            }
        }

        buttonsLeaf = new ButtonsLeaf(buttonComp, id, 3);

        return buttonsLeaf.getListButtonWithDataList();
    }

    private void sendWrongData() throws TelegramApiException {
        toDeleteMessage(sendMessage(18));
    }
}
