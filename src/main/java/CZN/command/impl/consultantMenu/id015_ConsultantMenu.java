package CZN.command.impl.consultantMenu;

import CZN.command.Command;
import CZN.exceptions.ButtonNotFoundException;
import CZN.exceptions.CommandNotFoundException;
import CZN.exceptions.KeyboardNotFoundException;
import CZN.exceptions.MessageNotFoundException;
import CZN.model.standart.Message;
import CZN.util.Const;
import com.itextpdf.text.DocumentException;
import org.telegram.telegrambots.meta.api.methods.send.SendAudio;
import org.telegram.telegrambots.meta.api.methods.send.SendDocument;
import org.telegram.telegrambots.meta.api.methods.send.SendVideo;
import org.telegram.telegrambots.meta.api.objects.InputFile;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.SQLException;

public class id015_ConsultantMenu extends Command {

    @Override
    public boolean execute() throws TelegramApiException, IOException, SQLException, FileNotFoundException, MessageNotFoundException, KeyboardNotFoundException, ButtonNotFoundException, CommandNotFoundException, DocumentException {
        deleteMessage();
        if (!isEmployee()) {
            sendMessage(Const.NO_ACCESS);
            return EXIT;
        }
        deleteMessage(updateMessageId);
        Message message = messageRepo.findByIdAndLanguageId((int) messageId, getLanguage().getId());
        sendMessage(messageId, chatId, null, message.getPhoto());
        if (message.getFile() != null) {
            switch (message.getFileType()) {
                case "audio":
                    SendAudio sendAudio = new SendAudio();
                    sendAudio.setAudio(new InputFile(message.getFile()));
                    sendAudio.setChatId(String.valueOf(chatId));
                    bot.execute(sendAudio);
                case "video":
                    SendVideo sendVideo = new SendVideo();
                    sendVideo.setVideo(new InputFile(message.getFile()));
                    sendVideo.setChatId(String.valueOf(chatId));
                    bot.execute(sendVideo);
                case "document":
                    SendDocument sendDocument = new SendDocument();
                    sendDocument.setChatId(String.valueOf(chatId));
                    sendDocument.setDocument(new InputFile(message.getFile()));
                    bot.execute(sendDocument);
            }
        }
        return EXIT;
    }
}
