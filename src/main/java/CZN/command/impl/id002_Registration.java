package CZN.command.impl;

import CZN.model.standart.Employee;
import CZN.model.standart.User;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import CZN.command.Command;
import CZN.service.RegistrationService;

public class id002_Registration extends Command {

    private RegistrationService registrationService = new RegistrationService();

    @Override
    public boolean execute() throws TelegramApiException {
        deleteMessage(updateMessageId);
        if (!isRegistered()) {
            if (!registrationService.isRegistration(update, botUtils)) {
                return COMEBACK;
            } else {
                usersRepo.save(registrationService.getUser());
                User user = registrationService.getUser();
                Employee employee = new Employee();
                employee.setFullName(user.getFullName());
                employee.setChatId(user.getChatId());
                employeeRepo.save(employee);
                sendMessageWithAddition();
            }
        } else {
            sendMessageWithAddition();
        }
        return EXIT;
    }
}
