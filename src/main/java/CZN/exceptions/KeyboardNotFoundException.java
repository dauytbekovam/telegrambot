package CZN.exceptions;

public class KeyboardNotFoundException extends Exception {
    public KeyboardNotFoundException(String s) {
        super(s);
    }
}
