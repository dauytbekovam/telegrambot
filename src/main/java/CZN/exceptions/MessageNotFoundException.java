package CZN.exceptions;

public class MessageNotFoundException extends Exception{

    public MessageNotFoundException(String s) {
        super(s);
    }
}
