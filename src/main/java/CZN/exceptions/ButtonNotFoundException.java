package CZN.exceptions;

public class ButtonNotFoundException extends Exception {
    
    public ButtonNotFoundException(String e){
        super(e);
    }
}
