package CZN.model.standart;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name = "roles")
public class Role {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(unique = false)
    private int     id;

    @Column(length = 500)
    private String  russianName;

    @Column(length = 500)
    private String  kazName;

}
