package CZN.model.standart;

import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
public class Employee {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int     id;

    private long    chatId;

    @Column(length = 500)
    private String  fullName;

    @ManyToMany(fetch = FetchType.EAGER)
    private List<Role> roleList;

    public Employee(long chatId, String fullName, List<Role> roleList) {
        this.chatId = chatId;
        this.fullName = fullName;
        this.roleList = roleList;
    }

    public Employee() {

    }

    public Employee setChatId(long chatId) {
        this.chatId = chatId;
        return this;
    }

}
