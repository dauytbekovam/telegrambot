package CZN.model.standart;

import CZN.model.custom.userMenu.PerDay;
import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name = "USERS")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int     id;

    private long    chatId;
    private String  phone;
    private String  fullName;

    @Column(length = 500)
    private String  username;
}