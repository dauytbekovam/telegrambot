package CZN.model.custom.userMenu;

import lombok.Data;

import javax.persistence.*;


@Table(name = "CONTACTS")
@Entity
@Data
public class AboutProjectContact {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(length = 1000)
    private String  nameRus;
    @Column(length = 1000)
    private String  nameKaz;

    @OneToOne
    private AboutProjectContactInfo aboutProjectContactInfo;

}
