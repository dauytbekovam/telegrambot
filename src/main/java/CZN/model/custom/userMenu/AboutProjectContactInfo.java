package CZN.model.custom.userMenu;

import lombok.Data;
import org.checkerframework.checker.units.qual.Length;

import javax.persistence.*;

@Entity
@Data
@Table(name = "CONTACT_INFO")
public class AboutProjectContactInfo {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int     id;

    @Column(columnDefinition="text", length = 1000)
    private String  nameRus;
    @Column(columnDefinition="text", length = 1000)
    private String  nameKaz;

}
