package CZN.model.custom.userMenu;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;
import java.util.Map;

@Entity
@Data
public class Poll {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private int interview;

    @ElementCollection(fetch = FetchType.EAGER)
    private Map<Integer, Integer> answers;     // <Q.id, A.id>

    @ElementCollection(fetch = FetchType.EAGER)
    private Map<Integer, String> answersReason;

    private Date date;
}
