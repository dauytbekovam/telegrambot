package CZN.model.custom.userMenu;


import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
public class PollInterview {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(unique = false)
    private int id;

    private String nameRU;
    private String nameKZ;

    @OneToMany(fetch = FetchType.EAGER)
    private List<PollQuestion> questions;

    private Boolean active;
}
