package CZN.model.custom.userMenu;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
public class ReminderMailing {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(unique = false)
    private int id;

    private int hour;
    private int minute;

}
