package CZN.model.custom.userMenu;

import CZN.enums.FileType;
import CZN.model.standart.User;
import lombok.Data;

import javax.persistence.*;
import java.util.Date;
import java.util.Map;

@Entity
@Data
public class AboutProjectOffer {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(unique = false)
    private long id;

    private Boolean accepted;
    private String offer;
    private String answer;


    @ElementCollection(fetch = FetchType.EAGER)
    Map<String, FileType> file;

    @ElementCollection(fetch = FetchType.EAGER)
    Map<String, FileType> fileEmp;

    @OneToOne
    User user;
    private Date date;
    private Date dateEmplyee;
}
