package CZN.model.custom.userMenu;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name = "INFO_ANSWERS")
public class InfoAnswer {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(unique = false)
    private int id;

    @Column(length = 500)
    private String  answerRus;

    @Column(length = 500)
    private String  answerKaz;

    @OneToOne
    private InfoQuestion infoQuestion;
}
