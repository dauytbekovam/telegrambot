package CZN.model.custom.userMenu;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name = "INFO_QUESTIONS")
public class InfoQuestion {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(length = 500)
    private String questionRus;

    @Column(length = 500)
    private String questionKaz;
}
