package CZN.model.custom.userMenu;

import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
public class PerDay {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(unique = false)
    private int id;

    @Column(length = 500)
    private String  nameRus;

    @Column(length = 500)
    private String  nameKaz;

    @OneToMany(fetch = FetchType.EAGER)
    private List<PerDayTime> hours;
}
