package CZN.model.custom.userMenu;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
public class Reminder {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(unique = false)
    private int     id;

    private String userId;
    private int hour;
    private int minute;

}
