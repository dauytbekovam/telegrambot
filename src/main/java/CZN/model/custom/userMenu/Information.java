package CZN.model.custom.userMenu;

import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
@Table(name = "INFORMATIONS")
public class Information {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int     id;

    @Column(length = 500)
    private String  nameRus;

    @Column(length = 500)
    private String  nameKaz;

    @OneToMany(fetch = FetchType.EAGER)
    private List<InfoQuestion> infoQuestions;
}
