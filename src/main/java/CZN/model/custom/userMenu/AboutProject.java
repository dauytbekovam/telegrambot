package CZN.model.custom.userMenu;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name = "ABOUT_PROJECT_FIELDS")
public class AboutProject {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(unique = false)
    private int     id;

    @Column(length = 1000)
    private String  nameRus;
    @Column(length = 1000)
    private String  nameKaz;
}
