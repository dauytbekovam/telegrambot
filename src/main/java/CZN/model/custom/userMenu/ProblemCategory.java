package CZN.model.custom.userMenu;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name = "PROBLEM_CATEGORIES")
public class ProblemCategory {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int     id;

    @Column(length = 500)
    private String  nameRus;

    @Column(length = 500)
    private String  nameKaz;

    @OneToOne
    private ProblemSubCategory problemSubCategory;
}
