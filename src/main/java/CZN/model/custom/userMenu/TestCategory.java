package CZN.model.custom.userMenu;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name = "TEST_CATEGORIES")
public class TestCategory {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int     id;

    private String  nameRus;
    private String  nameKaz;

    @OneToOne
    private TestSubCategory testSubCategory;
}
